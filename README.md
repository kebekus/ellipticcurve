# Elliptic Curve Plotter
<a href='https://flathub.org/apps/details/de.unifreiburg.ellipticcurve'><img width='100px' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png' style='vertical-align:middle'/></a>
<a href='https://snapcraft.io/elliptic-curve-plotter'><img width='100px' alt="Get it from the Snap Store" src='https://snapcraft.io/static/images/badges/en/snap-store-black.svg' style='vertical-align:middle'/></a>


The Elliptic Curve Plotter is a graphical application that illustrates elliptic curves. Users can play and exeriment with the curves and their group law.

- See the [app home page](https://kebekus.gitlab.io/ellipticcurve/) for more information.
- [Click here](https://cplx.vm.uni-freiburg.de/storage/software/ellipticcurve/wasm/ellipticcurve.html) to run a restricted version of the Elliptic Curve Plotter in your web browser right now.
