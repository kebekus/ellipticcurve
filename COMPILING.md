# Compiling

## Dependencies

* Qt5 development packages (including QtSVG and the lupdate tool)

* CMake build system

* C++ compiler

* doxygen if you want API documentation


## Compilation

### Linux

The Elliptic Curve Plotter uses the standard qmake cmake system, so compilation
is straightforward. Once the compilation terminates, you'll find a binary in the
build directory.

### Windows

The cmake files provided can be used to cross-compile a static binary for
windows, using the MXE (M cross environment, https://mxe.cc) cross compiler
unter Linux.


## Installation

On Linux, simply issue the command 'make install'


## Unit tests

On Linux, the command 'make test' compiles and runs a few unit tests.


## API documentation

In order to extract the (little) API documentation there is, use the command
"make doc" to run doxygen.  The resulting html files will be created in
"src/html", inside the build directory

