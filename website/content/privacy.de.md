---
title: "Datenschutz"
description: "Datenschutz"
menu: footer # Optional, add page to a menu. Options: main, side, footer
---

## 1. Verantwortlich im datenschutzrechtlichen Sinne

```
   Albert-Ludwigs-Universität Freiburg
   Mathematisches Institut
   Prof. Dr. Stefan Kebekus
   Ernst-Zermelo-Straße 1
   79094 Freiburg
```


## 2. Datenschutz

```
   Albert-Ludwigs-Universität Freiburg
   Datenschutzbeauftragter
   Friedrichstraße 39
   79098 Freiburg
   datenschutz@uni-freiburg.de
```


## 3. Hinweis vorab

Diese Informationen zum Datenschutz/Datenschutzerklärung beziehen sich auf die
Webseiten, die Sie unter cplx.vm.uni-freiburg.de erreichen. Diese Seiten werden
technisch vom Rechenzentrum der Universität Freiburg betrieben wird.

Generell nehmen wir die Privatsphäre der Besucherinnen und Besucher sehr ernst
und erheben nur erforderliche Daten, um die Funktionalität und Bedienbarkeit der
Webseiten zu optimieren.


## 4. Erhebung personenbezogener Daten

Soweit nicht auf den jeweiligen Webseiten besonders ausgeführt, werden
personenbezogene Daten wie folgt erhoben:

### 4.1. Bereitstellung der Webseite und Erstellung von Logfiles

#### 4.1.1. Beschreibung und Kategorien von Daten

Wenn Sie diese oder andere Internetseiten aufrufen, übermitteln Sie über Ihren
Browser Daten an unseren Webserver. Die folgenden Daten werden während einer
laufenden Verbindung ausschließlich im Fehlerfall temporär in einer Logdatei
aufgezeichnet:

- IP-Adresse des anfragenden Rechners

- Datum und Uhrzeit des Zugriffs

- Name, URL und übertragene Datenmenge zu der angefragten Datei

- Zugriffsstatus (angeforderte Datei übertragen, nicht gefunden etc.)

- Browsertyp und Betriebssystem (sofern vom anfragenden Webbrowser übermittelt)

- Webseite, von der aus der Zugriff erfolgte (sofern vom anfragenden Webbrowser
  übermittelt)

Die Verarbeitung der Daten in dieser Logdatei geschieht wie folgt.

- In Einzelfällen, d.h. bei gemeldeten Störungen, Fehlern und
  Sicherheitsvorfällen, erfolgt eine manuelle Analyse.

#### 4.1.2. Zweck

Die vorübergehende Speicherung der IP-Adresse durch das System ist notwendig, um
eine Auslieferung der Website an den Rechner des Nutzers zu ermöglichen. Hierfür
muss die IP-Adresse des Nutzers für die Dauer der Sitzung gespeichert bleiben.
Die Speicherung in einer Logdatei erfolgt, um die Funktionsfähigkeit der Website
sicherzustellen. Zudem dienen uns die Daten zur Optimierung der Website und zur
Sicherstellung der Sicherheit unserer informationstechnischen Systeme. Die in
den Logeinträgen enthaltenen IP-Adressen werden nicht mit anderen Datenbeständen
zusammengeführt, es sei denn, es liegen tatsächliche Anhaltspunkte für eine
Störung des ordnungsgemäßen Betriebs vor. In diesen Zwecken liegt auch unser
berechtigtes Interesse an der Datenverarbeitung nach Art. 6 Abs. 1 lit. f
DS-GVO.

#### 4.1.3. Rechtsgrundlage

Rechtsgrundlage für die vorübergehende Speicherung der Daten und der Logfiles
ist Art. 6 Abs. 1 lit. f DS-GVO.

#### 4.1.4. Empfänger

Sofern wegen Angriffen auf unsere informationstechnischen System
Ermittlungsmaßnahmen eingeleitet werden, können die oben unter 4.1. genannten
Daten bzw. Logdateien an staatliche Ermittlungsorgane (z.B. Polizei,
Staatsanwaltschaft) weitergegeben werden. Dasselbe gilt, wenn entsprechende
Behörden oder Gerichte Anfragen an die Universität richten und die Universität
dazu verpflichtet ist, diesen Folge zu leisten.

#### 4.1.5. Dauer der Speicherung

Die Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer
Erhebung nicht mehr erforderlich sind. Die Speicherung der Daten in Logdateien
werden nach zwei Wochen gelöscht.

#### 4.1.6. Folgen der Nichtangabe, Widerspruchs- bzw. Beseitigungsmöglichkeit

Die Erfassung der Daten zur Bereitstellung der Website und die Speicherung der
Daten in Logfiles ist für den Betrieb der Internetseite zwingend
erforderlich. Nutzer, die nicht möchten, dass ihre Daten wie beschrieben
verarbeitet werden, können sich über Alternativwege (telefonisch, schriftlich,
persönlich) an die Universität wenden, um entsprechende Informationen zu
erhalten oder Vorgänge durchzuführen.

### 4.2. Nutzung von Cookies

#### 4.2.1. Beschreibung und Kategorien von Daten

Unsere Webseite verwendet Cookies. Bei Cookies handelt es sich um Textdateien,
die vom Internetbrowser auf dem Computersystem des Nutzers gespeichert
werden. Ruft ein Nutzer eine Website auf, so kann ein Cookie auf dem
Computersystem des Nutzers gespeichert werden. Dieser Cookie enthält eine
charakteristische Zeichenfolge, die eine eindeutige Identifizierung des Browsers
beim erneuten Aufrufen der Website ermöglicht.

#### 4.2.2. Zweck

Der Zweck der Verwendung der unter 4.2.1 genannten technisch notwendiger Cookies
ist, die Nutzung von Websites für die Nutzer zu vereinfachen. Einige Funktionen
unserer Internetseite können ohne den Einsatz von Cookies nicht angeboten
werden. Für diese ist es erforderlich, dass der Browser auch nach einem
Seitenwechsel wiedererkannt wird.

Die durch die Cookies erhobenen Nutzerdaten werden nicht zur Erstellung von
Nutzerprofilen verwendet.

In diesen Zwecken liegt auch unser berechtigtes Interesse in der Verarbeitung
der personenbezogenen Daten nach Art. 6 Abs. 1 lit. f DS-GVO.

#### 4.2.3. Rechtsgrundlage

Die Rechtsgrundlage für die Verarbeitung personenbezogener Daten unter
Verwendung von Cookies ist Art. 6 Abs. 1 lit. f DS-GVO.

#### 4.2.6. Folgen der Nichtangabe, Widerspruchs- bzw. Beseitigungsmöglichkeit

Cookies werden auf dem Rechner der Besucher*innen gespeichert und von diesem an
unserer Seite übermittelt. Daher haben Sie als Nutzer*in – unabhängig von den
vorstehend aufgeführten Speicherfristen – auch die volle Kontrolle über die
Verwendung von Cookies. Durch eine Änderung der Einstellungen in Ihrem
Internetbrowser können Sie die Übertragung von Cookies deaktivieren oder
einschränken. Bereits gespeicherte Cookies können jederzeit gelöscht
werden. Dies kann auch automatisiert erfolgen. Werden Cookies für unseren
Webauftritt deaktiviert, können möglicherweise nicht mehr alle Funktionen der
Website vollumfänglich genutzt werden.

## 5. Ihre Rechte

Sie haben das Recht, von der Universität Auskunft über die zu Ihrer Person beim
Webseitenaufruf gespeicherten Daten zu erhalten und/oder unrichtig gespeicherte
Daten berichtigen zu lassen. Sie haben darüber hinaus das Recht auf Löschung
oder auf Einschränkung der Verarbeitung oder ein Widerspruchsrecht gegen die
Verarbeitung.  Bitte wenden Sie sich dazu an datenschutz@uni-freiburg.de. Sie
haben das Recht auf Beschwerde bei der Aufsichtsbehörde, wenn Sie der Ansicht
sind, dass die Verarbeitung der Sie betreffenden personenbezogenen Daten gegen
die Rechtvorschriften verstößt. Die zuständige Aufsichtsbehörde ist der
Landesbeauftragte für den Datenschutz und die Informationsfreiheit
Baden-Württemberg.
