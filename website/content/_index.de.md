---
title: "Elliptic Curve Plotter"
description: "Elliptic Curve Plotter"
---

<figure class="alignright is-resized">
  <a href="https://cplx.vm.uni-freiburg.de/storage/software/ellipticcurve/wasm/ellipticcurve.html" target="_blank" rel="noreferrer noopener">
    <img src="/ECP.webp" alt="" width="320" height="180"/>
  </a>
  <figcaption>
    Der Elliptic Curve Plotter
  </figcaption>
</figure>

Der Elliptic Curve Plotter ist eine graphische Applikation, die elliptische
Kurven illustriert.  Elliptische Kurven sind ein mathematisches Konzept, das
besonders in der Zahlentheorie relevant ist und in der Kryptographie zur
Konstruktion sicherer Verschlüsselungsmethoden verwendet wird.

Benutzer können elliptische Kurven skizzieren und mit dem
Gruppengesetz experimentieren. Die entstehenden Skizzen können im PNG oder SVG
Format gespeichert werden.


## Läuft direkt in Ihrem Web Browser

[Klicken Sie
hier](https://cplx.vm.uni-freiburg.de/storage/software/ellipticcurve/wasm/ellipticcurve.html)
um eine eingeschränkte Version des Elliptic Curve Plotter direkt in Ihrem Web
Browser zu starten.  Beachten Sie bitte, dass diese Online-Version des Elliptic
Curve Plotter keine Graphiken speichern kann und je nach Web-Browser vielleicht
etwas langsam wirkt. Die klassischen Programpakete, die Sie unten finden und
herunterladen können, haben diese Einschränkungen nicht.


## Download

<a href='https://flathub.org/apps/details/de.unifreiburg.ellipticcurve'><img width='100px' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png' style='vertical-align:middle'/></a>

- Linux: Die App steht zum kostenlosen Download bei
  [flathub.org](https://flathub.org/apps/details/de.unifreiburg.ellipticcurve)
  bereit. Vermutlich finden Sie die App aber auch in der Software-Management
  Anwendung auf Ihrem Computer.

- [MacOS X Disk
  Image](https://cplx.vm.uni-freiburg.de/storage/software/ellipticcurve/macos/Elliptic%20Curve%20Plotter-1.1.3.dmg)
  Universal Binary for all Apple computers.

- [Windows
  EXE-File](https://cplx.vm.uni-freiburg.de/storage/software/ellipticcurve/win32/ellipticcurve-1.1.5.exe)
  läuft ohne komplizierte Installationn – einfach herunterladen und anklicken.

- Den Quelltext des Programmes finden Sie
  [hier](https://gitlab.com/kebekus/ellipticcurve) auf GitLab. Das Programm
  verwendet die Qt-Bibliothek.  Sie finden eine Kopie des Quellcodes
  [hier](https://cplx.vm.uni-freiburg.de/storage/QtSources/).


## Autor

Der Elliptic Curve Plotter wurde von [Stefan
Kebekus](https://cplx.vm.uni-freiburg.de) an der Albert-Ludwigs-Universität
Freiburg geschrieben.


## Lizenz

Das Programm ist Open Source und kostenlos verfügbar.  Es ist unter der [GNU
Public License v3](http://www.gnu.de/documents/gpl-3.0.de.html) oder (nach
Wunsch des Anwenders) jeder spätere Version der GNU Public License lizensiert.
Falls Sie das Programm verwenden, um Illustrationen für eine Publikation zu
erstellen, würden wir uns über einen Verweis auf diese Seite freuen.
