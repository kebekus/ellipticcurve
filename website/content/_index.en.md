---
title: "Elliptic Curve Plotter"
description: "Elliptic Curve Plotter"
---

<figure class="alignright is-resized">
  <a href="https://cplx.vm.uni-freiburg.de/storage/software/ellipticcurve/wasm/ellipticcurve.html" target="_blank" rel="noreferrer noopener">
    <img src="/ECP.webp" alt="" width="320" height="180"/>
  </a>
  <figcaption>
    The Elliptic Curve Plotter in action
  </figcaption>
</figure>

The Elliptic Curve Plotter is a graphical application that illustrates elliptic
curves.  Elliptic curves are a mathematical concept that is especially important
in number theory and constitutes a major area of current research.  Elliptic
curves find applications in elliptic curve cryptography (ECC) and integer
factorization.

Users can sketch elliptic curves and experiment with their group law, and save
images in PNG or SVG format for later use.


## Runs right in your web browser

[Click
here](https://cplx.vm.uni-freiburg.de/storage/software/ellipticcurve/wasm/ellipticcurve.html)
to run a restricted version of the Elliptic Curve Plotter in your web browser
right now. Note that this online version of the program cannot save images and
might feel a little slow. For the best experience, please download and install
the native version of the program on your computer.



## Download

<a href='https://flathub.org/apps/details/de.unifreiburg.ellipticcurve'><img width='100px' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png' style='vertical-align:middle'/></a>

- Linux: The app is available for free download at
  [flathub.org](https://flathub.org/apps/details/de.unifreiburg.ellipticcurve),
  but you might also find the app in the software management application on your
  computer.

- [MacOS X Disk
  Image](https://cplx.vm.uni-freiburg.de/storage/software/ellipticcurve/macos/Elliptic%20Curve%20Plotter-1.1.3.dmg)
  Universal Binary for all Apple computers.

- [Windows
  EXE-File](https://cplx.vm.uni-freiburg.de/storage/software/ellipticcurve/win32/ellipticcurve-1.1.5.exe)
  works without complicated installation. Just download and start with a double
  click.

- You can find the source code [here](https://gitlab.com/kebekus/ellipticcurve)
  on GitLab. The program uses the Qt library. You can find a copy of the library
  source code [here](https://cplx.vm.uni-freiburg.de/storage/QtSources/).


## Author

The Elliptic Curve Plotter has been written by [Stefan
Kebekus](https://cplx.vm.uni-freiburg.de) at the University of Freiburg,
Germany.


## License

The program is open source and available for free. It is licensed under the [GNU
Public License v3](http://www.gnu.de/documents/gpl-3.0.de.html), or any later
version of the GNU Public License. If you use images created with this program
in your publication, we would be glad if you could include a reference to this
site.
