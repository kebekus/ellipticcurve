/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef _ecPlotter
#define _ecPlotter

#include <QGraphicsView>
#include <QPointer>
#include <QTimer>

#include "ecGraphicsScene.h"

class ec_real;
class ec_realFreePoint;
class ec_realPoint;


/**
 * Widget that displays an elliptic curve graphically.
 *
 * This widget displays a real elliptic curve and a number of points. The use
 * can manipulate the points with the mouse, and both curve and points are
 * monitored for changes.
 *
 * Objects displayed here should follows the following convention for
 * Z-Values.
 *
 * - coordinate grids, point box: -10
 *
 * - labelling of coordinates, point box: -9
 *
 * - elliptic curve: 1
 *
 * - points: 1--2
 *
 * The Z-value of the items should be set by before they are added to the
 * graphics scene.
 *
 * @author Stefan Kebekus
 */


class ecPlotter : public QGraphicsView
{
  Q_OBJECT

 public:
  /**
   * Default constructor
   *
   * After the ecPlottert is constructed, call setCurve(), ...
   *
   * @param parent This argument is passed on to the constructor of the widget
   */
   ecPlotter(QWidget *parent = nullptr);

   /**
    * draws the coordinate grid, a home-box for points, etc.
    *
    * This method should be called exactly once after the ecPlotter has been
    * constructed
    *
    * @param boundingBox bounds for the coordinate system that is drawn
    */
   void drawGrid(const QRectF &boundingBox);

   /**
    * Add a curve to the widget
    * 
    * After the ecPlotter has been constructed, use this method to add a curve
    * to the widget. The ecPlotter does not take ownership of the curve.
    *
    * This method should be called precisely once. It is possible to delete
    * the curve while the ecPlotter exists, but the ecPlotter will become
    * useless.
    * 
    * @param curve A pointer to the curve which is added to the widget
    */
   void setCurve(ec_real *curve);

   /**
    * Add a free point to the widget
    * 
    * After setCurve() has been called, use this method to add a free point to
    * the widget. The ecPlotter takes ownership of the point, and the point
    * will be deleted as soon as the ecPlotter is deleted.
    *
    * It is possible to delete the point while the ecPlotter exists, but the
    * ecPlotter will become useless.
    * 
    * @param pt A pointer to the free point which is to be added to the widget
    */
   void addFreePoint(ec_realFreePoint *pt);

   /**
    * Add a point to the widget
    * 
    * After setCurve() has been called, use this method to add a free point to
    * the widget. The ecPlotter takes ownership of the point, and the point
    * will be deleted as soon as the ecPlotter is deleted.
    *
    * It is possible to delete the point while the ecPlotter exists, but the
    * ecPlotter will become useless.
    * 
    * @param pt A pointer to the free point which is to be added to the widget
    */
   void addPoint(ec_realPoint *pt);

   /**
    * Paints the ellptic curve and its points to the painter
    *
    * This method paints the elliptic curce and its points to the painter. The
    * poin box is not painted. Axes and Grid are shown depending on the values
    * of showAxes and showGrid
    */
   void paint(QPainter &painter, bool showAxes=true, bool showGrid=true, bool transparent=false);

   /**
    * Returns a box that contains the ellipticcurve, its points and
    * construction lines, but not the point box
    */
   QRectF augmentedBox() const { return bBox.adjusted( -1, -1, 1, 1 ); }


 public slots:
   /**
    * Update GUI items when the curve or the points change. 
    *
    * This methods also sets all user-visible texts.
    */
   void updateGUI();

 protected:
   /**
    * Re-implemented from QWidget
    *
    * This method is re-implemented, in order to adjust the view whenever the
    * size of the widget changes
    */
   void resizeEvent(QResizeEvent *event) override;

 private:
   /**
    * Pointer to the curve which is displayed
    */
     QPointer<ec_real> curve{nullptr};

     /**
    * QGraphicsScene used for the QGraphicsView
    */
     ecGraphicsScene *scene;

     /**
    * List of free points that are shown in the widget
    */
     QList<QPointer<ec_realFreePoint> > freePoints;

     /**
    * Timer used to update the status bar text with a slight delay, so as to avoid
    * flickering.
    */
     QTimer timer;

     /**
    * boundingBox, as set in the drawGrid method
    */
     QRectF bBox;

     QGraphicsSimpleTextItem *label{};
     QGraphicsRectItem *rect{};
     QGraphicsPathItem *grid{};
     QGraphicsPathItem *axes{};
     QGraphicsSimpleTextItem *labelX{};
     QGraphicsSimpleTextItem *labelY{};
};

#endif
