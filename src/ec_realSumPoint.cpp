/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                                  *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QBrush>
#include <QDebug>
#include <QGraphicsLineItem>
#include <QPen>

#include "ec_real.h"
#include "ec_realSumPoint.h"

ec_realSumPoint::ec_realSumPoint(const QRectF &boundingBox,
                                 ec_real *_parentCurve,
                                 ec_realPoint *_parentPoint1,
                                 ec_realPoint *_parentPoint2,
                                 QGraphicsItem *parentItem)
    : ec_realPoint(boundingBox,
                   _parentCurve,
                   parentItem,
                   _parentPoint1->getName() + "+" + _parentPoint2->getName(),
                   true,
                   true)
{
  // Paranoid safety checks
  if (_parentCurve == nullptr) {
      qCritical() << "ec_realSumPoint::ec_realSumPoint called with parentCurve == 0";
  }
  if (_parentPoint1 == nullptr) {
      qCritical() << "ec_realSumPoint::ec_realSumPoint called with parentPoint1 == 0";
  }
  if (_parentPoint2 == nullptr) {
      qCritical() << "ec_realSumPoint::ec_realSumPoint called with parentPoint2 == 0";
  }

  compute      = false;

  parentPoint1 = _parentPoint1;
  parentPoint2 = _parentPoint2;

  
  circle->setBrush(QBrush(Qt::yellow));

  connect(parentPoint1, SIGNAL(changed()), this, SLOT(updateGraphics()));
  connect(parentPoint2, SIGNAL(changed()), this, SLOT(updateGraphics()));
}


QString ec_realSumPoint::description() const
{
  // Paranoid safety checks
  if (parentPoint1.isNull() || parentPoint2.isNull()) {
    qWarning() << "ec_realSumPoint::description() called with one of the parentPoints == 0";
    return {};
  }

  QString descr;
  if (isOffscreen()) {
      descr = tr("The point <b>%1</b> is the sum of the points <b>%2</b> and <b>%3</b> according "
                 "to the group law of the elliptic curve. "
                 "Geometrically, the point <b>%1</b> is constructed by taking the line through "
                 "<b>%2</b> and <b>%3</b>. "
                 "This line will intersect the elliptic curve in the exactly one further point, "
                 "which is offscreen and therefore not "
                 "shown in the picture. The sum <b>%1</b> is then constructed as the image of the "
                 "third point "
                 "under reflection against the x-axis.")
                  .arg(getName(), parentPoint1->getName(), parentPoint2->getName());
  } else {
      descr = tr("The point <b>%1</b> is the sum of the points <b>%2</b> and <b>%3</b> according "
                 "to the group law of the elliptic curve. "
                 "Geometrically, the point <b>%1</b> is constructed by taking the line through "
                 "<b>%2</b> and <b>%3</b>. "
                 "This line will intersect the elliptic curve in the exactly one further point, "
                 "shown as a small yellow circle. The sum "
                 "<b>%1</b> is then constructed as the image of the third point "
                 "under reflection against the x-axis.")
                  .arg(getName(), parentPoint1->getName(), parentPoint2->getName());
  }
  descr += " " + ec_realPoint::description();
  return descr;
}


QString ec_realSumPoint::descriptionForAssociativityConstruction() const
{
  // Paranoid safety checks
  if (parentPoint1.isNull() || parentPoint2.isNull()) {
    qWarning() << "ec_realSumPoint::description() called with one of the parentPoints == 0";
    return {};
  }

  return tr("The figure shows that the point <b>%1</b> can also be constructed as the sum of the points <b>%2</b> and <b>%3</b>. "
	    "This illustrates the fundamental fact that the order does not play any role when adding points on an elliptic "
	    "curve. Mathematicians express this fact by saying that 'addition on an elliptic curve is associative'. "
	    "Associativity is a consequence of 'Noether's residual intersection theorem', named after the german mathematician "
        "Max Noether (1844-1921).").arg(getName(), parentPoint1->getName(), parentPoint2->getName());
}


bool ec_realSumPoint::isComputable() const
{
    if (parentPoint1.isNull() || parentPoint2.isNull()) {
        return false;
    }

  return (parentPoint1->getState() != invalid) && (parentPoint2->getState() != invalid) ;
}


void ec_realSumPoint::setCompute(bool doCompute)
{
    if (doCompute == compute) {
        return;
    }

  compute = doCompute;
  updateGraphics();
}


void ec_realSumPoint::setToolTip()
{
    if (parentPoint1.isNull() || parentPoint2.isNull()) {
        return;
    }

  QString descr;  
  descr = tr("<h4>Auxiliary Line</h4>"
	     "<p>The point <b>%1</b> is constructed as the image of the small yellow point under reflection against the x-axis. "
	     "This auxiliary line connects the points <b>%1</b> and the small yellow point.</p>").arg(getName());
  vertLine->setToolTip(descr);

  descr = tr("<h4>Auxiliary Point</h4>"
	     "<p>This is an auxiliary point used in the construction of the sum <b>%1</b>. "
	     "The sum is constructed by looking at the secant line through <b>%2</b> and <b>%3</b>, "
	     "The secant line is guaranteed to intersect the elliptic curve in precisely one further "
	     "point, the point you are looking at right now.</p>"
	     "<p>Move the mouse pointer to the point <b>%1</b> to learn more about the construction.</p>")
    .arg(getName(), parentPoint1->getName(), parentPoint2->getName());
  smallCircle->setToolTip(descr);

  QString tt;
  tt += "<h4>" + tr("Sum of the points <b>%1</b> and  <b>%2</b>").arg(parentPoint1->getName(), parentPoint2->getName()) + "</h4>";
  tt += "<p>" + description() + "</p>";
  tt += "<p>" + tr("Move one of the points <b>%1</b> or <b>%2</b> to see how the sum changes. "
           "You can also change the shape of the curve to see how the whole construction changes.").arg(parentPoint1->getName(), parentPoint2->getName())
    + "</p>";
  circle->setToolTip(tt);

  tt = tr("<h4>Secant Line</h4>"
	  "<p>This line is the secant line through the points <b>%2</b> and <b>%3</b>, an auxiliary line used in the construction "
      "of the point <b>%1</b>.</p>").arg(getName(), parentPoint1->getName(), parentPoint2->getName());
  if (isOffscreen()) {
      tt += tr("<p>Apart from the points <b>%2</b> and <b>%3</b>, the secant line is guaranteed to "
               "intersect the "
               "elliptic curve in precisely one third point, which is offscreen and therefore not "
               "shown in the picture.</p>"
               "<p>The point <b>%1</b> is constructed as the image of the third intersection under "
               "reflection against the "
               "x-axis.</p>")
                .arg(getName(), parentPoint1->getName(), parentPoint2->getName());
  } else {
      tt += tr("<p>Apart from the points <b>%2</b> and <b>%3</b>, the secant line is guaranteed to "
               "intersect the "
               "elliptic curve in precisely one third point</p><p>Move the mouse pointer to the "
               "point <b>%1</b> to learn "
               "more about the construction.</p>")
                .arg(getName(), parentPoint1->getName(), parentPoint2->getName());
  }
  secantLine->setToolTip(tt);
}


void ec_realSumPoint::updateGraphics()
{
  // Paranoid safety checks
  if (parentCurve.isNull()) {
      qCritical() << "ec_realSumPoint::pointChanged called with parentCurve == 0";
  }
  if (parentPoint1.isNull()) {
      qCritical() << "ec_realSumPoint::pointChanged called with parentPoint1 == 0";
  }
  if (parentPoint2.isNull()) {
      qCritical() << "ec_realSumPoint::pointChanged called with parentPoint2 == 0";
  }

  // Check if we can and if we should be computing
  emit computabilityChanged();
  if ((!compute) || !isComputable()) {
    setInvalid();
    return;
  }

  // If the first parent point is the point at infinity, i.e., the neutral
  // element, then the sum is simply a copy of the second point.
  if (parentPoint1->getState() == infinity) {
    if (parentPoint2->getState() == infinity) {
      secantLine->setLine( QLineF() );
      setInfinity();
      return;
    }
    secantLine->setLine(computeLineSegment(parentPoint2->getCoords(), QPointF(0,1)));
    setCoords( parentPoint2->getCoords() );
    return;
  }

  // If the second parent point is the point at infinity, i.e., the neutral
  // element, then the sum is simply a copy of the first point.
  if (parentPoint2->getState() == infinity) {
    if (parentPoint1->getState() == infinity) {
      secantLine->setLine( QLineF() );
      setInfinity();
      return;
    }
    secantLine->setLine(computeLineSegment(parentPoint1->getCoords(), QPointF(0,1)));
    setCoords( parentPoint1->getCoords() );
    return;
  }

  // If the x-coordinates of the first and the second parent point agree, then
  // the points are equal, or one is the inverse of the other. We treat these
  // cases separately here
  if (qAbs(parentPoint1->getCoords().x() - parentPoint2->getCoords().x()) < 0.01) {
    if (qAbs(parentPoint1->getCoords().y() - parentPoint2->getCoords().y()) < 0.01) {
      // parentPoint1 and parentPoint2 are approximately equal. We should be
      // computing the double instead.
      if (qAbs(parentPoint1->getCoords().y()) < 0.01) {
	// The double of any 2-torsion point is infinity
	secantLine->setLine(computeLineSegment(parentPoint1->getCoords(), QPointF(0,1)));
	setInfinity();
	return;
      }
      secantLine->setLine(computeLineSegment(parentPoint1->getCoords(), parentCurve->tangent(parentPoint1->getCoords())));
      qreal const lambda = (3 * parentPoint1->getCoords().x() * parentPoint1->getCoords().x()
                            + parentCurve->a())
                           / (2 * parentPoint1->getCoords().y());
      qreal const mu = parentPoint1->getCoords().y() - lambda * parentPoint1->getCoords().x();
      qreal const x = lambda * lambda - 2 * parentPoint1->getCoords().x();
      setCoords( parentCurve->pointOnCurve(QPointF(x, -lambda*x-mu)) );
      return;
    }
    
    // parentPoint1 and parentPoint2 are approximately inverse to one
    // another. The sum is this the point at infinity.
    secantLine->setLine(computeLineSegment(parentPoint1->getCoords(), QPointF(0,1)));
    setInfinity();
  } 

  // parentPoint1 and parentPoint2 are neither equal nor inverse to one
  // another. We can compute the sum using the standard formulae.
  secantLine->setLine(computeLineSegment(parentPoint1->getCoords(), parentPoint1->getCoords()-parentPoint2->getCoords() ));
  qreal const s = (parentPoint1->getCoords().y() - parentPoint2->getCoords().y())
                  / (parentPoint1->getCoords().x() - parentPoint2->getCoords().x());
  qreal const xR = s * s - parentPoint1->getCoords().x() - parentPoint2->getCoords().x();
  qreal const yR = -parentPoint1->getCoords().y() + s * (parentPoint1->getCoords().x() - xR);
  setCoords( parentCurve->pointOnCurve(QPointF(xR, yR)) );
}
