/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef INACTIVITYDETECTOR
#define INACTIVITYDETECTOR

#include <QTimer>

#include "ui_aboutDialog.h"


/**
 * inactivityDetector
 *
 * This is a simple event filter that emits a signal when it does not receive a
 * mouse or keyboard event for more than three minutes. When inactivity lasts
 * longer, only one signal is emitted. The class can be used as follows.
 *
 * @code
 *
 *  inactivityDetector *detector = new inactivityDetector(this);
 *  connect(detector, SIGNAL(inactivityDetected()), myObject, SLOT(reactToInactivity()));
 *  qApp->installEventFilter(detector);
 *
 * @endcode
 */

class inactivityDetector : public QObject
{
  Q_OBJECT

public:
  inactivityDetector(QObject *parentQObject = nullptr);

signals:
  /**
   * Emitted when inactivity has been detected.
   */
  void inactivityDetected();
  
protected:
  bool eventFilter(QObject *obj, QEvent *event) override;
  
private:
   QTimer timer;
};

#endif
