/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef EC
#define EC

#include <QObject>


/**
 * Represents a short Weierstrass equation for an elliptic curve, defined over
 * the integers.
 *
 * This class represents an elliptic curve (singular or smooth), defined over
 * the integers. The elliptic curve is given as a short Weierstrass equation,
 * i.e., as an equation of the form y^2 = x^3 + a*x + b. This class contains
 * many convenience functions.
 * 
 * @author Stefan Kebekus
 */

class ec : public QObject
{
  Q_OBJECT
    
 public:
  /**
   * Default constructor
   *
   * The coefficients of the curve are set using the method
   * setRealTypeEllipticWithOval().
   */
  ec(QObject *parent = nullptr);

  /** 
   * Topological type of the curve
   *
   * The short Weierstrass equation represented by this class can describe
   * several types of real curves, some of them elliptic with and without
   * oval, some others singular. This enum is used to describe the possible
   * types.
   */
  enum realType { 
    smoothEllipticWithOval,    /**< The curve is smooth, elliptic and has two connected components */
    smoothEllipticWithoutOval, /**< The curve is smooth, elliptic and has one connected component */
    neilParabola,              /**< The curve is a singular rational curve with a cusp */
    nodalCurve,                /**< The curve is a singular rational curve with a node */
    isolatedPoint,             /**< The curve is a singular curve, one component is an isloalated point */
    invalid                    /**< No equation has been set yet */
  };

  /**
   * Returns the coefficient 'a' of the short Weierstrass equation y^2
   * = x^3 + a*x + b.
   *
   * @see setA()
   */
  int a() const {return _a;};

  /**
   * Returns the coefficient 'b' of the short Weierstrass equation y^2
   * = x^3 + a*x + b.
   *
   * @see setB()
   */
  int b() const {return _b;};

  /**
   * Returns the Delta-invariant of the short Weierstrass equation y^2 = x^3 + a*x + b.
   *
   * The Delta-invariant is defined as Delta = -16*(4*a^3+27*b^2). It
   * has the following meaning.
   *
   * - The curve defined by the equation over a field K is singular if
   *   and only if Delta = 0 in that field.
   *
   * - If K is the real number field and Delta != 0, then the curve
   *   consists of two connected components if and only if Delta >
   *   0. Otherwise, the curve is connected,
   */
  int Delta() const {return -16*(4*_a*_a*_a+27*_b*_b);};

 public slots:
  /**
   * Makes the curve emit the signal "change"
   *
   * The signal "change" is emitted, regardless if the curve has been changed
   * or not
   */
  void touch() { emit changed(this); };
 
 /**
  * Sets the coefficient 'a' of the equation x^3 + a*x + b - y^2.
  *
  * This method sets the coefficient 'a' of the equation x^3 + a*x + b -
  * y^2. The signal 'changed' is emitted.
  * 
  * @param a The new coefficient for the equation. This value must be in the
  * range -10..10. If 'a' is outside of that range, 'a' will be set to either
  * -10 or 10.
  */
  void setA(int a) {setAB(a, _b);};

 /**
  * Sets the coefficient 'a' of the equation x^3 + a*x + b - y^2.
  *
  * This method sets the coefficient 'a' of the equation x^3 + a*x + b -
  * y^2. The signal 'changed' is emitted.
  * 
  * @param b The new coefficient for the equation. This value must be in the
  * range -10..10. If 'b' is outside of that range, 'b' will be set to either
  * -10 or 10.
  */
  void setB(int b) {setAB(_a, b);};

 /**
  * Sets the coefficients 'a' and 'b' of the equation x^3 + a*x + b - y^2.
  *
  * This method sets the coefficients 'a' and 'b' of the equation x^3 + a*x +
  * b - y^2. The signal 'changed' is emitted.
  * 
  * @param a The new coefficient for the equation. This value must be in the
  * range -10..10. If 'a' is outside of that range, 'a' will be set to either
  * -10 or 10.
  * 
  * @param b The new coefficient for the equation. This value must be in the
  * range -10..10. If 'b' is outside of that range, 'b' will be set to either
  * -10 or 10.
  */
  void setAB(int a, int b);

  /**
   * Adjusts the coefficients so that the type of the associated curve defined
   * over the reals is a Neil parabola.
   *
   * The signal "changed" is emitted.
   *
   * @see setAB()
   */
  void setRealTypeNeilParabola() {setAB(0,0);};

  /**
   * Adjusts the coefficients so that the type of the associated curve defined
   * over the reals is a nodal rational curve.
   *
   * The signal "changed" is emitted.
   *
   * @see setAB()
   */
  void setRealTypeNodalCurve() {setAB(-3,2);};

  /**
   * Adjusts the coefficients so that the type of the associated curve defined
   * over the reals is a curve plus an isolated point.
   *
   * The signal "changed" is emitted.
   *
   * @see setAB()
   */
  void setRealTypeIsolatedPt() {setAB(-3,-2);};

  /**
   * Adjusts the coefficients so that the type of the associated curve defined
   * over the reals is smooth parabola.
   *
   * The signal "changed" is emitted.
   *
   * @see setAB()
   */
  void setRealTypeEllipticWithoutOval() {setAB(-6, 10);};

  /**
   * Adjusts the coefficients so that the type of the associated curve defined
   * over the reals is a smooth parabola and an oval.
   *
   * The signal "changed" is emitted.
   *
   * @see setAB()
   */
  void setRealTypeEllipticWithOval() {setAB(-8, 4);};

 signals:
  /**
   * This signal is emitted whenever the curve changes
   *
   * @param curve A pointer to this curve. This pointer is never zero.
   */
  void changed(ec *curve);
  
 private:
  /**
   * Coefficients in the Weierstrass equation
   */
  int    _a{}, _b{};
};

#endif
