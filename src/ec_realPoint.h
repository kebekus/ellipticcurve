/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef EC_REALPOINT
#define EC_REALPOINT 1

#include <QGraphicsSimpleTextItem>
#include <QPointer>
#include <QPointF>

class ec_real;
class QGraphicsEllipseItem;


/**
 * Base class for graphic presentation of a point on an elliptic curve,
 * defined over the real numbers.
 *
 * This class represents a point on a smooth elliptic curve and gives a
 * graphic representation, which includes a conveniently placed label and a
 * nice tooltip. The elliptic curve is set in the constructor and cannot be
 * changed after it the point has been constructed. The point always has one
 * of the following three states
 *
 * - "invalid". No coordinates have been set, the point is not the point at
 *   infinity, or the curve is not a smooth elliptic curve. The state might be
 *   set to invalid automatically if the curve changes drastically. When the
 *   state switches to "invalid", the visibility of the point as a
 *   QGraphicsItem is automatically set to "false".
 *
 * - "infinity". The curve is a smooth elliptic curve, and the point is the
 *   point at infinity. When the state switches to "invalid", the visibility
 *   of the point as a QGraphicsItem is automatically set to "false".
 *
 * - "affine". The curve is a smooth elliptic curve, and the point is not the
 *   point at infinity. When the state switches to "affine", the visibility of
 *   the point as a QGraphicsItem is automatically set to "true".
 *
 * Visibility and the state of the point is set automatically and cannot be
 * set manually. In essence, the point is visible whenever it is affine and
 * invisible otherwise.
 *
 * In addition to the point, this class can also draw a few auxiliary
 * graphical object that can indicate how the point is constructed.
 *
 * @note This class is not really useful by itself. Instead, use one of the
 * derived classes.
 *
 * @author Stefan Kebekus
 */


class ec_realPoint : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)

public:
    /**
   * Default constructor
   *
   * Constructs a point on the curve "parentcurve". The parent curve is
   * mandatory. Once the parent curve is destructed, this object becomes
   * useless. Right after construction, the point is in state 'invalid'.
   *
   * @param boundingBox The point is only shown if it is inside the
   * boundingBox, which is defined here.
   *
   * @param parentCurve This object represents a point on the curve
   * "parentCurve". Changes of the parentCurve are monitored, and coordinates,
   * state and visibility of the point may change when the parentCurve
   * changes. The point is not deleted automatically when the parentCurve is
   * deleted, but becomes useless once the parentCurve is gone. This must be
   * a pointer to ec_real.
   *
   * @param parentItem The parentItem is passed on to the default constructor
   * of the QGraphicsItem class, which this class inherits.
   *
   * @param name The name of the point, used for the label.
   *
   * @param _showVertLine If true, then a small circle is drawn at the inverse
   * of this point and a vertical line connects the small circle with this
   * point, whenever this point is affine and inside the bounding
   * box. Otherwise, nothing is drawn. If this set to TRUE in a subclass, then
   * the subclass should use the pointers vertLine and smallCircle to set
   * appropriate toolTips.
   *
   * @param _showSecantLine If true, then the secantLine becomes visible
   * whenever this point is affine. While visibility of the secantLine is
   * automatically handled, a subclass that wishes to use the secantLine needs
   * to set the position of the line itself. The subclass should also set
   * appropriate toolTips.
   */
    ec_realPoint(const QRectF &boundingBox,
                 QObject* parentCurve,
                 QGraphicsItem *parentItem,
                 const QString &name = QString(),
                 bool _showVertLine = false,
                 bool _showSecantLine = false);

    /**
   * State of the point.
   *
   * The point described by this object may be in several 'states'. This enum
   * lists all possibilities.
   */
    enum stateFlags {
        invalid,  /**< Point or curve is not valid and does not have a graphical
		   representation. No coordinates have been set, the point is
		   not the point at infinity, or the curve is not a smooth
		   elliptic curve. The state might be set to invalid
		   automatically if the curve changes drastically. When the
		   state switches to "invalid", the visibility of the point as
		   a QGraphicsItem is automatically set to "false". A point
		   that is invalid once, will never automatically change its
		   state without user interaction. */
        infinity, /**< Point and curve are valid, point is at infinity and does
		   not have a graphical representation. The curve is a smooth
		   elliptic curve, and the point is the point at
		   infinity. When the state switches to "invalid", the
		   visibility of the point as a QGraphicsItem is automatically
		   set to "false". */
        affine    /**< Point and curve are valid, point is not infinity and has a
		   graphical representation. The curve is a smooth elliptic
		   curve, and the point is not the point at infinity. When the
		   state switches to "affine", the visibility of the point as
		   a QGraphicsItem is automatically set to "true". */
    };

    /**
   * Returns the state of the point.
   */
    ec_realPoint::stateFlags getState() const { return state; }

    /**
   * Coordinate of the point
   *
   * If the state of the point is ec_realPoint::realType::affine, this method
   * returns the approximate coordinate of the point. If the state of the
   * point is not ec_realPoint::realType::affine, this method should not be
   * called, and an undefined vector is returned.
   */
    QPointF getCoords() const;

    /**
   * Get the name of the point
   */
    QString getName() const { return label->text(); }

    /**
   * Represents a description of the point as a string
   *
   * @returns If the state is not 'invalid', a representation of the point as
   * a string with HTML-Tags is returned, but without surrounding
   * <p>-tags. Otherwise, an empty string is returned.
   */
    virtual QString description() const;

    /**
   * Sets this point to state 'invalid'
   */
    void setInvalid();

    /**
   * Reimplemented from QGraphicsItem
   */
    QRectF boundingRect() const override;

    /**
   * Reimplemented from QGraphicsItem
   */
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;


public slots:
    /**
   * Makes the point emit the signal "change"
   *
   * The signal "change" is emitted, regardless if the point has been changed
   * or not
   */
    void touch() { emit changed(); }


signals:
    /**
   * This signal is emitted when the point changes. 
   * 
   * This signal is emitted when the point changes. A change may occur if the
   * user changes the coordinates, or if the curve changes.
   */
    void changed();


protected:
    /**
   * Sets the name of the point
   */
    void setName(const QString& name);

    /**
   * Sets this point to state 'infinity'
   *
   * Attempts to set this point to 'infinity'. The attempt will fail if the
   * curve is not an elliptic curve.
   *
   * @returns true, if the change was successful, false otherwise
   */
    bool setInfinity();

    /**
   * Set coordinates of the point
   *
   * Attempts to set the coordinate of the point. If the coordinate is close
   * the elliptic curve, the coordinate is accepted. Otherwise, the attempt
   * fails and the point remains unchanged.
   *
   * @returns true, if the change was successful, false otherwise
   */
    bool setCoords(QPointF pt);

    /**
   * Returns the bounding box set in the constructor
   */ 
    QRectF getBoundingBox() { return boundingBox; }


protected slots:
    /**
   * Update the point and its state when the curve changes
   *
   * Derived classes need to implement this method, to ensure that the point
   * properly changes its state whenever the curve changes. To constructor
   * will connect the parent curve's "changed()" signal to this slot.
   */
    virtual void updateGraphics() = 0;

    /**
   * Sets the tooltip of the point
   *
   * The slot is called whenever the point changes, so that toolTips can be
   * updated.  This implementation takes the string returned by description()
   * and sets this as the ToolTip for the circle. Classes which inherit from
   * ec_realPoint can re-implement this function to set more elaborate
   * ToolTips, or to set ToolTips on other graphical items.
   *
   */
    virtual void setToolTip();


protected:
    /**
   * Pointer to the parent curve
   */
    QPointer<ec_real>        parentCurve;

    /**
   * Utility method that computes a segment of a line such that the line
   * segement exactly fits the boundingBox
   *
   * Given a line (specified as a point and a direction), this method computes
   * the segement of the line which lies inside the given boundingBox.
   *
   * @param point A point on the line. This point must lie inside the
   * boundingBox. Otherwise, a random line segment is returned.
   * 
   * @param direction The direction vector for the line. If the length of this
   * vector is smaller than 1/100, a random result is returned.
   *
   * @returns The largest segement of the line that lies inside the
   * boundingBox.
   */
    QLineF computeLineSegment(QPointF point, QPointF direction) const;

    /**
   * Checks if a point lies in the boundingBox
   */
    bool isOffscreen(QPointF pt) const { return !boundingBox.contains(pt); }

    /**
   * Checks if this lies in the boundingBox
   */
    bool isOffscreen() const { return !boundingBox.contains(coords); }

    /**
   * Graphical representation of the point
   *
   * This QGraphicsEllipseItem contains the graphical representation of the
   * point. It is completely controlled by this class. Classes that inherit
   * ec_realPoint should not try to modify shape or visibility of this
   * object. The reason that the circle is accessible to subclasses is that
   * subclasses should make all graphical objects that are associated to this
   * point (i.e. extra text labels, or auxiliary lines that illustrate the
   * construction of the point) and which they want to be hidden and shwon
   * automatically as the state of the point changes, children of the circle.
   *
   * - The class ec_realPoint will hide/show the circle whenever the state of
   * the point changes. In particular, the circle and all its children will be
   * hidden whenever the point becomes invalid or infinity. Subclasses thus do
   * not need to worry about hiding their extra graphical entities. The class
   * ec_realPoint will never touch it's own visibility, which is left
   * completely to be controlled by the use who owns the ec_realPoint.
   *
   * - With this setup, there are two levels of visibility control: the user
   * of a ec_realPoint can use ec_realPoint::setVisibile() anytime without
   * worrying that calling ec_realPoint::setVisibile(true) on an invalid point
   * might uncover graphical garbarge. In fact, calling
   * ec_realPoint::setVisibile(true) on an invalid point will not show
   * anything since the circle and all its children are hidden. Once the state
   * changes to affine, everything will automatically be shown.
   */
    QGraphicsEllipseItem * circle;

    /**
   * Auxiliary line, drawn to show the secant
   *
   * This element is constructed in the constructor of *this, so that the
   * pointer is always valid. Visibility of this entity if controlled by the
   * updateVisibility() method, which is in turn connected to the changed()
   * signal. In summary, that means that visibility of this entity is handled
   * automagically, nothing need to be done manually by us.  The only reason
   * that this member is protected rather than private is that classes that
   * derive from ec_realPoint can set the secantLines' position and set an
   * appropriate toolTip.
   */
    QGraphicsLineItem * secantLine;

    /**
   * Auxiliary line, drawn it indicate relflection along the x-axis
   *
   * This element is constructed in the constructor of *this, so that the
   * pointer is always valid. Visibility of this entity if controlled by the
   * updateVisibility() method, which is in turn connected to the changed()
   * signal. In summary, that means that visibility of this entity is handled
   * automagically, nothing need to be done manually by us.  The position and
   * length of the vertical line is also set automatically. The only reason
   * that this member is protected rather than private is that classes that
   * derive from ec_realPoint can set an appropriate toolTip
   */
    QGraphicsLineItem * vertLine;

    /**
   * Auxiliary circle that marks the point that needs to be inverted
   *
   * This element is constructed in the constructor of *this, so that the
   * pointer is always valid. The smallCircle line is set automatically, and
   * its visibility is cotrolled by this class. The position of the
   * smallCircle is also set be this automagically. The only reason that this
   * member is protected rather than private is that classes that derive from
   * ec_realPoint can set an appropriate toolTip
   */
    QGraphicsEllipseItem * smallCircle;

    /**
   * Copy of the bounding box specified in the constructor
   */
    QRectF boundingBox;


private slots:

    /**
   * Sets visibility of the secantLine, the smallCircle and the vertical line
   *
   * This slot is connected to the changed() signal of this slot. Whenever
   * this point changes, this method automatically sets the visbility of the
   * secantLine, the smallCircle and the vertical line, so not manual
   * programming is needed.
   */
    void updateVisibility();


private:
    /**
   * Compute and set the label's position
   *
   * This method computes and sets the label's position in a way that avoids
   * overlap with the parent curve.
   */
    void positionLabel();

    /**
   * State of the point
   */
    ec_realPoint::stateFlags state;

    /**
   * Coordinates of the point
   */
    QPointF                  coords;

    /**
   * Text label
   */
    QGraphicsSimpleTextItem * label;

    /**
   * Indicate if the vertical line is ever shown
   *
   * This member is set in the constructor, never touched afterwards, and only
   * read by the updateVisibility() slot. If true, then the smallCircle and
   * the verticalLine are shown whenver this point is affine and on-screen.
   */
    bool showVertLine;

    /**
   * Indicate if the secant line is ever shown
   *
   * This member is set in the constructor, never touched afterwards, and only
   * read by the updateVisibility() slot. If true, then the secantLine is
   * shown whenver this point is affine.
   */
    bool showSecantLine;
};

#endif
