<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_TW">
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="68"/>
        <source>Sketching elliptic curves over the real numbers</source>
        <translation>描繪實數平面上的橢圓曲線</translation>
    </message>
    <message>
        <location filename="main.cpp" line="73"/>
        <source>This program comes with ABSOLUTELY NO WARRANTY; for details
use the menu item &apos;About the Elliptic Curve Plotter&apos; from
the &apos;Help&apos; menu. This is free software, and you are welcome
to redistribute it under certain conditions; use the menu
item &apos;About the Elliptic Curve Plotter&apos; from the &apos;Help&apos;
menu for details.

Usage: %1 [Options]

-help  Show this help text.

-kiosk Run the in Kiosk Mode, a restricted full-screen mode
       useful for running the program on terminals or kiosks
       that are open to the public.

</source>
        <translatorcomment>&lt;p&gt;Kiosk 模式是用何在公共場合展示Elliptic Curve Plotter的安全模式&lt;/p&gt;&lt;ul&gt;&lt;li&gt;它將會鎖住數項功能選項，如在公眾場合使用Kiosk模式可避免使用者存取其他檔案或離開軟體。&lt;/li&gt;&lt;li&gt;使用者界面在閒置三分鐘之後自動重設&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;&lt;b&gt;僅能藉由關閉軟體來結束Kiosk模式。&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;提示：&lt;/b&gt;如想使用Kiosk模式開啟Elliptic Curve Plotter時不顯示這個警告標語。請使用命令行&lt;b&gt;-kiosk&lt;/b&gt;開啟Elliptic Curve Plotter。&lt;/p&gt;</translatorcomment>
        <translation>本軟體不提供任何更新或保固的承諾。請點選說明選項裡的「關於Elliptic Curve Plotter」以獲得更多資訊。 此免費軟體可在特定條件下分享給他人，請使用說明選項內的「關於Elliptic Curve Plotter」以獲得更多資訊。

使用: %1 [Options]

說明  顯示說明文件.

-kiosk 啟動Kiosk模式, (全螢幕安全模式)
       在公開場合用使用終端機模式或kiosk 開啟軟體時相當有用

</translation>
    </message>
</context>
<context>
    <name>aboutDialog</name>
    <message>
        <location filename="aboutDialog.ui" line="14"/>
        <source>About the Elliptic Curve Plotter</source>
        <translation>關於Elliptic Curve Plotter</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="24"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;;&quot;&gt;Sketching elliptic curves over the real numbers.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;;&quot;&gt;描繪實數平面上的橢圓曲線&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="74"/>
        <source>Author</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="89"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:16px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/kebekus.png&quot; style=&quot;float: right;&quot; /&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Stefan Kebekus&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:600;&quot;&gt;Address&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;Stefan Kebekus&lt;br /&gt;Albert-Ludwigs-Universität Freiburg&lt;br /&gt;Ernst-Zermelo-Straße 1&lt;br /&gt;79104 Freiburg im Breisgau&lt;br /&gt;GERMANY&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:600;&quot;&gt;E-Mail&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;mailto:stefan.kebekus@math.uni-freiburg.de&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; text-decoration: underline; color:#0057ae;&quot;&gt;stefan.kebekus@math.uni-freiburg.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:600;&quot;&gt;Homepage&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://cplx.vm.uni-freiburg.de&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; text-decoration: underline; color:#0057ae;&quot;&gt;https://cplx.vm.uni-freiburg.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:16px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/kebekus.png&quot; style=&quot;float: right;&quot; /&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Stefan Kebekus&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Address&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Stefan Kebekus&lt;br /&gt;Albert-Ludwigs-Universität Freiburg&lt;br /&gt;Ernst-Zermelo-Straße 1&lt;br /&gt;79104 Freiburg im Breisgau&lt;br /&gt;GERMANY&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;E-Mail&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;mailto:stefan.kebekus@math.uni-freiburg.de&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;stefan.kebekus@math.uni-freiburg.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Homepage&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://home.mathematik.uni-freiburg.de/kebekus&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://home.mathematik.uni-freiburg.de/kebekus&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:16px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/kebekus.png&quot; style=&quot;float: right;&quot; /&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Stefan Kebekus&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;通訊地址&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Stefan Kebekus&lt;br /&gt;Albert-Ludwigs-Universität Freiburg&lt;br /&gt;Ernst-Zermelo-Straße 1&lt;br /&gt;79104 Freiburg im Breisgau&lt;br /&gt;GERMANY&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;聯絡方式&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;mailto:stefan.kebekus@math.uni-freiburg.de&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;stefan.kebekus@math.uni-freiburg.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;首頁&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://cplx.vm.uni-freiburg.de&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;https://cplx.vm.uni-freiburg.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="113"/>
        <source>Acknowledgements</source>
        <translation>銘謝</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="119"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Inspiration&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;This program was inspired by Java applets included in the &amp;quot;ECC Tutorial&amp;quot; found on the homepage of Certicom Corp. This program does not share code or other components with any of Certicom&apos;s products.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;Link:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;monospace&apos;; font-size:9pt;&quot;&gt; &lt;/span&gt;&lt;a href=&quot;http://www.certicom.com/index.php/ecc-tutorial&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.certicom.com/index.php/ecc-tutorial&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Artwork&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;Several icons used in this program are versions of icons found in the &lt;/span&gt;&lt;a href=&quot;www.kde.org&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; text-decoration: underline; color:#0057ae;&quot;&gt;K Desktop Environment&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;, Version 4.0, or modified version of these icons.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Inspiration&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;本程式計畫乃受到數個&amp;quot;ECC Tutorial&amp;quot;計畫內的JAVA小程式的啟發，&amp;quot;ECC Tutorial&amp;quot;可於Certicom Corp的首頁裡尋得。本程式並沒有與Certicom的任何產品分享任何程式碼或組件。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;連結:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;monospace&apos;;&quot;&gt; &lt;/span&gt;&lt;a href=&quot;http://www.certicom.com/index.php/ecc-tutorial&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.certicom.com/index.php/ecc-tutorial&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;美工&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;許多程式內的圖示是來自或改自第四版的&lt;/span&gt;&lt;a href=&quot;www.kde.org&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;K Desktop Environment&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;&lt;/span&gt;&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="144"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Translations&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;The following people have kindly helped to translate the Elliptic Curve Plotter into other languages.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;翻譯&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;以下人員熱心地協助翻譯$NAME&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="172"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;license&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:600;&quot;&gt;Icons&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;Several icons used in this program are modified versions of icons distributed with the K Desktop Environment, Version 4.0. These icons are licensed under GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Inspiration&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;This program was inspired by Java applets included in the &amp;quot;ECC Tutorial&amp;quot; found on the homepage of Certicom Corp. This program does not share code or other components with any of Certicom&apos;s products.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Link:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;monospace&apos;;&quot;&gt; &lt;/span&gt;&lt;a href=&quot;http://www.certicom.com/index.php/ecc-tutorial&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.certicom.com/index.php/ecc-tutorial&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Artwork&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Several icons used in this program are versions of icons found in the &lt;/span&gt;&lt;a href=&quot;www.kde.org&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;K Desktop Environment&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;, Version 4.0, or modified version of these icons.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Software used in the development&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/ermine.png&quot; style=&quot;float: right;&quot; /&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;The portable binaries for Linux were created using the program &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Ermine Light&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;. The authors of Ermine Light kindly supported the development of the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Elliptic CurvePlotter&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt; with a free license.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Link:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;monospace&apos;;&quot;&gt; &lt;/span&gt;&lt;a href=&quot;http://www.magicermine.com&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.magicermine.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Inspiration&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;本程式計畫乃受到數個&amp;quot;ECC Tutorial&amp;quot;計畫內的JAVA小程式的啟發，&amp;quot;ECC Tutorial&amp;quot;可於Certicom Corp的首頁裡尋得。本程式並沒有與Certicom的任何產品分享任何程式碼或組件。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;連結:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;monospace&apos;;&quot;&gt; &lt;/span&gt;&lt;a href=&quot;http://www.certicom.com/index.php/ecc-tutorial&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.certicom.com/index.php/ecc-tutorial&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;美工&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;許多程式內的圖示是來自或改自第四版的&lt;/span&gt;&lt;a href=&quot;www.kde.org&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;K Desktop Environment&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;開發時使用軟體&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/ermine.png&quot; style=&quot;float: right;&quot; /&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;軟體中所使用的 Linux平台的可攜式執行檔乃是使用&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Ermine Light&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;所開發而成。Ermine Light 的作者亦同時親切地協助開發&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Elliptic CurvePlotter&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;並給予自由授權.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Link:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;monospace&apos;;&quot;&gt; &lt;/span&gt;&lt;a href=&quot;http://www.magicermine.com&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.magicermine.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="138"/>
        <source>Translations</source>
        <translation>協同翻譯</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Translations&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The following people have kindly helped to translate the Elliptic Curve Plotter into other languages.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;翻譯&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;以下人員熱心地協助翻譯Elliptic Curve Plotter&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="160"/>
        <source>License</source>
        <translation>授權條款</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;license&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Copyright (C) 2018 Stefan Kebekus&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Icons&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Several icons used in this program are modified versions of icons distributed with the K Desktop Environment, Version 4.0. These icons are licensed under GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>Several icons used in this program are modified versions of icons distributed with the K Desktop Environment, Version 4.0. These icons are licensed under GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translatorcomment>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;license&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;版權 (C) 2018 Stefan Kebekus&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;.
本程式係一個免費軟體，你可在自由軟體基金會所發行的第三版(或更新的)公用通種授權條款的規範下更改或散布此軟體。&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;本程式本於有益於群眾的的自我期許下散布，但不包含任何如可銷售性或針對特定用途的適用性等默示擔保。詳見公用通種授權條款。
&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;圖示&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;許多用於軟體中的圖示乃改自第四版的K Desktop Environment，自由軟體基金會所發行的第三版(或更新的)公用通種授權條款的規範&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="201"/>
        <source>GPL</source>
        <translation>公用通眾授權條款</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="342"/>
        <source>FDL</source>
        <translation>自由文件授權條款</translation>
    </message>
</context>
<context>
    <name>ecDescription</name>
    <message>
        <location filename="ecDescription.cpp" line="85"/>
        <source>Points on the elliptic curve</source>
        <translation>橢圓曲線上的點</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="89"/>
        <source>Experiment with the curve and its group law!</source>
        <translation>實作橢圓曲線與其群結構！</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="91"/>
        <source>Use the sliders to change the shape of the curve.</source>
        <translation>藉由滑桿調整參數以改變曲線的形狀。</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="93"/>
        <source>Use the sliders to change the shape of the curve, or drag some points to the curve and use the menu &apos;Group Law&apos; to experiment with the group law.</source>
        <translation>藉由滑桿調整參數以改變曲線的形狀， 或移動曲線上的點並使用「群結構」選項操作其群結構。</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="105"/>
        <source>The inverse of the point &lt;b&gt;P&lt;/b&gt;</source>
        <translation>&lt;b&gt;P&lt;/b&gt;點的反元素</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="118"/>
        <source>The double of the point &lt;b&gt;P&lt;/b&gt;</source>
        <translation>計算兩倍的&lt;b&gt;P&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="135"/>
        <source>Sums of points</source>
        <translation>總和</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="154"/>
        <source>Sums of three points</source>
        <translation>三點總和</translation>
    </message>
</context>
<context>
    <name>ecPlotter</name>
    <message>
        <location filename="ecPlotter.cpp" line="187"/>
        <source>Point Box</source>
        <translation>收納箱</translation>
    </message>
    <message>
        <location filename="ecPlotter.cpp" line="189"/>
        <source>&lt;h4&gt;Point Box&lt;/h4&gt;&lt;p&gt;Drag the points from this box to the curve and vice versa.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;收納箱&lt;/h4&gt;&lt;p&gt;從箱內抓取點放在曲線上或將線上的點放回收納箱。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ecPlotter.cpp" line="215"/>
        <source>Drag the points to the curve, and use the &apos;Group Law&apos; menu to experiment with the group structure.</source>
        <translation>將點拉至曲線上並使用「群運算」選項去操作曲線的群結構。</translation>
    </message>
    <message>
        <location filename="ecPlotter.cpp" line="217"/>
        <source>Drag the points to the curve.</source>
        <translation>將點拉至曲線上。</translation>
    </message>
    <message>
        <location filename="ecPlotter.cpp" line="220"/>
        <source>Drag the points along the curve, and use the &apos;Group Law&apos; menu to experiment with the group structure.</source>
        <translation>拉動曲線上的點並使用「群運算」選項操作曲線的群結構。</translation>
    </message>
</context>
<context>
    <name>ec_real</name>
    <message>
        <location filename="ec_real.cpp" line="66"/>
        <source>&lt;h4&gt;Elliptic Curve&lt;/h4&gt;&lt;p&gt;The curve outlined in black is the elliptic curve defined by the equation&lt;/p&gt;&lt;p&gt;%1.&lt;/p&gt; </source>
        <translation>&lt;h4&gt;橢圓曲線&lt;/h4&gt;&lt;p&gt;黑色曲線為方程式&lt;/p&gt;&lt;p&gt;%1&lt;/p&gt;&lt;/p&gt;所決定的橢圓曲線&lt;/p&gt; </translation>
    </message>
    <message>
        <location filename="ec_real.cpp" line="70"/>
        <source>&lt;h4&gt;Curve&lt;/h4&gt;&lt;p&gt;The curve outlined in black is the (non-elliptic) curve defined by the equation&lt;/p&gt;&lt;p&gt;%1.&lt;/p&gt; </source>
        <translation>&lt;h4&gt;曲線&lt;/h4&gt;&lt;p&gt;黑色曲線為方程式&lt;/p&gt;&lt;p&gt;%1&lt;/p&gt;&lt;/p&gt;所對應的曲線(非橢圓曲線)&lt;/p&gt; </translation>
    </message>
    <message>
        <location filename="ec_real.cpp" line="95"/>
        <source>&lt;h2&gt;Neil Parabola&lt;/h2&gt;&lt;p&gt;This curve is not an elliptic curve. It is a singular curve,  known as the &lt;span style=&quot;font-style: italic;&quot;&gt;Neil parabola&lt;/span&gt;  or &lt;span style=&quot;font-style: italic;&quot;&gt;semicubical parabola&lt;/span&gt;.  The singularity at the origin is usually called a &lt;span    style=&quot;font-style: italic;&quot;&gt;cusp&lt;/span&gt;. The Neil parabola is a  rational curve. This means that it can be parameterized using only  rational functions and without using transzendental functions such  as sine or cosine. One such parameterization is given as t &amp;#8594;  (t&amp;sup2;, t&amp;sup3;).&amp;nbsp;&lt;/p&gt;&lt;p&gt;This curve does not have a group structure. There is, however, a  group structure on the smooth locus of the curve. This group is  isomorphic to the group of real numbers with addition. The Neil  parabola is named after William Neil (1637&amp;#8212;1670) who first  computed its arc length. Historically, the Neil parabola was the  first algebraic curve whose arc length could be computed.&lt;/p&gt;</source>
        <translation>&lt;h2&gt;尼爾拋物線&lt;/h2&gt;&lt;p&gt;&lt;span style=&quot;font-style: italic;&quot;&gt;尼爾拋物線&lt;/span&gt;  (或稱 &lt;span style=&quot;font-style: italic;&quot;&gt;半立方拋物線&lt;/span&gt;)並非一條橢圓曲線，因為這條曲線有奇異點。這條曲線位於原點的唯一奇異點通常被稱為一個&lt;span    style=&quot;font-style: italic;&quot;&gt;尖點&lt;/span&gt;。尼爾拋物線實際上是一條有理曲線，亦即這條曲線的參數式可用一個有理函數來表示而無須使用如正弦函數與餘弦函數等超越函數。實際上我們可使用t &amp;#8594;  (t&amp;sup2;, t&amp;sup3;).&amp;nbsp;作為參數。&lt;/p&gt;&lt;p&gt;很可惜這條曲線並沒有群結構，但我們依舊能在奇異點以外的地方定義群結構。事實上，在尼爾拋物線扣除奇異點的集合與其被賦予的群結構，是等同實數加法群的。威廉 尼爾(1637&amp;#8212;1670)計算了這類型曲線的弧線長，為了紀念他所以將這類曲線命名為尼爾拋物線。同時，尼爾拋物線也是歷史上第一個被成功計算出弧長的代數曲線。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_real.cpp" line="112"/>
        <source>&lt;h2&gt;Nodal Plane Cubic&lt;/h2&gt;&lt;p&gt;This curve is not an elliptic curve. It is a singular curve,  known as the &lt;span style=&quot;font-style: italic;&quot;&gt;Nodal Plane Cubic&lt;/span&gt;.  The singularity is usually called a &lt;span style=&quot;font-style:    italic;&quot;&gt;node&lt;/span&gt;.&lt;/p&gt;&lt;p&gt;The nodal plane cubic is a rational curve. This means that it can  be parameterized using only rational functions and without using  transzendental functions such as sine or cosine. One such  parameterization is given as t &amp;#8594; ( t&amp;sup2;-2, t&amp;sup3;-3t).&lt;/p&gt;&lt;p&gt; This curve does not have a group structure. There is, however, a  group structure on the smooth locus of the curve. This group is  isomorphic to the group of non-zero real numbers with  multiplication.&lt;/p&gt;</source>
        <translation>&lt;h2&gt;節點平面三次曲線&lt;/h2&gt;&lt;p&gt;這條被通稱為&lt;span style=&quot;font-style: italic;&quot;&gt;節點平面三次曲線&lt;/span&gt;的曲線並非一條橢圓曲線，因為他包含有奇異點，而這個奇異點通常被稱為一個&lt;span style=&quot;font-style:    italic;&quot;&gt;節&lt;/span&gt;.&lt;/p&gt;。&lt;p&gt;節點平面三次曲線實際上是一條有理曲線，亦即這條曲線的參數式可用一個有理函數來表示而無須使用如正弦函數與餘弦函數等超越函數。實際上我們可使用t &amp;#8594;  (t&amp;sup2;, t&amp;sup3;).&amp;nbsp;作為參數。&lt;/p&gt;&lt;p&gt;很可惜這條曲線並沒有群結構，但我們依舊能在奇異點以外的地方定義群結構。事實上，節點三次曲線扣除奇異點的集合與其被賦予的群結構，是等同非零實數所形成的乘法群。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_real.cpp" line="126"/>
        <source>&lt;h2&gt;Curve with Isolated Point&lt;/h2&gt;&lt;p&gt;This is a singular curve, and not a smooth elliptic curve. Over the  real numbers, the equation defines the curve that you see on the left,  and an additional point with coordinates (-1, 0). If you move the  slider for the value &lt;span style=&quot;font-style: italic;&quot;&gt;b&lt;/span&gt; a  little you can see that the point is a degenerate form of the oval that  you see in many smooth elliptic curves.&lt;/p&gt;&lt;p&gt;The curve with the isolated point does not form a group.&lt;/p&gt;</source>
        <translation>&lt;h2&gt;擁有獨立個點的曲線&lt;/h2&gt;&lt;p&gt;這是一條帶有奇異點的曲線。方程式在實數平面上的零點，給出左邊的曲線與座標為 (-1, 0) 的獨立點。如果你微調&lt;span style=&quot;font-style: italic;&quot;&gt;b&lt;/span&gt;的值，你將會發現這一個獨立點是來自「橢圓」部份退化縮小而來。&lt;/p&gt;&lt;p&gt;擁有獨立個點的曲線上的點並不擁有群結構。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_real.cpp" line="135"/>
        <source>&lt;h2&gt;Elliptic Curve with Oval&lt;/h2&gt;&lt;p&gt;This curve is a smooth elliptic curve. The points of this curve  form a group.&lt;/p&gt;&lt;p&gt;This elliptic curve consists of two parts, classically called  &quot;oval&quot; and &quot;parabola&quot;. Interestingly, it is not possible to define  the oval or the parabola alone with algebraic equations: any  polynomial function that vanishes along one component must  necessarily also vanish along the other.&lt;/p&gt;</source>
        <translation>&lt;h2&gt;擁有獨立橢圓的橢圓曲線&lt;/h2&gt;&lt;p&gt;這是一條光滑橢圓曲線，且曲線上的點能形成一個群。&lt;/p&gt;&lt;p&gt;這條曲線有兩個部份，通常我們稱之為「橢圓」部份與「拋物線」部份。有趣的是，我們是找不到一個多項式單獨定義其中一部分，換句話說，假如有一個多項式，在「橢圓」部份為零，則該多項式亦在「拋物線」部份上為零，反之亦然。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_real.cpp" line="144"/>
        <source>&lt;h2&gt;Elliptic Curve without Oval&lt;/h2&gt;&lt;p&gt;This curve is a smooth elliptic curve. The points of this curve  form a group.&lt;/p&gt;</source>
        <translation>&lt;h2&gt;無獨立橢圓的橢圓曲線&lt;/h2&gt;&lt;p&gt;這是一條光滑橢圓曲線，且曲線上的點形成一個群。&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>ec_realDoublePoint</name>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="59"/>
        <source>The point &lt;b&gt;%1&lt;/b&gt; is the double of the point &lt;b&gt;%2&lt;/b&gt; according to the group law of the elliptic curve. Geometrically, the point &lt;b&gt;%1&lt;/b&gt; is constructed by taking the tangent line through &lt;b&gt;%2&lt;/b&gt;. This tangent line intersects the elliptic curve in the point &lt;b&gt;%2&lt;/b&gt;, and in the point at infinity.</source>
        <translation>點&lt;b&gt;%1&lt;/b&gt;是點&lt;b&gt;%2&lt;/b&gt;在橢圓曲線群結構下的兩倍。幾何上，構造點&lt;b&gt;%1&lt;/b&gt;的方法為：取通過&lt;b&gt;%2&lt;/b&gt;的切線，這條切線會在橢圓曲線上交在點&lt;b&gt;%2&lt;/b&gt;與無窮遠點。</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="64"/>
        <source>The point &lt;b&gt;%1&lt;/b&gt; is the double of the point &lt;b&gt;%2&lt;/b&gt; according to the group law of the elliptic curve. Geometrically, the point &lt;b&gt;%1&lt;/b&gt; is constructed by taking the tangent line through &lt;b&gt;%2&lt;/b&gt;. This tangent line will intersect the elliptic curve in the point &lt;b&gt;%2&lt;/b&gt;, and in exactly one second point. The double of &lt;b&gt;%2&lt;/b&gt; is then constructed as the image of the second point under reflection against the x-axis.</source>
        <translation>點&lt;b&gt;%1&lt;/b&gt;是點&lt;b&gt;%2&lt;/b&gt;在橢圓曲線群結構下的兩倍。幾何上，構造點&lt;b&gt;%1&lt;/b&gt;的方法為：取通過&lt;b&gt;%2&lt;/b&gt;的切線，此切線與橢圓曲線會正好交在&lt;b&gt;%1&lt;/b&gt;與第二點！則點&lt;b&gt;%2&lt;/b&gt;的兩倍即為第二點對 x-軸的鏡射。</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="102"/>
        <source>&lt;h4&gt;Auxiliary Line&lt;/h4&gt;&lt;p&gt;The point &lt;b&gt;%1&lt;/b&gt; is constructed as the image of the small yellow point under reflection against the x-axis. This auxiliary line connects the point &lt;b&gt;%1&lt;/b&gt; and the small yellow point.&lt;/p&gt;&lt;p&gt;Move the mouse pointer to the point &lt;b&gt;%1&lt;/b&gt; to learn more about the construction.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;輔助線&lt;/h4&gt;&lt;p&gt;點 &lt;b&gt;%1&lt;/b&gt; 為小黃點對 x-軸的鏡射點。輔助線通過點&lt;b&gt;%1&lt;/b&gt;與小黃點。&lt;/p&gt;&lt;p&gt;將游標移至點&lt;b&gt;%1&lt;/b&gt;上以獲取更多資訊。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="108"/>
        <source>&lt;h4&gt;Auxiliary Point&lt;/h4&gt;&lt;p&gt;This is an auxiliary point used in the construction of the double of the point &lt;b&gt;%1&lt;/b&gt;. The double of &lt;b&gt;%1&lt;/b&gt; is constructed by looking at the tangent line through &lt;b&gt;%1&lt;/b&gt;. The tangent line is guaranteed to intersect the elliptic curve in the point &lt;b&gt;%1&lt;/b&gt; and in precisely one second point, the point you are looking at right now.&lt;/p&gt;&lt;p&gt;To learn more about the construction, move the mouse pointer to the tangent line or to the point &lt;b&gt;%2&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;輔助點&lt;/h4&gt;&lt;p&gt;這一個輔助點是用來構造兩倍的點 &lt;b&gt;%1&lt;/b&gt;。構造兩倍的點 &lt;b&gt;%1&lt;/b&gt; 的方法為取通過點 &lt;b&gt;%1&lt;/b&gt; 的切線，這一條切線將會交橢圓曲線於點 &lt;b&gt;%1&lt;/b&gt; 與你現在看到的這一個點。&lt;/p&gt;&lt;p&gt;將游標移至切線或點&lt;b&gt;%2&lt;/b&gt;上以獲取更多關於構造的資訊。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="118"/>
        <source>&lt;h4&gt;Double of the point &lt;b&gt;%1&lt;/b&gt;&lt;/h4&gt;</source>
        <translation>&lt;h4&gt;兩倍的點&lt;b&gt;%1&lt;/b&gt;&lt;/h4&gt;</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="120"/>
        <source>&lt;p&gt;Move the point &lt;b&gt;%1&lt;/b&gt; to see how the double of &lt;b&gt;%1&lt;/b&gt; changes along with &lt;b&gt;%1&lt;/b&gt;. You can also use the sliders to change the shape of the elliptic curve, and to see how the whole construction changes.&lt;/p&gt;</source>
        <translation>&lt;p&gt;藉由移動點&lt;b&gt;%1&lt;/b&gt;來看點&lt;b&gt;%1&lt;/b&gt;的兩倍如何隨著點&lt;b&gt;%1&lt;/b&gt;而改變。你亦可使用滑桿改變橢圓曲線的形狀，來觀察整個結構是如何改變的。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="125"/>
        <source>&lt;h4&gt;Tangent Line&lt;/h4&gt;&lt;p&gt;This line is the tangent line through &lt;b&gt;%2&lt;/b&gt;, an auxiliary line used in the construction of the point &lt;b&gt;%1&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;切線&lt;/h4&gt;&lt;p&gt;這條線是通過點&lt;b&gt;%2&lt;/b&gt;的切線，我們使用這條輔助線來構造點&lt;b&gt;%1&lt;/b&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="129"/>
        <source>&lt;p&gt;The tangent line is the unique line which contains &lt;b&gt;%1&lt;/b&gt; and just touches the ellipticcurve, without intersecting it transversely. The tangent line is guaranteed to intersect the elliptic curve in the point &lt;b&gt;%1&lt;/b&gt; and in precisely one second point, which is offscreen and therefore not shown in the picture.&lt;/p&gt;&lt;p&gt;The point &lt;b&gt;%2&lt;/b&gt; is constructed as the image of the second intersection under reflection against the x-axis.&lt;/p&gt;</source>
        <translation>&lt;p&gt;切線為唯一相切橢圓曲線於&lt;b&gt;%1&lt;/b&gt;的直線，這條直線將會正好與橢圓曲線交在點&lt;b&gt;%1&lt;/b&gt;與因超出螢幕所以沒有顯示在圖片上的另一點。&lt;/p&gt;&lt;p&gt;點&lt;b&gt;%2&lt;/b&gt;為另一點對x-軸的鏡射點。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="135"/>
        <source>&lt;p&gt;The tangent line is the unique line which contains &lt;b&gt;%1&lt;/b&gt; and just touches the ellipticcurve, without intersecting it transversely. The tangent line is guaranteed to intersect the elliptic curve in the point &lt;b&gt;%1&lt;/b&gt; and in precisely one second point.&lt;/p&gt;&lt;p&gt;Move the mouse pointer to the point &lt;b&gt;%2&lt;/b&gt; to learn more about the construction.&lt;/p&gt;</source>
        <translation>&lt;p&gt;切線為唯一相切橢圓曲線於&lt;b&gt;%1&lt;/b&gt;的直線，這條直線將會正好與橢圓曲線交在點&lt;b&gt;%1&lt;/b&gt;與另一點。&lt;/p&gt;&lt;p&gt;將游標移動至點&lt;b&gt;%2&lt;/b&gt;上以獲得更多關於構造的資訊&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>ec_realFreePoint</name>
    <message>
        <location filename="ec_realFreePoint.cpp" line="71"/>
        <source>Point</source>
        <translation>點</translation>
    </message>
    <message>
        <location filename="ec_realFreePoint.cpp" line="73"/>
        <source>Move the point along the curve, or move it to its home position.</source>
        <translation>沿著曲線移動點或是將其放回收納箱</translation>
    </message>
    <message>
        <location filename="ec_realFreePoint.cpp" line="89"/>
        <source>This point cannot be moved to the curve right now, because the curve is not an elliptic curve. Before moving the point, use the sliders to change the shape of the curve.</source>
        <translation>這一個點現在不能移動，因為目前曲線並非一條橢圓曲線。請使用滑桿調整曲線的形狀。</translation>
    </message>
    <message>
        <location filename="ec_realFreePoint.cpp" line="99"/>
        <source>Drag the point to the curve!</source>
        <translation>將點拉到曲線上！</translation>
    </message>
</context>
<context>
    <name>ec_realMinusPoint</name>
    <message>
        <location filename="ec_realMinusPoint.cpp" line="57"/>
        <source>The point &lt;b&gt;%1&lt;/b&gt; is the inverse of the point &lt;b&gt;%2&lt;/b&gt; according to the group law of the elliptic curve. Geometrically, the point &lt;b&gt;%1&lt;/b&gt; is constructed as the image of the point &lt;b&gt;%2&lt;/b&gt; under reflection against the x-axis.</source>
        <translation>點 &lt;b&gt;%1&lt;/b&gt; 為點 &lt;b&gt;%2&lt;/b&gt; 的反元素(橢圓曲線的群運算)。幾何上，點 &lt;b&gt;%1&lt;/b&gt; 為點 &lt;b&gt;%2&lt;/b&gt; 對x-軸的鏡射點。</translation>
    </message>
    <message>
        <location filename="ec_realMinusPoint.cpp" line="86"/>
        <source>&lt;h4&gt;Auxiliary Line&lt;/h4&gt;&lt;p&gt;The point &lt;b&gt;%1&lt;/b&gt; is constructed as the image of the point &lt;b&gt;%2&lt;/b&gt; under reflection against the x-axis. This auxiliary line connects the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%1&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;輔助線&lt;/h4&gt;&lt;p&gt;點 &lt;b&gt;%1&lt;/b&gt; 為點 &lt;b&gt;%2&lt;/b&gt; 對x-軸的鏡射點。 輔助線為通過點 &lt;b&gt;%2&lt;/b&gt; 與點 &lt;b&gt;%1&lt;/b&gt; 的直線。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realMinusPoint.cpp" line="93"/>
        <source>Inverse of the Point &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>點 &lt;b&gt;%1&lt;/b&gt; 的反元素</translation>
    </message>
    <message>
        <location filename="ec_realMinusPoint.cpp" line="95"/>
        <source>Move the point &lt;b&gt;%1&lt;/b&gt; to see how the inverse changes along with &lt;b&gt;%1&lt;/b&gt;. You can also change the shape of the curve to see how the whole construction changes.</source>
        <translation>藉由移動點 &lt;b&gt;%1&lt;/b&gt; 觀察其反元素如何隨著點 &lt;b&gt;%1&lt;/b&gt; 而改變。 亦可藉由改變曲線的形狀來觀察整個構造如何隨之改變。</translation>
    </message>
</context>
<context>
    <name>ec_realPoint</name>
    <message>
        <location filename="ec_realPoint.cpp" line="125"/>
        <source>Point</source>
        <translation>點</translation>
    </message>
    <message>
        <location filename="ec_realPoint.cpp" line="149"/>
        <source>The point &lt;b&gt;%1&lt;/b&gt; is the point at infinity and therefore not shown on the screen.</source>
        <translation>點 &lt;b&gt;%1&lt;/b&gt; 為無限遠點，故沒有標記在螢幕上。</translation>
    </message>
    <message>
        <location filename="ec_realPoint.cpp" line="152"/>
        <source>The coordinates of the point &lt;b&gt;%1&lt;/b&gt; are approximately (%2, %3).</source>
        <translation>點 &lt;b&gt;%1&lt;/b&gt; 的座標大約是 (%2, %3)。</translation>
    </message>
    <message>
        <location filename="ec_realPoint.cpp" line="154"/>
        <source>The point is &lt;b&gt;%1&lt;/b&gt; is a special point of the elliptic curve. If you use the group law of the curve to add the point &lt;b&gt;%1&lt;/b&gt; to itself, the point at infinity is obtained as a result. The inverse of &lt;b&gt;%1&lt;/b&gt; is the same as &lt;b&gt;%1&lt;/b&gt;. Mathematicians say that &lt;b&gt;%1&lt;/b&gt; is a 2-torsion point of the elliptic curve.</source>
        <translatorcomment>it&apos;s hard to translate 2-torsion point</translatorcomment>
        <translation>點 &lt;b&gt;%1&lt;/b&gt; 為橢圓曲線上相當特別的一個點。如果你考慮點 &lt;b&gt;%1&lt;/b&gt; 加點 &lt;b&gt;%1&lt;/b&gt; ，則結果會是無限遠點。換句話說，點 &lt;b&gt;%1&lt;/b&gt; 的反元素還是點 &lt;b&gt;%1&lt;/b&gt; 。數學上，我們稱呼這樣的點 &lt;b&gt;%1&lt;/b&gt; 為橢圓曲線上的二扭點。</translation>
    </message>
    <message>
        <location filename="ec_realPoint.cpp" line="159"/>
        <source> Because the coordinates are so large, the point is offscreen and therefore not shown.</source>
        <translation>因為該點的座標過大，所以不會顯示這個座落於螢幕外的點。</translation>
    </message>
</context>
<context>
    <name>ec_realSumPoint</name>
    <message>
        <location filename="ec_realSumPoint.cpp" line="66"/>
        <source>The point &lt;b&gt;%1&lt;/b&gt; is the sum of the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt; according to the group law of the elliptic curve. Geometrically, the point &lt;b&gt;%1&lt;/b&gt; is constructed by taking the line through &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;. This line will intersect the elliptic curve in the exactly one further point, which is offscreen and therefore not shown in the picture. The sum &lt;b&gt;%1&lt;/b&gt; is then constructed as the image of the third point under reflection against the x-axis.</source>
        <translation>點 &lt;b&gt;%1&lt;/b&gt; 為點 &lt;b&gt;%2&lt;/b&gt; 與點 &lt;b&gt;%3&lt;/b&gt; 的和(橢圓曲線的群運算)。幾何上，點 &lt;b&gt;%1&lt;/b&gt; 由以下方法構造出來，拿一條通過點 &lt;b&gt;%2&lt;/b&gt; 與點 &lt;b&gt;%3&lt;/b&gt; 的直線，此直線會正好交橢圓曲線於第三點(這一點在螢幕外，所以不會顯示於圖片中)。則點 &lt;b&gt;%1&lt;/b&gt; 為此第三點對 x-軸的鏡射點。</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="72"/>
        <source>The point &lt;b&gt;%1&lt;/b&gt; is the sum of the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt; according to the group law of the elliptic curve. Geometrically, the point &lt;b&gt;%1&lt;/b&gt; is constructed by taking the line through &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;. This line will intersect the elliptic curve in the exactly one further point, shown as a small yellow circle. The sum &lt;b&gt;%1&lt;/b&gt; is then constructed as the image of the third point under reflection against the x-axis.</source>
        <translation>點 &lt;b&gt;%1&lt;/b&gt; 為點 &lt;b&gt;%2&lt;/b&gt; 與點 &lt;b&gt;%3&lt;/b&gt; 的和(橢圓曲線的群運算)。幾何上，點 &lt;b&gt;%1&lt;/b&gt; 由以下方法構造出來，拿一條通過點 &lt;b&gt;%2&lt;/b&gt; 與點 &lt;b&gt;%3&lt;/b&gt; 的直線，此直線會正好交橢圓曲線於第三點，此點以黃色小點標示。則點 &lt;b&gt;%1&lt;/b&gt; 為此第三點對 x-軸的鏡射點。</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="90"/>
        <source>The figure shows that the point &lt;b&gt;%1&lt;/b&gt; can also be constructed as the sum of the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;. This illustrates the fundamental fact that the order does not play any role when adding points on an elliptic curve. Mathematicians express this fact by saying that &apos;addition on an elliptic curve is associative&apos;. Associativity is a consequence of &apos;Noether&apos;s residual intersection theorem&apos;, named after the german mathematician Max Noether (1844-1921).</source>
        <translation>圖形顯示點 &lt;b&gt;%1&lt;/b&gt; 亦可當作點 &lt;b&gt;%2&lt;/b&gt; 與點 &lt;b&gt;%3&lt;/b&gt; 的和。這顯示橢圓曲線上的加法順序並不影響結果的這一基本結果，數學家稱呼這種現象為「橢圓曲線上的加法有結合律」。我們可由德國數學家馬克思諾特(1844-1921)的「諾特餘交定理」證明其結合律性質。</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="123"/>
        <source>&lt;h4&gt;Auxiliary Line&lt;/h4&gt;&lt;p&gt;The point &lt;b&gt;%1&lt;/b&gt; is constructed as the image of the small yellow point under reflection against the x-axis. This auxiliary line connects the points &lt;b&gt;%1&lt;/b&gt; and the small yellow point.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;輔助線&lt;/h4&gt;&lt;p&gt;點 &lt;b&gt;%1&lt;/b&gt; 為小黃點對 x-軸的鏡射點。 輔助線為通過點 &lt;b&gt;%1&lt;/b&gt; 與小黃點的直線。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="128"/>
        <source>&lt;h4&gt;Auxiliary Point&lt;/h4&gt;&lt;p&gt;This is an auxiliary point used in the construction of the sum &lt;b&gt;%1&lt;/b&gt;. The sum is constructed by looking at the secant line through &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;, The secant line is guaranteed to intersect the elliptic curve in precisely one further point, the point you are looking at right now.&lt;/p&gt;&lt;p&gt;Move the mouse pointer to the point &lt;b&gt;%1&lt;/b&gt; to learn more about the construction.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;輔助線&lt;/h4&gt;&lt;p&gt;這條輔助線是用構造點 &lt;b&gt;%2&lt;/b&gt; 與點 &lt;b&gt;%3&lt;/b&gt; 的和，點&lt;b&gt;%1&lt;/b&gt;。構造點 &lt;b&gt;%1&lt;/b&gt;的方法為拿取通過點 &lt;b&gt;%2&lt;/b&gt;與點 &lt;b&gt;%3&lt;/b&gt;的切線，此切線將與橢圓曲線正好交在你所看到的第三點&lt;/p&gt;&lt;p&gt;移動鼠標到點&lt;b&gt;%1&lt;/b&gt;上以獲得更多資訊。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="138"/>
        <source>Sum of the points &lt;b&gt;%1&lt;/b&gt; and  &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>點 &lt;b&gt;%1&lt;/b&gt;與點 &lt;b&gt;%2&lt;/b&gt;的總和</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="140"/>
        <source>Move one of the points &lt;b&gt;%1&lt;/b&gt; or &lt;b&gt;%2&lt;/b&gt; to see how the sum changes. You can also change the shape of the curve to see how the whole construction changes.</source>
        <translation>藉由移動點 &lt;b&gt;%1&lt;/b&gt;或點 &lt;b&gt;%2&lt;/b&gt;觀察和的變化。你亦可藉由改變曲線的形狀來觀察整個構造的變化。</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="145"/>
        <source>&lt;h4&gt;Secant Line&lt;/h4&gt;&lt;p&gt;This line is the secant line through the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;, an auxiliary line used in the construction of the point &lt;b&gt;%1&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;切線&lt;/h4&gt;&lt;p&gt;這一條通過 &lt;b&gt;%2&lt;/b&gt; 與 &lt;b&gt;%3&lt;/b&gt;的切線，是用來構造點&lt;b&gt;%1&lt;/b&gt;的輔助線。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="149"/>
        <source>&lt;p&gt;Apart from the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;, the secant line is guaranteed to intersect the elliptic curve in precisely one third point, which is offscreen and therefore not shown in the picture.&lt;/p&gt;&lt;p&gt;The point &lt;b&gt;%1&lt;/b&gt; is constructed as the image of the third intersection under reflection against the x-axis.&lt;/p&gt;</source>
        <translation>&lt;p&gt;除卻&lt;b&gt;%2&lt;/b&gt;、&lt;b&gt;%3&lt;/b&gt;兩點以外，切線會與橢圓曲線恰巧交於第三點(這一點在畫面之外，所以沒有呈現在圖形中)&lt;/p&gt;&lt;p&gt;點&lt;b&gt;%1&lt;/b&gt;為此第三點對x軸的鏡射。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="154"/>
        <source>&lt;p&gt;Apart from the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;, the secant line is guaranteed to intersect the elliptic curve in precisely one third point&lt;/p&gt;&lt;p&gt;Move the mouse pointer to the point &lt;b&gt;%1&lt;/b&gt; to learn more about the construction.&lt;/p&gt;</source>
        <translation>&lt;p&gt;除卻&lt;b&gt;%2&lt;/b&gt;、&lt;b&gt;%3&lt;/b&gt;兩點以外，切線會與橢圓曲線恰巧交於第三點&lt;/p&gt;&lt;p&gt;移動游標至點&lt;b&gt;%1&lt;/b&gt;上以獲得更多資訊。&lt;p/&gt;</translation>
    </message>
</context>
<context>
    <name>gfxExportDialog</name>
    <message>
        <location filename="gfxExportDialog.ui" line="14"/>
        <source>Export Options</source>
        <translation>匯出選項</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="26"/>
        <source>Bitmap Graphic Options</source>
        <translation>點陣圖型選項</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="38"/>
        <source>Width</source>
        <translation>寬度</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="54"/>
        <location filename="gfxExportDialog.ui" line="92"/>
        <source> Pixel</source>
        <translation> 像素</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="76"/>
        <source>Height</source>
        <translation>高度</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="105"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If checked, the background of the image becomes transparent rather than white.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If checked, the background of the image becomes transparent rather than whilte.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;選取後圖片背景即變為透明&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="112"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If checked, the background of the image becomes transparent rather than white.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This can be useful if you wish to merge the graphic into an existing picture.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If checked, the background of the image becomes transparent rather than whilte.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This can be useful if you wish to merge the graphic into an existing picture.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;選取後圖片背景即變為透明&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;這允許你將圖形嵌入另一張圖片&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="120"/>
        <source>Transparent Background</source>
        <translation>背景透明</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="130"/>
        <source>Features Shown</source>
        <translation>背景選項</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="136"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Include the light gray coordinate grid when exporting to a graphics file.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;在匯出的圖檔中顯示淺灰色的座標方格&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="143"/>
        <source>Show Grid</source>
        <translation>顯示座標方格</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="153"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Include the dark coordinate axes when exporting to a graphics file.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;於匯出的圖檔中顯示黑色座標軸&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="156"/>
        <source>Show Axes</source>
        <translation>顯示座標軸</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="192"/>
        <source>Preview</source>
        <translation>預覽</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="204"/>
        <source>TextLabel</source>
        <translation>文字標籤</translation>
    </message>
</context>
<context>
    <name>infoDialog</name>
    <message>
        <location filename="infoDialog.ui" line="14"/>
        <source>About Elliptic Curves</source>
        <translation>關於橢圓曲線</translation>
    </message>
    <message>
        <location filename="infoDialog.ui" line="24"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;;&quot;&gt;Sketching elliptic curves over the real numbers.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;;&quot;&gt;描繪實數平面上的橢圓曲線&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="infoDialog.ui" line="80"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:14px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Questions and Experiments&lt;/span&gt;&lt;/p&gt;
&lt;ol style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Where is the neutral group element? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Try to use the Elliptic Curve Plotter to illustrate the associativity law. &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;What is the group-theoretic meaning of the inflection points? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choose a=2 and b=-10. You will obtain an elliptic curve without oval, which is rather flat. The inflection points are therefore hard to locate visually. Using the group law, can you devise a way to use the Elliptic Curve Plotter that helps you find the inflection points precisely? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The elliptic curves are real Lie groups. Which one?&lt;/li&gt;&lt;/ol&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:14px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;問題與實驗實作&lt;/span&gt;&lt;/p&gt;
&lt;ol style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;找出橢圓曲線加法群的單位元所對應的點? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;嘗試使用Elliptic Curve Plotter顯示連鎖律&lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;反曲點在群理論裡的意義&lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;選擇a=2 and b=-10。你將得到一個相當平坦的無獨立橢圓的橢圓曲線。故反曲點將相當難以用肉眼判別。你能否找到一個使用$軟體操作群運算的方法找到曲線上的反曲點?&lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;橢圓曲線是實李群，你能發現是哪一個嗎？&lt;/li&gt;&lt;/ol&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="infoDialog.ui" line="100"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Report Problems or Suggestions&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you find a bug in the Elliptic Curve Plotter, or if you have a suggestion for improvement, please contact the author at &lt;span style=&quot; font-weight:600;&quot;&gt;stefan.kebekus@math.uni-freiburg.de&lt;/span&gt;. We appreciate your input!&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you are reporting a bug, please include details about your setup:&lt;/p&gt;
&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;What version of the Elliptic Curve Plotter are you using? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;What platform are you using? Linux? MacOS? Windows? What distribution and what version? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Where did you get the program from?&lt;/li&gt;&lt;/ul&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Also, please remember that it is almost impossible to fix a bug that we cannot reproduce, so please include all information necessary to reproduce the bug. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;銘謝&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;建議或回報問題&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;如果發現任何程式錯誤或建議，請使用&lt;span style=&quot; font-weight:600;&quot;&gt;stefan.kebekus@math.uni-freiburg.de&lt;/span&gt;聯絡作者。 感謝您的回饋!&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;如您要回報問題，請包含以下系統資訊。&lt;/p&gt;
&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;請問你使用哪一個版本的Elliptic Curve Plotter &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;您使用哪種作業軟體？Linux? MacOS? Windows?哪一個版本或發行版本？ &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;您從何處下載此軟體？&lt;/li&gt;&lt;/ul&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;同時，請記得我們僅能修復我們能夠重現的軟體錯誤，所以請提供足夠多的資訊以讓我們重現該軟體錯誤。 
 &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="infoDialog.ui" line="121"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:14px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Help to improve the Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Come up with new ideas&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you have suggestion how the Elliptic Curve Plotter could be improved, let us know!&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Help to implement new features&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you have programming skills, you can help to implement new features or streamline the existing program. The Elliptic Curve Plotter is written in the standard C++ programming language and uses the excellent and very well-documented Qt library for its user interface.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Translate the Elliptic Curve Plotter into new languages&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you know a language that the Elliptic Curve Plotter does not yet support, join us to add language support. Programming skills are not required.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;銘謝&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:14px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;協助改進Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;新點子&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;如您有任何改進Elliptic Curve Plotter的想法，請讓我們知道！&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;協助改進功能&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;如您會寫程式，您可以協助改進功能或將現有程式優化。Elliptic Curve Plotter是以標準C++程式語言寫成，並佐以Qt. Library撰寫使用者界面。&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;翻譯Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;如您通曉Elliptic Curve Plotter目前所沒有支援的語言，請協助我們增加Elliptic Curve Plotter的語言版本。不須任何撰寫程式能力。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>mainWindow</name>
    <message>
        <location filename="mainWindow.ui" line="22"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This field shows a sketch of the curve defined over the real numbers by the equation &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;這裡顯示該方程式於實數平面上的圖形&lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="36"/>
        <source>Elliptic Curve Graph</source>
        <translation>橢圓曲線圖形</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="45"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Curve Equation: &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;%1%2&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;曲線方程式 &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;%1%2&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="85"/>
        <source>Equation Parameters</source>
        <translation>方程式參數</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="198"/>
        <source>Curve Description</source>
        <translation>曲線敘述</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="227"/>
        <source>&amp;File</source>
        <translation>&amp;檔案</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="231"/>
        <source>Export Drawing</source>
        <translation>匯出圖片</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="245"/>
        <source>&amp;Curves</source>
        <translation>&amp;曲線</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="256"/>
        <source>Help</source>
        <translation>說明</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="269"/>
        <source>Group Law</source>
        <translation>群運算</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="282"/>
        <source>View</source>
        <translation>界面</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="286"/>
        <source>Language</source>
        <translation>語言</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="305"/>
        <source>toolBar</source>
        <translation>工具列</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="337"/>
        <source>&amp;Quit</source>
        <translation>&amp;離開</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="340"/>
        <source>Exits the application.</source>
        <translation>離開程式</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="343"/>
        <source>Exits the Elliptic Curve Plotter</source>
        <translation>離開Elliptic Curve Plotter</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="351"/>
        <source>Elliptic curve with oval</source>
        <translation>帶有獨立橢圓的橢圓曲線</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="354"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choses values for &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; and &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; such that the curve defined by the equation &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; is an elliptic curve that consists of two disjoint components.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;選取&lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; 與 &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt;使得&lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt;所對應的橢圓曲線有兩個不相交的分枝&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="363"/>
        <source>Elliptic curve without oval</source>
        <translation>無獨立橢圓的橢圓曲線</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="366"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choses values for &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; and &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; such that the curve defined by the equation &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; is an elliptic curve that consists of one component only.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;選取&lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; 與 &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt;使得&lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt;所對應的橢圓曲線僅有一個分枝&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="375"/>
        <source>Neil Parabola</source>
        <translation>尼爾拋物線</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="378"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choses values for &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; and &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; such that the curve defined by the equation &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; is a semicubical parabola.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>選取&lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; 與 &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt;使得&lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt;所對應的橢圓曲線是一條半立方拋物線&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translatorcomment>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;選取&lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; 與 &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt;使得&lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt;所對應的橢圓曲線是一條半立方拋物線&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="387"/>
        <source>Nodal Plane Cubic</source>
        <translation>節點平面三次曲線</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="390"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choses values for &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; and &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; such that the curve defined by the equation &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; is a nodel plane cubic.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>選取&lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; 與 &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt;使得&lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt;所對應的橢圓曲線是一條節點平面三次曲線&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translatorcomment>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;選取&lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; 與 &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt;使得&lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt;所對應的橢圓曲線是一條節點平面三次曲線&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="399"/>
        <source>Curve with Isolated Point</source>
        <translation>有孤立個點的曲線</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="402"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choses values for &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; and &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; such that the curve defined by the equation &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; is a singular elliptic curve that consists of a parabola and an isolated point.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;選取&lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; 與 &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt;使得&lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt;所對應的橢圓曲線是一條有孤立個點的橢圓曲線&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="414"/>
        <source>About the Elliptic Curve Plotter ...</source>
        <translation>關於Elliptic Curve Plotter</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="417"/>
        <source>Shows information about the Elliptic Curve Plotter and its license.</source>
        <translation>顯示Elliptic Curve Plotter的資訊與其授權條款。</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="429"/>
        <source>What&apos;s This?</source>
        <translation>這個是什麼？</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="432"/>
        <source>What&apos;s This? Click on this item and then on any field to obtain a detailed explanation.</source>
        <translation>這個是什麼？請點選此圖像並放置於任何地方以獲得該物件的說明。</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="435"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/whatsthis.png&quot; /&gt;Click on this item and then on any other field or menu item to obtain a more detailed description of the field.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/whatsthis.png&quot; /&gt;點選此選項蹦將放置在任何想了解其詳細說明位置或功能選項上以獲得更多資訊.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="453"/>
        <source>Compute 2P</source>
        <translation>計算2P</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="459"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this action to compute the sum of the point &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; with itself according to the group law of the elliptic curve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;使用這個去計算兩倍的&lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; 點。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="473"/>
        <source>Compute P+Q</source>
        <translation>計算P+Q</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="479"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this action to compute the sum of the points &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; and &lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt; according to the group law of the elliptic curve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;使用這個計算 &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; 點與 &lt;span style=&quot; font-weight:600;&quot;&gt;Q點&lt;/span&gt; 的和。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="490"/>
        <source>Compute -P</source>
        <translation>計算負P</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="496"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this action to compute the inverse of the point &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; according to the group law of the elliptic curve&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;使用這個計算&lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; 點在橢圓曲線群結構裡的反元素&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="510"/>
        <source>Compute Q+R</source>
        <translation>計算 Q+R</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="516"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this action to compute the sum of the points &lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt; and &lt;span style=&quot; font-weight:600;&quot;&gt;R &lt;/span&gt;according to the group law of the elliptic curve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;使用這個去計算&lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt; 與 &lt;span style=&quot; font-weight:600;&quot;&gt;R &lt;/span&gt;兩點的和。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="527"/>
        <source>Compute P+(Q+R)</source>
        <translation>計算P+(Q+R)</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="533"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this action to compute the sum of the points &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; and &lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt;+&lt;span style=&quot; font-weight:600;&quot;&gt;R&lt;/span&gt; according to the group law of the elliptic curve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;使用這個去計算&lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; 、 &lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt;+&lt;span style=&quot; font-weight:600;&quot;&gt;R&lt;/span&gt;三點的總和。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="544"/>
        <location filename="mainWindow.ui" line="550"/>
        <source>Compute (P+Q)+R</source>
        <translation>計算 (P+Q)+R</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="553"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this action to compute the sum of the points &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt;+&lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt; and &lt;span style=&quot; font-weight:600;&quot;&gt;R&lt;/span&gt; according to the group law of the elliptic curve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;使用這個去計算&lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt;+&lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt; 這一個點與 &lt;span style=&quot; font-weight:600;&quot;&gt;R&lt;/span&gt; 點的和。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="562"/>
        <source>About Qt ...</source>
        <translation>關於 Qt ...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="565"/>
        <source>Shows information about the Qt library, a software product that is used internally by the Elliptic Curve Plotter.</source>
        <translation>顯示Qt library的資訊。</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="576"/>
        <source>Export to Vector Graphics File...</source>
        <translation>匯出向量圖檔...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="579"/>
        <location filename="mainWindow.ui" line="586"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Saves the elliptic curve graph as a Scalable Vector Graphics (SVG) file. SVG files can be edited with specialized programs (such as &lt;span style=&quot; font-weight:600;&quot;&gt;karbon&lt;/span&gt; or &lt;span style=&quot; font-weight:600;&quot;&gt;inkscape&lt;/span&gt;), and are suitable for high quality printouts.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>將橢圓曲線的圖片存成可縮放向量圖形(SVG)檔。 SVG檔可使用&lt;span style=&quot; font-weight:600;&quot;&gt;karbon&lt;/span&gt; 或 &lt;span style=&quot; font-weight:600;&quot;&gt;inkscape&lt;/span&gt;等軟體編輯， 且能列印高品質的圖片&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translatorcomment>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;將橢圓曲線的圖片存成可縮放向量圖形(SVG)檔。 SVG檔可使用&lt;span style=&quot; font-weight:600;&quot;&gt;karbon&lt;/span&gt; 或 &lt;span style=&quot; font-weight:600;&quot;&gt;inkscape&lt;/span&gt;等軟體編輯， 且能列印高品質的圖片&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="595"/>
        <source>&amp;Print...</source>
        <translation>&amp;列印</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="600"/>
        <source>&amp;Open...</source>
        <translation>&amp;開啟舊檔</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="605"/>
        <source>Save &amp;As...</source>
        <translation>&amp;另存新檔</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="610"/>
        <source>Report Problem or Suggestion...</source>
        <translation>回報問題或建議...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="613"/>
        <location filename="mainWindow.ui" line="616"/>
        <source>If you find a bug in the Elliptic Curve Plotter, or if you have a suggestion for future improvements, use this menu entry to contact the author.</source>
        <translation>如果你發現Elliptic Curve Plotter的錯誤或有任何對於程式的建議或改進，請使用這個選項聯絡作者</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="621"/>
        <location filename="mainWindow.ui" line="648"/>
        <source>How to help...</source>
        <translation>如何提供協助...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="629"/>
        <source>Export to Raster Graphics File...</source>
        <translation>匯出點陣圖...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="632"/>
        <location filename="mainWindow.ui" line="635"/>
        <source>Saves the elliptic curve graph as a raster graphics file in Portable Network Graphics (PNG) format. PNG files can easily be included in web pages, but have limited printout quality.</source>
        <translation>將橢圓曲線圖形存成行動式網路圖形(PNG)格式的點陣圖。PNG檔案易於夾附於網頁之中，但列印品質不佳。</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="640"/>
        <source>Questions and Experiments...</source>
        <translation>問題與實作實驗...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="643"/>
        <source>Questions and Suggestions for Experiments</source>
        <translation>問題與實作實驗的建議</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="651"/>
        <source>Use this action to learn how to help improving the Elliptic Curve Plotter</source>
        <translation>使用這個去了解如何協助改進$程式</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="659"/>
        <source>Kiosk Mode...</source>
        <translation>Kiosk 模式...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="667"/>
        <source>Reset User Interface</source>
        <translation>重設使用者界面</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="675"/>
        <source>Use System Default Language</source>
        <translation>使用系統預設語言</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="678"/>
        <location filename="mainWindow.ui" line="681"/>
        <source>Use the system default language if translations exist. Otherwise, English is used.</source>
        <translation>使用系統預設語言(如該翻譯存在)。如無，則為英文模式。</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="254"/>
        <source>English</source>
        <comment>Translate this to the name of YOUR language, in your language (e.g. &apos;Deutsch&apos;, &apos;Italiano&apos;, &apos;Español&apos;)</comment>
        <translation>繁體中文</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="390"/>
        <source>Curve Equation: %1</source>
        <translation>曲線方程式： %1</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="446"/>
        <source>&lt;p&gt;The file name &lt;b&gt;%1&lt;/b&gt; does not end in &lt;b&gt;%2&lt;/b&gt;. This might make it difficult for the file manager and other programs to recognize the file type.&lt;/p&gt;</source>
        <translation>&lt;p&gt;檔案名稱&lt;b&gt;%1&lt;/b&gt; 並沒有以 &lt;b&gt;%2&lt;/b&gt;作為結尾。 這將會對檔案管理者或其他軟體造成辨別檔案類型的困難&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="449"/>
        <source>&lt;p&gt;Would you like to add the ending to the file name &lt;b&gt;and overwrite the existing file %1&lt;/b&gt;?&lt;/p&gt;</source>
        <translation>&lt;p&gt;請問你想在檔案名稱後增加字綴並&lt;b&gt;複寫已存在的檔案%1&lt;/b&gt;嗎?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="451"/>
        <source>&lt;p&gt;Would you like to add the ending to the file name?&lt;/p&gt;</source>
        <translation>&lt;p&gt;請問你想在檔案名稱後增加字綴嗎?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="479"/>
        <source>Scalable Vector Graphics File (*.svg)</source>
        <translation>可縮放向量圖形(*.svg)</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="491"/>
        <source>Elliptic Curve</source>
        <translation>橢圓曲線</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="503"/>
        <location filename="mainWindow.cpp" line="535"/>
        <source>File Error</source>
        <translation>檔案錯誤</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="504"/>
        <location filename="mainWindow.cpp" line="536"/>
        <source>&lt;p&gt;The file %1 could not be written.&lt;/p&gt;&lt;p&gt;The system reported the following error: %2&lt;/p&gt;</source>
        <translation>&lt;p&gt;無法複寫檔案%1&lt;/p&gt;&lt;p&gt;系統回報錯誤 %2&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="515"/>
        <source>Portable Network Graphics (*.png)</source>
        <translation>行動式網路圖形(*.png)</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="546"/>
        <source>Report Problem or Suggestion</source>
        <translation>回報問題或建議</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="558"/>
        <source>How to Help</source>
        <translation>如何提供協助</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="570"/>
        <source>Questions and Experiments</source>
        <translation>問題與實作實驗</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="620"/>
        <location filename="mainWindow.cpp" line="635"/>
        <source>Resetting User Interface</source>
        <translation>重設使用者界面</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="636"/>
        <source>Several Minutes without user interaction.</source>
        <translation>閒置時間過長</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="719"/>
        <source>Elliptic Curve Plotter - Kiosk Mode</source>
        <translation>Elliptic Curve Plotter - Kiosk 模式</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="719"/>
        <source>Do you really wish to enter the Kiosk Mode?</source>
        <translation>請問你真的想進入Kiosk模式？</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="722"/>
        <source>&lt;p&gt;The Kiosk Mode is a restricted mode useful when running the Elliptic Curve Plotter on interactive kiosks that are open to the public.&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Several menu items are disabled, so users cannot exit the program or access the file system.&lt;/li&gt;&lt;li&gt;The program switches to full-screen mode.&lt;/li&gt;&lt;li&gt;The user interface will be reset after three minutes of inactivity.&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;&lt;b&gt;There is no way to leave the Kiosk Mode other than exiting the program.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Hint:&lt;/b&gt; To start the Elliptic Curve Plotter in Kiosk Mode without showing this warning, start the Elliptic Curve Plotter from the command line using the command line option &lt;b&gt;-kiosk&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Kiosk 模式是用何在公共場合展示Elliptic Curve Plotter的安全模式&lt;/p&gt;&lt;ul&gt;&lt;li&gt;它將會鎖住數項功能選項，如在公眾場合使用Kiosk模式可避免使用者存取其他檔案或離開軟體。&lt;/li&gt;&lt;li&gt;使用者界面在閒置三分鐘之後自動重設&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;&lt;b&gt;僅能藉由關閉軟體來結束Kiosk模式。&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;提示：&lt;/b&gt;如想使用Kiosk模式開啟Elliptic Curve Plotter時不顯示這個警告標語。請使用命令行&lt;b&gt;-kiosk&lt;/b&gt;開啟Elliptic Curve Plotter。&lt;/p&gt;</translation>
    </message>
</context>
</TS>
