/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef ECDESCRIPTION
#define ECDESCRIPTION

#include <ec_realPoint.h>

#include <QPointer>
#include <QTextBrowser>
#include <QTimer>


class ec_real;

/**
 * Widget that describes an elliptic curve and its points.
 *
 * This widget displays a rich text that describes a real elliptic curve and a
 * set of points in some detail. To avoid flickering whenever the curve or the
 * point change rapidly, the display is updated with a slight delay.
 *
 * @author Stefan Kebekus
 */

class ecDescription : public QTextBrowser
{
  Q_OBJECT

  public:
  /**
   * Default constructur
   *
   * @param parent A pointer to the parent widget, or Null for a top-level
   * widget.
   */
  ecDescription(QWidget *parent = nullptr);

  /**
   * Setting the curve which will be described
   *
   * This method should be called after the ecDescription widget has been
   * constructed. It tells the widget which curve to describe, and monitors
   * the curve for changes, in order to update the display whenever the curve
   * is modified.
   *
   * After a curve has been set, it is possible to delete the curve while this
   * widget still exists. However, the widget will become useless, and the
   * graphics may leave to be desired.
   * 
   * @param parentCurve A pointer to a curve which is monitored for
   * changes. The ecDescription widget will not take ownership of the curve.
   */
  void setCurve( ec_real * parentCurve );

  /**
   * Setting points which will be described
   *
   * This method should be called (once for each point whose description is to
   * be included) after the ecDescription widget has been constructed, and
   * after a curve has been set with the method setCurve(). A description of
   * the point will be added, and the point is monitored for changes, in order
   * to update the display whenever something is modified.
   *
   * Points are described in the order in which they were added. There is no
   * way to changed the order.
   *
   * @param pt A pointer to a point which is monitored for changes. The
   * ecDescription widget will not take ownership of the point.
   */
   void addPoint( ec_realPoint * pt );

 private slots:
   /**
    * Update display
    *
    * This slot is connected to the timer, and will hardly ever be called
    * otherwise
    */
   void  updateDescription();

 private:
   /** 
    * Pointer to the curve which is displayed
    */
     QPointer<ec_real> parentCurve{nullptr};

     /**
    * List of points that are displayed
    */
     QList<QPointer<ec_realPoint> > points;

     /**
    * Timer used to update the display with a slight delay, so as to avoid
    * flickering.
    */
     QTimer timer;
};

#endif
