/***************************************************************************
 *   Copyright (C) 2012 by Stefan Kebekus                                  *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QResizeEvent>
#include <QScreen>
#include <QWindow>

#include "ecPlotter.h"
#include "gfxExportDialog.h"


gfxExportDialog::gfxExportDialog(ecPlotter *_plotter, bool bitmap, QWidget *parent)
  : QDialog(parent)
{
  Q_ASSERT(_plotter != nullptr);

  plotter = _plotter;
  ui.setupUi(this);

  if (!bitmap) {
      ui.bitmapOptions->hide();
  }

  QRectF const box = plotter->augmentedBox();
  Q_ASSERT(box.width() != 0.0);

  ratio = box.height()/box.width();
  Q_ASSERT(ratio != 0.0);
  ui.heightBox->setMinimum(qRound(ui.widthBox->minimum()*ratio));
  ui.heightBox->setMaximum(qRound(ui.widthBox->maximum()*ratio));
  widthChanged();

  QRect const screenGeometry = qApp->primaryScreen()->geometry();
  double const screenFill = 0.5;
  double scale = 20;
  if (!box.isEmpty() && !screenGeometry.isEmpty()) {
      scale = qMin(screenGeometry.width() * screenFill / box.width(),
                   screenGeometry.height() * screenFill / box.height());
  }
  ui.preview->setMinimumSize( qRound(box.width()*scale), qRound(box.height()*scale) );
  updatePreview();

  connect(ui.axesBox, &QAbstractButton::toggled, this, &gfxExportDialog::updatePreview);
  connect(ui.gridBox, &QAbstractButton::toggled, this, &gfxExportDialog::updatePreview);
  connect(ui.widthBox, &QSpinBox::valueChanged, this, &gfxExportDialog::widthChanged);
  connect(ui.heightBox, &QSpinBox::valueChanged, this, &gfxExportDialog::heightChanged);
}


gfxExportDialog::~gfxExportDialog()
= default;


void gfxExportDialog::widthChanged()
{
  Q_ASSERT(ratio != 0.0);
  if (ratio == 0.0) {
      return;
  }

  int const newWidth = qRound(ui.heightBox->value() / ratio);
  int const newHeight = qRound(ui.widthBox->value() * ratio);

  if (ui.widthBox->value() == newWidth) {
      return;
  }
  if (ui.heightBox->value() == newHeight) {
      return;
  }

  ui.heightBox->setValue(newHeight);
}


void gfxExportDialog::heightChanged()
{
  Q_ASSERT(ratio != 0.0);
  if (ratio == 0.0) {
      return;
  }

  int const newWidth = qRound(ui.heightBox->value() / ratio);
  int const newHeight = qRound(ui.widthBox->value() * ratio);

  if (ui.widthBox->value() == newWidth) {
      return;
  }
  if (ui.heightBox->value() == newHeight) {
      return;
  }

  ui.widthBox->setValue(newWidth);
}


void gfxExportDialog::resizeEvent(QResizeEvent *event)
{
  updatePreview();
  event->accept();
}


void gfxExportDialog::updatePreview()
{
  Q_ASSERT(plotter != nullptr);
  if (plotter == nullptr) {
      return;
  }

  QRectF const box = plotter->augmentedBox();
  QSize const widgetBox = ui.preview->size();
  double const scale = qMin(widgetBox.width() / box.width(), widgetBox.height() / box.height());

  QPixmap img( ui.preview->width() - 2*ui.preview->frameWidth(), ui.preview->height() - 2*ui.preview->frameWidth() );
  img.fill();
  QPainter painter;
  painter.begin(&img);
  painter.setRenderHint(QPainter::Antialiasing);
  painter.translate( (ui.preview->size().width()-box.width()*scale)/2, (ui.preview->size().height()-box.height()*scale)/2);
  plotter->paint(painter, ui.axesBox->isChecked(), ui.gridBox->isChecked());
  painter.end();

  ui.preview->setPixmap(img);
}
