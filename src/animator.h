/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef ANIMATOR
#define ANIMATOR

#include <QTimeLine>

class ec;


/**
 * Animates a real elliptic curve, to reflect changes in an elliptic curve
 * defined over the integers.
 *
 * The class is designed to create a smooth animation of a deformation of a
 * real elliptic curve, in order to reflect changes in an elliptic curve
 * defined over the integers. To use this class, set up your code as follows
 *
 * @code
 * ec       *intEC;
 * ec_real  *realEC;
 *
 * // ... make intEC and realEC point to actual curves ...
 *
 * // Construct the animator
 * animator *anim = new animator(intEC, this);
 *
 * // Connect the animator with the real elliptic curve
 * connect(anim, SIGNAL(changed(qreal, qreal)), realCurve, SLOT(setAB(qreal, qreal)));
 * @endcode
 *
 * Now, whenver the integral curve changes, the anim object will smoothly
 * deform the realEC by generating a series of "changed" signals over a time
 * of 200ms. Changing the intEC repeatedly while the animation is running is
 * not a problem. The intEC can be deconstructed at any time without problems.
 *
 * When the intEC is touched, i.e., when intEC emits the signal "ec::changed"
 * but no change is really taking place, the touch is simply forwarded, i.e.,
 * the signal "animator::changed" is emitted only once.
 *
 * @note The animator class is designed to work with only one integral curve
 * at a time.
 *
 * @warning The methods of this class are not thread-safe
 * 
 * @author Stefan Kebekus
 */

class animator : public QObject
{
  Q_OBJECT
    
 public:
  /**
   * Default constructor
   *
   * @param curve Pointer to the integral curve that is monitored for
   * changes. This parameter must not be zero.
   *
   * @param parent Pointer to the parent QObject 
   */
  animator(ec *curve, QObject *parent = nullptr);
  
 signals:
  /**
   * Emitted in order to change a real elliptic curve
   *
   * When the integral elliptic curve changes, this signal is emitted
   * frequently over a time frame of about 200ms, in order to create a smooth
   * animation of the deformation of the elliptic cuve.
   *
   * @param a Coefficient of the Weierstraß equation of the real elliptic
   * curve
   *
   * @param b Coefficient of the Weierstraß equation of the real elliptic
   * curve
   */
  void changed(qreal a, qreal b);
  
 private slots:
  void intCurveChanged(ec *curve);
  void animate(qreal);

 private:
  QTimeLine timeLine;
  qreal  animationStartA, animationStartB;
  qreal  animationEndA, animationEndB;
  qreal  intermediateA, intermediateB;
};

#endif
