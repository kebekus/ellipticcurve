/***************************************************************************
 *   Copyright (C) 2018-2024 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QDebug>
#include <QPen>
#include <cmath>

#include "ec_real.h"

using namespace Qt;


ec_real::ec_real(const QRectF &bbox, QObject *parentQObject, QGraphicsItem *parentQGraphicsItem)
    : QObject(parentQObject)
    , QGraphicsPathItem(parentQGraphicsItem)
    , boundingBox(bbox)
{
    if ((boundingBox.top() >= 0) || (boundingBox.top() != -boundingBox.bottom())) {
        qWarning() << "ec_real::ec_real() bounding box conditions are not satisfied";
    }

    _a = 0.0;
    _b = 0.0;

    QPen pen(Qt::black);
    pen.setWidthF(0.08);
    setPen(pen);
    setZValue(1.0);
}


void ec_real::setAB(qreal a, qreal b)
{
    // check range of values. qBound removed because it's not understood by android
    // a = qBound(-10.0, a, 10.0);
    if (a < -10.0) {
        a = -10.0;
    }
    if (a > 10.0) {
        a = 10.0;
    }
    //b = qBound(-10.0, b, 10.0);
    if (b < -10.0) {
        b = -10.0;
    }
    if (b > 10.0) {
        b = 10.0;
    }

    // set the values and emit changed()
    _a = a;
    _b = b;

    QPainterPath const path1 = path();
    setPath(path1);
    if (!qFuzzyIsNull(Delta())) {
        setToolTip(tr("<h4>Elliptic Curve</h4>"
                      "<p>The curve outlined in black is the elliptic curve defined "
                      "by the equation</p><p>%1.</p> ").arg(equation()));
    } else {
        setToolTip(tr("<h4>Curve</h4>"
                      "<p>The curve outlined in black is the (non-elliptic) curve defined "
                      "by the equation</p><p>%1.</p> ").arg(equation()));
    }
    emit changed();
}


auto ec_real::getType() const -> ec::realType
{
    if ((_a == 0) && (_b == 0)) {
        return ec::neilParabola;
    }
    if ((_a == -3) && (_b == 2)) {
        return ec::nodalCurve;
    }
    if ((_a == -3) && (_b == -2)) {
        return ec::isolatedPoint;
    }
    if (Delta() > 0) {
        return ec::smoothEllipticWithOval;
    }
    return ec::smoothEllipticWithoutOval;
}


auto ec_real::description() const -> QString
{
    switch(getType()) {
    case ec::neilParabola:
        return tr("<h2>Neil Parabola</h2>"
                  "<p>This curve is not an elliptic curve. It is a singular curve,"
                  "  known as the <span style=\"font-style: italic;\">Neil parabola</span>"
                  "  or <span style=\"font-style: italic;\">semicubical parabola</span>."
                  "  The singularity at the origin is usually called a <span"
                  "    style=\"font-style: italic;\">cusp</span>. The Neil parabola is a"
                  "  rational curve. This means that it can be parameterized using only"
                  "  rational functions and without using transzendental functions such"
                  "  as sine or cosine. One such parameterization is given as t &#8594;"
                  "  (t&sup2;, t&sup3;).&nbsp;</p>"
                  "<p>This curve does not have a group structure. There is, however, a"
                  "  group structure on the smooth locus of the curve. This group is"
                  "  isomorphic to the group of real numbers with addition. The Neil"
                  "  parabola is named after William Neil (1637&#8212;1670) who first"
                  "  computed its arc length. Historically, the Neil parabola was the"
                  "  first algebraic curve whose arc length could be computed.</p>");
    case ec::nodalCurve:
        return tr("<h2>Nodal Plane Cubic</h2>"
                  "<p>This curve is not an elliptic curve. It is a singular curve,"
                  "  known as the <span style=\"font-style: italic;\">Nodal Plane Cubic</span>."
                  "  The singularity is usually called a <span style=\"font-style:"
                  "    italic;\">node</span>.</p>"
                  "<p>The nodal plane cubic is a rational curve. This means that it can"
                  "  be parameterized using only rational functions and without using"
                  "  transzendental functions such as sine or cosine. One such"
                  "  parameterization is given as t &#8594; ( t&sup2;-2, t&sup3;-3t).</p>"
                  "<p> This curve does not have a group structure. There is, however, a"
                  "  group structure on the smooth locus of the curve. This group is"
                  "  isomorphic to the group of non-zero real numbers with"
                  "  multiplication.</p>");
    case ec::isolatedPoint:
        return tr("<h2>Curve with Isolated Point</h2>"
                  "<p>This is a singular curve, and not a smooth elliptic curve. Over the"
                  "  real numbers, the equation defines the curve that you see on the left,"
                  "  and an additional point with coordinates (-1, 0). If you move the"
                  "  slider for the value <span style=\"font-style: italic;\">b</span> a"
                  "  little you can see that the point is a degenerate form of the oval that"
                  "  you see in many smooth elliptic curves.</p>"
                  "<p>The curve with the isolated point does not form a group.</p>");
    case ec::smoothEllipticWithOval:
        return tr("<h2>Elliptic Curve with Oval</h2>"
                  "<p>This curve is a smooth elliptic curve. The points of this curve"
                  "  form a group.</p>"
                  "<p>This elliptic curve consists of two parts, classically called"
                  "  \"oval\" and \"parabola\". Interestingly, it is not possible to define"
                  "  the oval or the parabola alone with algebraic equations: any"
                  "  polynomial function that vanishes along one component must"
                  "  necessarily also vanish along the other.</p>");
    case ec::smoothEllipticWithoutOval:
        return tr("<h2>Elliptic Curve without Oval</h2>"
                  "<p>This curve is a smooth elliptic curve. The points of this curve"
                  "  form a group.</p>");
    default:
        return {};
    }

    return {};
}

auto ec_real::gradient(QPointF pos) const -> QPointF
{
    QPointF grad;

    grad.setX(3.0*pos.x()*pos.x()+_a);
    grad.setY(-2.0*pos.y());
    return grad;
}


auto ec_real::tangent(QPointF pos) const -> QPointF
{
    QPointF t;

    t.setX(2.0*pos.y());
    t.setY(3.0*pos.x()*pos.x()+_a);
    return t;
}


auto ec_real::path() const -> QPainterPath
{
    QPainterPath path1;
    QPainterPath path2;

    bool negativeRadical = true;
    qreal x=boundingBox.left()-0.02;
    while( x <= boundingBox.right() ) {
        x += 0.02;

        qreal const h = x * x * x + a() * x + b();

        if (h < 0.0) {
            // Check if the sign of the discriminant changed from negative
            // to positive. In that case, we need to end the drawing paths
            // at the x-Axis
            if (!negativeRadical) {
                path1.lineTo(x, 0);
                path2.lineTo(x, 0);
            }
            negativeRadical = true;
            continue;
        }

        // Check if the sign of the discriminant changed from negative to
        // positive. In that case, we need to start a new drawing path at
        // the x-Axis
        if (negativeRadical) {
            path1.moveTo(x, 0);
            path2.moveTo(x, 0);
        }
        negativeRadical = false;

        qreal const y = sqrt(h);
        if (y >= boundingBox.bottom()) {
            continue;
        }

        path1.lineTo(x,  y);
        path2.lineTo(x, -y);
    }
    path1.addPath(path2);

    return path1;
}


auto ec_real::pointOnCurve(QPointF startPos, bool *error) const -> QPointF
{
    QPointF solution = startPos;
    qreal   val      = value(solution);
    int     counter  = 0;
    while(!isOnCurve(solution) && (++counter < 200)) {
        QPointF grad;
        qreal   len = NAN;

        grad     = gradient(solution);
        len      = length(grad);
        if (qAbs(len) < 0.01) {
            solution.setY(solution.y()+0.1);
            continue;
        }
        solution = solution - grad*(val/(len*len));
        val      = value(solution);
    }

    if (error != nullptr) {
        *error = !isOnCurve(solution);
    }

    return solution;
}


auto ec_real::isOnCurve(QPointF pt) const -> bool
{
    if (length(pt) > 1000) {
        return true;
    }

    return qAbs(value(pt)) <= 0.0001;
}


auto ec_real::equation() const -> QString
{
    QString lin;
    QString con;

    if (fabs(_a) < 0.01) {
        lin = u""_s;
    } else if (fabs(_a+1.0) < 0.01) {
        lin = u" - x"_s;
    } else if (fabs(_a-1.0) < 0.01) {
        lin = u" + x"_s;
    } else {
        lin.setNum( qRound(qAbs(_a)) );
        if (_a >= 0) {
            lin.prepend(" + ");
        } else {
            lin.prepend(" - ");
        }
        lin.append("x");
    }

    if (fabs(_b) < 0.01) {
        con = u""_s;
    } else {
        con.setNum( qRound(qAbs(_b)) );
        if (_b >= 0) {
            con.prepend(" + ");
        } else {
            con.prepend(" - ");
        }
    }

    return QString("<span style=\" font-style:italic;\">y</span>"
                   "<span style=\" font-style:italic; vertical-align:super;\">2</span>"
                   "<span style=\" font-style:italic;\"> = x</span>"
                   "<span style=\" font-style:italic; vertical-align:super;\">3</span>"
                   "<span style=\" font-style:italic;\">%1%2</span>").arg(lin, con);
}
