<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="68"/>
        <source>Sketching elliptic curves over the real numbers</source>
        <translation>Ein Zeichenprogramm für reelle elliptische Kurven</translation>
    </message>
    <message>
        <location filename="main.cpp" line="73"/>
        <source>This program comes with ABSOLUTELY NO WARRANTY; for details
use the menu item &apos;About the Elliptic Curve Plotter&apos; from
the &apos;Help&apos; menu. This is free software, and you are welcome
to redistribute it under certain conditions; use the menu
item &apos;About the Elliptic Curve Plotter&apos; from the &apos;Help&apos;
menu for details.

Usage: %1 [Options]

-help  Show this help text.

-kiosk Run the in Kiosk Mode, a restricted full-screen mode
       useful for running the program on terminals or kiosks
       that are open to the public.

</source>
        <translation>Dies ist freie Software, die Sie unter bestimmten Bedingungen
weitergeben dürfen. Benutzen Sie den Menüpunkt &apos;Über den
Elliptic Curve Plotter&apos; aus dem &apos;Hilfe&apos; Menü um die
Lizenzbedingungen einzusehen.

Aufruf: %1 [Optionen]

-help  Zeigt diesen Text.

-kiosk Startet das Programm im Ausstellungs-Modus. Dies ist ein 
       eingeschränkter Modus, der sinnvoll ist, wenn das
       Programm auf öffentlich zugänglichen Terminals
       läuft.                                   
</translation>
    </message>
</context>
<context>
    <name>aboutDialog</name>
    <message>
        <location filename="aboutDialog.ui" line="24"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;;&quot;&gt;Sketching elliptic curves over the real numbers.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;;&quot;&gt;Sketching elliptic curves over the real numbers.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;;&quot;&gt;Ein Zeichenprogramm für relle elliptischen Kurven&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="74"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="89"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:16px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/kebekus.png&quot; style=&quot;float: right;&quot; /&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Stefan Kebekus&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:600;&quot;&gt;Address&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;Stefan Kebekus&lt;br /&gt;Albert-Ludwigs-Universität Freiburg&lt;br /&gt;Ernst-Zermelo-Straße 1&lt;br /&gt;79104 Freiburg im Breisgau&lt;br /&gt;GERMANY&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:600;&quot;&gt;E-Mail&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;mailto:stefan.kebekus@math.uni-freiburg.de&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; text-decoration: underline; color:#0057ae;&quot;&gt;stefan.kebekus@math.uni-freiburg.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:600;&quot;&gt;Homepage&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://cplx.vm.uni-freiburg.de&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; text-decoration: underline; color:#0057ae;&quot;&gt;https://cplx.vm.uni-freiburg.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:16px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/kebekus.png&quot; style=&quot;float: right;&quot; /&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Stefan Kebekus&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Address&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Stefan Kebekus&lt;br /&gt;Albert-Ludwigs-Universität Freiburg&lt;br /&gt;Ernst-Zermelo-Straße 1&lt;br /&gt;79104 Freiburg im Breisgau&lt;br /&gt;GERMANY&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;E-Mail&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;mailto:stefan.kebekus@math.uni-freiburg.de&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;stefan.kebekus@math.uni-freiburg.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Homepage&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://home.mathematik.uni-freiburg.de/kebekus&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://home.mathematik.uni-freiburg.de/kebekus&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:16px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/kebekus.png&quot; style=&quot;float: right;&quot; /&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;Stefan Kebekus&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Anschrift&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Stefan Kebekus&lt;br /&gt;Albert-Ludwigs-Universität Freiburg&lt;br /&gt;Ernst-Zermelo-Straße 1&lt;br /&gt;79104 Freiburg im Breisgau&lt;br /&gt;GERMANY&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;E-Mail&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;mailto:stefan.kebekus@math.uni-freiburg.de&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;stefan.kebekus@math.uni-freiburg.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Homepage&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://cplx.vm.uni-freiburg.de&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;https://cplx.vm.uni-freiburg.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="113"/>
        <source>Acknowledgements</source>
        <translation>Danksagungen</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="119"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Inspiration&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;This program was inspired by Java applets included in the &amp;quot;ECC Tutorial&amp;quot; found on the homepage of Certicom Corp. This program does not share code or other components with any of Certicom&apos;s products.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;Link:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;monospace&apos;; font-size:9pt;&quot;&gt; &lt;/span&gt;&lt;a href=&quot;http://www.certicom.com/index.php/ecc-tutorial&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.certicom.com/index.php/ecc-tutorial&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Artwork&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;Several icons used in this program are versions of icons found in the &lt;/span&gt;&lt;a href=&quot;www.kde.org&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; text-decoration: underline; color:#0057ae;&quot;&gt;K Desktop Environment&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;, Version 4.0, or modified version of these icons.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Danksagungen&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Inspiration&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;Der Elliptic Curve Plotter entstand, nachdem Stefan Kebekus die Java-Applets gesehen hatte, die auf der Homepage der Firma Certicom im &quot;ECC Tutorial&quot; zu finden sind. Der Elliptic Curve Plotter enthält aber keine Programmteile aus Produkten von Certicom.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;Link:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;monospace&apos;; font-size:9pt;&quot;&gt; &lt;/span&gt;&lt;a href=&quot;http://www.certicom.com/index.php/ecc-tutorial&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.certicom.com/index.php/ecc-tutorial&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Piktogramme&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;Mehrere der Piktogramme, die im Elliptic Curve Plotter verwendet werden, stammen aus dem &lt;/span&gt;&lt;a href=&quot;www.kde.org&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; text-decoration: underline; color:#0057ae;&quot;&gt;K Desktop Environment&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;, oder sind modifizierte Versionen dieser.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="144"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Translations&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;The following people have kindly helped to translate the Elliptic Curve Plotter into other languages.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Übersetzungen&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;Die folgenden Personen haben freundlicherweise mitgeholfen, den Elliptic Curve Plotter in andere Sprachen zu übersetzen.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="172"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;license&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:600;&quot;&gt;Icons&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;Several icons used in this program are modified versions of icons distributed with the K Desktop Environment, Version 4.0. These icons are licensed under GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Inspiration&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;This program was inspired by Java applets included in the &amp;quot;ECC Tutorial&amp;quot; found on the homepage of Certicom Corp. This program does not share code or other components with any of Certicom&apos;s products.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Link:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;monospace&apos;;&quot;&gt; http://www.certicom.com/index.php/ecc-tutorial &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Artwork&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Several icons used in this program are versions of icons found in the K Desktop Environment, Version 4.0, or modified version of these icons.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Software used in the development&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/ermine.png&quot; style=&quot;float: right;&quot; /&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;The static Linux binaries were created using the program &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Ermine Light&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;. The authors of Ermine Light kindly supported the development of the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Elliptic CurvePlotter&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt; with a free license.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Link:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;monospace&apos;;&quot;&gt; http://www.magicermine.com&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Inspiration&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This program was inspired by Java applets included in the &amp;quot;ECC Tutorial&amp;quot; found on the homepage of Certicom Corp. This program does not share code or other components with any of Certicom&apos;s products.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Link:&lt;span style=&quot; font-family:&apos;monospace&apos;;&quot;&gt; http://www.certicom.com/index.php/ecc-tutorial &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Artwork&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Several icons used in this program are versions of icons found in the K Desktop Environment, Version 4.0, or modified version of these icons.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Software used in the development&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/ermine.png&quot; style=&quot;float: right;&quot; /&gt;The static Linux binaries werecreated using the program &lt;span style=&quot; font-weight:600;&quot;&gt;Ermine Light&lt;/span&gt;. The authors of Ermine Light kindlysupported the development of the &lt;span style=&quot; font-weight:600;&quot;&gt;Elliptic CurvePlotter&lt;/span&gt; with a free license.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Link:&lt;span style=&quot; font-family:&apos;monospace&apos;;&quot;&gt; http://www.magicermine.com&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Inspiration&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Der Elliptic Curve Plotter entstand, nachdem Stefan Kebekus die Java-Applets gesehen hatte, die auf der Homepage der Firma Certicom im &quot;ECC Tutorial&quot; zu finden sind. Der Elliptic Curve Plotter enth&amp;auml;lt aber keine Programmteile aus Produkten von Certicom.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Link:&lt;span style=&quot; font-family:&apos;monospace&apos;;&quot;&gt; http://www.certicom.com/index.php/ecc-tutorial &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Piktogramme&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Mehrere der Piktogramme, die im Elliptic Curve Plotter verwendet werden, stammen aus dem K Desktop Environment, Version 4.0, oder sind modifizierte Versionen dieser.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Software&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/ermine.png&quot; style=&quot;float: right;&quot; /&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Die statischen Binaries für Linux wurden mit Hilfe des Programmes &lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Ermine Light&lt;/span&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt; erstellt. Die Autoren von Ermine Light unterstützten die Entwicklung des &lt;/span&gt;&lt;span style=&quot; font-size:9pt; font-weight:600;&quot;&gt;Elliptic CurvePlotter&lt;/span&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt; freundlicherweise mit einer kostenlosen Lizenz.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Link:&lt;span style=&quot; font-family:&apos;monospace&apos;;&quot;&gt; http://www.magicermine.com&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;license&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Copyright (C) 2018 Stefan Kebekus&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Icons&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Several icons used in this program are modified versions of icons distributed with the K Desktop Environment, Version 4.0. These icons are licensed under GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;license&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Copyright (C) 2018 Stefan Kebekus&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Icons&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Several icons used in this program are modified versions of icons distributed with the K Desktop Environment, Version 4.0. These icons are licensed under GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;license&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Copyright (C) 2018 Stefan Kebekus&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Dieses Programm ist freie Software. Sie k&amp;ouml;nnen es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation ver&amp;ouml;ffentlicht, weitergeben und/oder modifizieren, entweder gem&amp;auml;&amp;szlig; Version 3 der Lizenz oder (nach Ihrer Option) jeder sp&amp;auml;teren Version.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Die Ver&amp;ouml;ffentlichung dieses Programms erfolgt in der Hoffnung, dass es Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE, sogar ohne die implizite Garantie der MARKTREIFE oder der VERWENDBARKEIT F&amp;Uuml;R EINEN BESTIMMTEN ZWECK. Details finden Sie in der GNU General Public License.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Piktogramme&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Mehrere der Piktogramme, die im Elliptic Curve Plotter verwendet werden, stammen aus dem K Desktop Environment, Version 4.0. Diese Piktogramme sind ebenfalls unter der GNU General Public License (Version 3 der Lizenz oder jede sp&amp;auml;tere Version) lizenziert.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;&amp;Uuml;bersetzungen der Lizenzbedingungen&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Im Internet finden Sie deutsche &amp;Uuml;bersetzungen der GNU General Public License, die Ihnen beim Verst&amp;auml;ndnis der Lizenzbedingungen helfen k&amp;ouml;nnen. Es gilt allerdings immer der englischsprachige Originaltext.&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="160"/>
        <source>License</source>
        <translation>Lizenzbedingungen</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="14"/>
        <source>About the Elliptic Curve Plotter</source>
        <translation>Über den Elliptic Curve Plotter</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Inspiration&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;This program was inspired by Java applets included in the &amp;quot;ECC Tutorial&amp;quot; found on the homepage of Certicom Corp. This program does not share code or other components with any of Certicom&apos;s products.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Link:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;monospace&apos;;&quot;&gt; &lt;/span&gt;&lt;a href=&quot;http://www.certicom.com/index.php/ecc-tutorial&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.certicom.com/index.php/ecc-tutorial&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Artwork&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Several icons used in this program are versions of icons found in the &lt;/span&gt;&lt;a href=&quot;www.kde.org&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;K Desktop Environment&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;, Version 4.0, or modified version of these icons.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Software used in the development&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/ermine.png&quot; style=&quot;float: right;&quot; /&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;The portable binaries for Linux were created using the program &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Ermine Light&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;. The authors of Ermine Light kindly supported the development of the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Elliptic CurvePlotter&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt; with a free license.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Link:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;monospace&apos;;&quot;&gt; &lt;/span&gt;&lt;a href=&quot;http://www.magicermine.com&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.magicermine.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Inspiration&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Der Elliptic Curve Plotter entstand, nachdem Stefan Kebekus die Java-Applets gesehen hatte, die auf der Homepage der Firma Certicom im &quot;ECC Tutorial&quot; zu finden sind. Der Elliptic Curve Plotter enth&amp;auml;lt aber keine Programmteile aus Produkten von Certicom.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Link: &lt;a href=&quot;http://www.certicom.com/index.php/ecc-tutorial&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.certicom.com/index.php/ecc-tutorial&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Piktogramme&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Mehrere der Piktogramme, die im Elliptic Curve Plotter verwendet werden, stammen aus dem K Desktop Environment, Version 4.0, oder sind modifizierte Versionen dieser.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Software&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/ermine.png&quot; style=&quot;float: right;&quot; /&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Die portablen Binaries für Linux wurden mit Hilfe des Programmes &lt;/span&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Ermine Light&lt;/span&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt; erstellt. Die Autoren von Ermine Light unterstützten die Entwicklung des &lt;/span&gt;&lt;span style=&quot; font-size:9pt; font-weight:600;&quot;&gt;Elliptic CurvePlotter&lt;/span&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt; freundlicherweise mit einer kostenlosen Lizenz.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Link: &lt;a href=&quot;http://www.magicermine.com&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.magicermine.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="138"/>
        <source>Translations</source>
        <translation>Übersetzungen</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Translations&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The following people have kindly helped to translate the Elliptic Curve Plotter into other languages.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Übersetzungen&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Die folgenden Personen haben freundlicherweise mitgeholfen, den Elliptic Curve Plotter in andere Sprachen zu übersetzen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;license&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Copyright (C) 2018 Stefan Kebekus&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Icons&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Several icons used in this program are modified versions of icons distributed with the K Desktop Environment, Version 4.0. These icons are licensed under GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;license&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Copyright (C) 2018 Stefan Kebekus&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Dieses Programm ist freie Software. Sie k&amp;ouml;nnen es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation ver&amp;ouml;ffentlicht, weitergeben und/oder modifizieren, entweder gem&amp;auml;&amp;szlig; Version 3 der Lizenz oder (nach Ihrer Option) jeder sp&amp;auml;teren Version.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Die Ver&amp;ouml;ffentlichung dieses Programms erfolgt in der Hoffnung, dass es Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE, sogar ohne die implizite Garantie der MARKTREIFE oder der VERWENDBARKEIT F&amp;Uuml;R EINEN BESTIMMTEN ZWECK. Details finden Sie in der GNU General Public License.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Piktogramme&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Mehrere der Piktogramme, die im Elliptic Curve Plotter verwendet werden, stammen aus dem K Desktop Environment, Version 4.0. Diese Piktogramme sind ebenfalls unter der GNU General Public License (Version 3 der Lizenz oder jede sp&amp;auml;tere Version) lizenziert.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;&amp;Uuml;bersetzungen der Lizenzbedingungen&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Im Internet finden Sie deutsche &amp;Uuml;bersetzungen der GNU General Public License, die Ihnen beim Verst&amp;auml;ndnis der Lizenzbedingungen helfen k&amp;ouml;nnen. Es gilt allerdings immer der englischsprachige Originaltext.&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="201"/>
        <source>GPL</source>
        <translation>GNU Public License</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="342"/>
        <source>FDL</source>
        <translation>Free Documentation License</translation>
    </message>
</context>
<context>
    <name>ecDescription</name>
    <message>
        <location filename="ecDescription.cpp" line="85"/>
        <source>Points on the elliptic curve</source>
        <translation>Punkte auf der elliptischen Kurve</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="89"/>
        <source>Experiment with the curve and its group law!</source>
        <translation>Experimentieren Sie mit der Kurve und ihrem Gruppengesetz!</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="91"/>
        <source>Use the sliders to change the shape of the curve.</source>
        <translation>Verwenden Sie die Schieberegler, um die Gestalt der Kurve zu ändern.</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="93"/>
        <source>Use the sliders to change the shape of the curve, or drag some points to the curve and use the menu &apos;Group Law&apos; to experiment with the group law.</source>
        <translation>Verwenden Sie die Schieberegler, um die Gestalt der Kurve zu ändern, oder ziehen Sie einige Punkte mit der Maus auf die Kurve und verwenden Sie das Menü &apos;Gruppengesetz&apos; um mit dem Gruppengesetz zu experimentieren.</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="105"/>
        <source>The inverse of the point &lt;b&gt;P&lt;/b&gt;</source>
        <translation>Das Inverse des Punktes &lt;b&gt;P&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="118"/>
        <source>The double of the point &lt;b&gt;P&lt;/b&gt;</source>
        <translation>Die Verdopplung des Punktes &lt;b&gt;P&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="135"/>
        <source>Sums of points</source>
        <translation>Summen von Punkten</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="154"/>
        <source>Sums of three points</source>
        <translation>Summen von drei Punkten</translation>
    </message>
</context>
<context>
    <name>ecPlotter</name>
    <message>
        <location filename="ecPlotter.cpp" line="189"/>
        <source>&lt;h4&gt;Point Box&lt;/h4&gt;&lt;p&gt;Drag the points from this box to the curve and vice versa.&lt;/p&gt;</source>
        <oldsource>&lt;h4&gt;Point Box&lt;/h4&gt;&lt;p&gt;Use the mouse to drag the points from this box to the curve and vice versa.&lt;/p&gt;</oldsource>
        <translation>&lt;h4&gt;Punktekiste&lt;/h4&gt;&lt;p&gt;Sie können die Punkte aus dieser Kiste auf die Kurve ziehen --und zurück.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ecPlotter.cpp" line="187"/>
        <source>Point Box</source>
        <translation>Punktekiste</translation>
    </message>
    <message>
        <location filename="ecPlotter.cpp" line="215"/>
        <source>Drag the points to the curve, and use the &apos;Group Law&apos; menu to experiment with the group structure.</source>
        <oldsource>Use the mouse to drag the points to the curve, and use the &apos;Group Law&apos; menu to experiment with the group structure.</oldsource>
        <translation>Ziehen Sie die Punkte auf die Kurve und verwenden Sie das Menü &apos;Gruppengesetz&apos;, um mit der Gruppenstruktur zu experimentieren.</translation>
    </message>
    <message>
        <location filename="ecPlotter.cpp" line="217"/>
        <source>Drag the points to the curve.</source>
        <oldsource>Use the mouse to drag the points to the curve.</oldsource>
        <translation>Ziehen Sie die Punkte auf die Kurve.</translation>
    </message>
    <message>
        <location filename="ecPlotter.cpp" line="220"/>
        <source>Drag the points along the curve, and use the &apos;Group Law&apos; menu to experiment with the group structure.</source>
        <oldsource>Use the mouse to drag the points along the curve, and use the &apos;Group Law&apos; menu to experiment with the group structure.</oldsource>
        <translation>Bewegen Sie die Punkte entlang der Kurve. Benutzen Sie das Menü &apos;Gruppengesetz&apos;, um mit der Gruppenstruktur zu experimentieren.</translation>
    </message>
</context>
<context>
    <name>ec_real</name>
    <message>
        <location filename="ec_real.cpp" line="66"/>
        <source>&lt;h4&gt;Elliptic Curve&lt;/h4&gt;&lt;p&gt;The curve outlined in black is the elliptic curve defined by the equation&lt;/p&gt;&lt;p&gt;%1.&lt;/p&gt; </source>
        <translation>&lt;h4&gt;Elliptische Kurve&lt;/h4&gt;&lt;p&gt;Die schwarz gezeichnete Kurve ist die elliptische Kurve, die durch folgende Gleichung bestimmt ist:&lt;/p&gt;&lt;p&gt;%1.&lt;/p&gt; </translation>
    </message>
    <message>
        <location filename="ec_real.cpp" line="70"/>
        <source>&lt;h4&gt;Curve&lt;/h4&gt;&lt;p&gt;The curve outlined in black is the (non-elliptic) curve defined by the equation&lt;/p&gt;&lt;p&gt;%1.&lt;/p&gt; </source>
        <translation>&lt;h4&gt;Kurve&lt;/h4&gt;&lt;p&gt;Die schwarz gezeichnete Kurve ist keine elliptische Kurve. Die Kurve ist durch folgende Gleichung gegeben:&lt;/p&gt;&lt;p&gt;%1.&lt;/p&gt; </translation>
    </message>
    <message>
        <location filename="ec_real.cpp" line="95"/>
        <source>&lt;h2&gt;Neil Parabola&lt;/h2&gt;&lt;p&gt;This curve is not an elliptic curve. It is a singular curve,  known as the &lt;span style=&quot;font-style: italic;&quot;&gt;Neil parabola&lt;/span&gt;  or &lt;span style=&quot;font-style: italic;&quot;&gt;semicubical parabola&lt;/span&gt;.  The singularity at the origin is usually called a &lt;span    style=&quot;font-style: italic;&quot;&gt;cusp&lt;/span&gt;. The Neil parabola is a  rational curve. This means that it can be parameterized using only  rational functions and without using transzendental functions such  as sine or cosine. One such parameterization is given as t &amp;#8594;  (t&amp;sup2;, t&amp;sup3;).&amp;nbsp;&lt;/p&gt;&lt;p&gt;This curve does not have a group structure. There is, however, a  group structure on the smooth locus of the curve. This group is  isomorphic to the group of real numbers with addition. The Neil  parabola is named after William Neil (1637&amp;#8212;1670) who first  computed its arc length. Historically, the Neil parabola was the  first algebraic curve whose arc length could be computed.&lt;/p&gt;</source>
        <translation>&lt;h2&gt;Neilsche Parabel&lt;/h2&gt;
&lt;p&gt;Diese Kurve ist keine elliptische Kurve, sondern eine singul&amp;auml;re rationale Kurve, die auch als &lt;span style=&quot;font-style: italic;&quot;&gt;Neilsche Parabel&lt;/span&gt; oder als &lt;span style=&quot;font-style: italic;&quot;&gt;semikubische Parabel &lt;/span&gt; bekannt ist. Die Singularit&amp;auml;t, die Sie im Nullpunkt sehen, hei&amp;szlig;t &lt;span  style=&quot;font-style: italic;&quot;&gt;Spitze&lt;/span&gt;.&lt;/p&gt;
&lt;p&gt;Die Neilsche Parabel ist eine rationale Kurve. Das bedeutet, dass man die Kurve mit Hilfe rationaler Funktionen parametrisieren kann, ohne dass man auf transzendente Funktionen wie etwa Sinus oder Cosinus zurückgreifen muß. Eine solche Parametrisierung ist wie folgt gegeben: t &amp;#8594;  (t&amp;sup2;, t&amp;sup3;).&amp;nbsp;&lt;/p&gt;
&lt;p&gt;Die Punkte der Neilschen Parabel bilden keine Gruppe. Es gibt allerdings eine Gruppenstruktur auf dem glatten Ort der Kurve. Die Gruppe der glatten Punkte kann mit den reellen Zahlen identizifiert werden, wobei die Gruppenverknüpfung auf den reellen Zahlen durch die Addition gegeben ist. Die Neilsche Parabel ist nach William Neil (1637&amp;#8212;1670) benannt, der die Bogenl&amp;auml;nge dieser Kurve berechnete. Historisch war die Neilsche Parabel die erste algebraische Kurve, deren Bogenl&amp;auml;nge berechnet werden konnte.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_real.cpp" line="112"/>
        <source>&lt;h2&gt;Nodal Plane Cubic&lt;/h2&gt;&lt;p&gt;This curve is not an elliptic curve. It is a singular curve,  known as the &lt;span style=&quot;font-style: italic;&quot;&gt;Nodal Plane Cubic&lt;/span&gt;.  The singularity is usually called a &lt;span style=&quot;font-style:    italic;&quot;&gt;node&lt;/span&gt;.&lt;/p&gt;&lt;p&gt;The nodal plane cubic is a rational curve. This means that it can  be parameterized using only rational functions and without using  transzendental functions such as sine or cosine. One such  parameterization is given as t &amp;#8594; ( t&amp;sup2;-2, t&amp;sup3;-3t).&lt;/p&gt;&lt;p&gt; This curve does not have a group structure. There is, however, a  group structure on the smooth locus of the curve. This group is  isomorphic to the group of non-zero real numbers with  multiplication.&lt;/p&gt;</source>
        <translation>&lt;h2&gt;Ebene Knotenkurve&lt;/h2&gt;
&lt;p&gt;Diese Kurve ist keine elliptische Kurve, sondern eine singul&amp;auml;re Kurve, die als &lt;span style=&quot;font-style: italic;&quot;&gt;ebene Knotenkurve&lt;/span&gt; bekannt ist. Die Singularit&amp;auml;t im Nullpunkt wird &amp;uuml;blicherweise als &lt;span  style=&quot;font-style: italic;&quot;&gt;Knoten&lt;/span&gt; bezeichnet.&lt;/p&gt;
&lt;p&gt;Die Knotenkurve eine rationale Kurve. Das bedeutet, dass man die Kurve mit Hilfe rationaler Funktionen parametrisieren kann, ohne dass man auf transzendente Funktionen wie etwa Sinus oder Cosinus zurückgreifen muß. Eine solche Parametrisierung ist wie folgt gegeben: t &amp;#8594; ( t&amp;sup2;-2, t&amp;sup3;-3t).&lt;/p&gt;
&lt;p&gt;Die Punkte der Knotenkurve bilden keine Gruppe. Es gibt aber eine Gruppenstruktur auf dem glatten Ort der Kurve.  Die Gruppe der glatten Punkte kann mit den nicht-verschwindenden reellen Zahlen identizifiert werden, wobei die Gruppenverknüpfung durch die Multiplikation gegeben ist.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_real.cpp" line="126"/>
        <source>&lt;h2&gt;Curve with Isolated Point&lt;/h2&gt;&lt;p&gt;This is a singular curve, and not a smooth elliptic curve. Over the  real numbers, the equation defines the curve that you see on the left,  and an additional point with coordinates (-1, 0). If you move the  slider for the value &lt;span style=&quot;font-style: italic;&quot;&gt;b&lt;/span&gt; a  little you can see that the point is a degenerate form of the oval that  you see in many smooth elliptic curves.&lt;/p&gt;&lt;p&gt;The curve with the isolated point does not form a group.&lt;/p&gt;</source>
        <translation>&lt;h2&gt;Kurve mit isoliertem Punkt&lt;/h2&gt;
&lt;p&gt;Die Abbildung zeigt eine singul&amp;auml;re Kurve, die keine elliptische Kurve ist. Die Gleichung definiert die links abgebildete Kurve und einen zus&amp;auml;tzlichen Punkt, der die Koordinaten (-1, 0) hat. Wenn Sie den Schieberegler f&amp;uuml;r den Wert &lt;span style=&quot;font-style: italic;&quot;&gt;b&lt;/span&gt; ein wenig bewegen, k&amp;ouml;nnen Sie sehen, dass der Punkt eine degeneriere Form des Ovals ist, das Sie in vielen elliptischen Kurven sehen.&lt;/p&gt;
&lt;p&gt;Die Punkte dieser Kurve bilden keine Gruppe.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_real.cpp" line="135"/>
        <source>&lt;h2&gt;Elliptic Curve with Oval&lt;/h2&gt;&lt;p&gt;This curve is a smooth elliptic curve. The points of this curve  form a group.&lt;/p&gt;&lt;p&gt;This elliptic curve consists of two parts, classically called  &quot;oval&quot; and &quot;parabola&quot;. Interestingly, it is not possible to define  the oval or the parabola alone with algebraic equations: any  polynomial function that vanishes along one component must  necessarily also vanish along the other.&lt;/p&gt;</source>
        <translation>&lt;h2&gt;Elliptische Kurve mit Oval&lt;/h2&gt;
&lt;p&gt;Die Abbildung zeigt eine glatte elliptische Kurve. Die Punkte dieser Kurve bilden eine Gruppe.&lt;/p&gt;
&lt;p&gt;Diese elliptische Kurve besteht aus zwei Teilen, die &amp;uuml;blicherweise &quot;Oval&quot; und &quot;Parabel&quot; genannt werden. Interessanterweise ist es nicht m&amp;ouml;glich, das Oval oder die Parabel allein durch algebraische Gleichungen zu definieren, denn jede polynomielle Funktion, die auf einer der beiden Komponenten verschwindet, verschwindet notwendigerweise immer auch auf der anderen Komponente.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_real.cpp" line="144"/>
        <source>&lt;h2&gt;Elliptic Curve without Oval&lt;/h2&gt;&lt;p&gt;This curve is a smooth elliptic curve. The points of this curve  form a group.&lt;/p&gt;</source>
        <translation>&lt;h2&gt;Elliptische Kurve ohne Oval&lt;/h2&gt;
&lt;p&gt;Die Abbildung zeigt eine glatte elliptische Kurve. Die Punkte dieser Kurve bilden eine Gruppe.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>ec_realDoublePoint</name>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="102"/>
        <source>&lt;h4&gt;Auxiliary Line&lt;/h4&gt;&lt;p&gt;The point &lt;b&gt;%1&lt;/b&gt; is constructed as the image of the small yellow point under reflection against the x-axis. This auxiliary line connects the point &lt;b&gt;%1&lt;/b&gt; and the small yellow point.&lt;/p&gt;&lt;p&gt;Move the mouse pointer to the point &lt;b&gt;%1&lt;/b&gt; to learn more about the construction.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;Hilfslinie&lt;/h4&gt;&lt;p&gt;Man konstruiert den Punkt &lt;b&gt;%1&lt;/b&gt;, indem man den kleinen gelben Punkt an der x-Achse spiegelt. Diese Hilfslinie verbindet die Punkte &lt;b&gt;%1&lt;/b&gt; und den kleinen gelben Punkt und illustriert so die Konstruktion.&lt;/p&gt;&lt;p&gt;Bewegen Sie den Mauszeiger zum Punkt &lt;b&gt;%1&lt;/b&gt; um mehr über die Konstruktion zu erfahren.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="108"/>
        <source>&lt;h4&gt;Auxiliary Point&lt;/h4&gt;&lt;p&gt;This is an auxiliary point used in the construction of the double of the point &lt;b&gt;%1&lt;/b&gt;. The double of &lt;b&gt;%1&lt;/b&gt; is constructed by looking at the tangent line through &lt;b&gt;%1&lt;/b&gt;. The tangent line is guaranteed to intersect the elliptic curve in the point &lt;b&gt;%1&lt;/b&gt; and in precisely one second point, the point you are looking at right now.&lt;/p&gt;&lt;p&gt;To learn more about the construction, move the mouse pointer to the tangent line or to the point &lt;b&gt;%2&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;Hilfspunkt&lt;/h4&gt;&lt;p&gt;Dies ist ein Hilfpunkt, der bei der Konstruktion der Verdopplung des Punktes &lt;b&gt;%1&lt;/b&gt; verwendet wird. Man konstruiert das Doppelte von &lt;b&gt;%1&lt;/b&gt;, indem man die Tangentialgerade durch den Punkt &lt;b&gt;%1&lt;/b&gt; betrachtet. Die Tangentialgerade schneidet die elliptische Kurve in dem Punkt &lt;b&gt;%1&lt;/b&gt; und in genau einem weiteren Punkt, nämlich dem Punkt, den Sie gerade betrachten.&lt;/p&gt;&lt;p&gt;Bewegen Sie den Mauszeiger auf die Tangentialgerade oder auf den Punkt &lt;b&gt;%2&lt;/b&gt; um mehr über die Konstruktion der Verdopplung zu erfahren.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="59"/>
        <source>The point &lt;b&gt;%1&lt;/b&gt; is the double of the point &lt;b&gt;%2&lt;/b&gt; according to the group law of the elliptic curve. Geometrically, the point &lt;b&gt;%1&lt;/b&gt; is constructed by taking the tangent line through &lt;b&gt;%2&lt;/b&gt;. This tangent line intersects the elliptic curve in the point &lt;b&gt;%2&lt;/b&gt;, and in the point at infinity.</source>
        <translation>Der Punkt &lt;b&gt;%1&lt;/b&gt; ist das Doppelte des Punktes &lt;b&gt;%2&lt;/b&gt; gemäß dem Gruppengesetz der elliptischen Kurve. Man konstruiert den Punkt &lt;b&gt;%1&lt;/b&gt; geometrisch, indem man die Tangentialgerade durch den Punkt &lt;b&gt;%2&lt;/b&gt; betrachtet. Die Tangentialgerade schneidet die elliptische Kurve im Punkt &lt;b&gt;%2&lt;/b&gt; und im unendlich fernen Punkt.</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="64"/>
        <source>The point &lt;b&gt;%1&lt;/b&gt; is the double of the point &lt;b&gt;%2&lt;/b&gt; according to the group law of the elliptic curve. Geometrically, the point &lt;b&gt;%1&lt;/b&gt; is constructed by taking the tangent line through &lt;b&gt;%2&lt;/b&gt;. This tangent line will intersect the elliptic curve in the point &lt;b&gt;%2&lt;/b&gt;, and in exactly one second point. The double of &lt;b&gt;%2&lt;/b&gt; is then constructed as the image of the second point under reflection against the x-axis.</source>
        <translation>Der Punkt &lt;b&gt;%1&lt;/b&gt; ist das Doppelte des Punktes &lt;b&gt;%2&lt;/b&gt; gemäß dem Gruppengesetz der elliptischen Kurve. Man konstruiert den Punkt &lt;b&gt;%1&lt;/b&gt; geometrisch indem man die Tangentialgerade durch den Punkt &lt;b&gt;%2&lt;/b&gt; betrachtet. Die Tangentialgerade schneidet die elliptische Kurve im Punkt &lt;b&gt;%2&lt;/b&gt; und in genau einem weiteren Punkt. Man konstruiert das Doppelte von &lt;b&gt;%2&lt;/b&gt; dann, indem man den weiteren Punkt an der x-Achse spiegelt.</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="118"/>
        <source>&lt;h4&gt;Double of the point &lt;b&gt;%1&lt;/b&gt;&lt;/h4&gt;</source>
        <translation>&lt;h4&gt;Verdopplung des Punktes &lt;b&gt;%1&lt;/b&gt;&lt;/h4&gt;</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="120"/>
        <source>&lt;p&gt;Move the point &lt;b&gt;%1&lt;/b&gt; to see how the double of &lt;b&gt;%1&lt;/b&gt; changes along with &lt;b&gt;%1&lt;/b&gt;. You can also use the sliders to change the shape of the elliptic curve, and to see how the whole construction changes.&lt;/p&gt;</source>
        <oldsource>&lt;p&gt;Move the point &lt;b&gt;%1&lt;/b&gt; with the mouse to see how the double of &lt;b&gt;%1&lt;/b&gt; changes along with &lt;b&gt;%1&lt;/b&gt;. You can also use the sliders to change the shape of the elliptic curve, and to see how the whole construction changes.&lt;/p&gt;</oldsource>
        <translation>&lt;p&gt;Bewegen Sie den Punkt &lt;b&gt;%1&lt;/b&gt; entlang der Kurve um zu sehen, wie sich dabei das Doppelte von &lt;b&gt;%1&lt;/b&gt; ändert. Sie können auch mit Hilfe der Schieberegler die Gestalt der Kurve variieren und schauen, wie sich dabei die Gesamtkonstruktion ändert.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="125"/>
        <source>&lt;h4&gt;Tangent Line&lt;/h4&gt;&lt;p&gt;This line is the tangent line through &lt;b&gt;%2&lt;/b&gt;, an auxiliary line used in the construction of the point &lt;b&gt;%1&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;Tangentialgerade&lt;/h4&gt;&lt;p&gt;Diese Gerade ist die Tangentialgerade durch den Punkt &lt;b&gt;%2&lt;/b&gt;. Die Tangentialgerade spielt bei der Konstruktion des Punktes &lt;b&gt;%1&lt;/b&gt; eine Rolle.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="129"/>
        <source>&lt;p&gt;The tangent line is the unique line which contains &lt;b&gt;%1&lt;/b&gt; and just touches the ellipticcurve, without intersecting it transversely. The tangent line is guaranteed to intersect the elliptic curve in the point &lt;b&gt;%1&lt;/b&gt; and in precisely one second point, which is offscreen and therefore not shown in the picture.&lt;/p&gt;&lt;p&gt;The point &lt;b&gt;%2&lt;/b&gt; is constructed as the image of the second intersection under reflection against the x-axis.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die Tangentialgerade ist die eindeutige Gerade durch den Punkt &lt;b&gt;%1&lt;/b&gt;, die die elliptische Kurve lediglich berührt und nicht transversal schneidet. Die Tangentialgerade schneidet die elliptische Kurve im Punkt &lt;b&gt;%1&lt;/b&gt; und in genau einem weiteren Punkt, der allerdings außerhalb des Anzeigebereiches liegt und deshalb nicht zu sehen ist.&lt;/p&gt;&lt;p&gt;Man konstruiert den Punkt &lt;b&gt;%2&lt;/b&gt;, indem man den zweiten Schnittpunkt an der x-Achse spiegelt.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="135"/>
        <source>&lt;p&gt;The tangent line is the unique line which contains &lt;b&gt;%1&lt;/b&gt; and just touches the ellipticcurve, without intersecting it transversely. The tangent line is guaranteed to intersect the elliptic curve in the point &lt;b&gt;%1&lt;/b&gt; and in precisely one second point.&lt;/p&gt;&lt;p&gt;Move the mouse pointer to the point &lt;b&gt;%2&lt;/b&gt; to learn more about the construction.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die Tangentialgerade ist die eindeutige Gerade durch den Punkt &lt;b&gt;%1&lt;/b&gt;, die die elliptische Kurve lediglich berührt und nicht transversal schneidet. Die Tangentialgerade schneidet die elliptische Kurve im Punkt &lt;b&gt;%1&lt;/b&gt; und in genau einem weiteren Punkt.&lt;/p&gt;&lt;p&gt;Bewegen Sie den Mauszeiger zum Punkt &lt;b&gt;%2&lt;/b&gt; um mehr über die Konstruktion zu erfahren.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>ec_realFreePoint</name>
    <message>
        <location filename="ec_realFreePoint.cpp" line="71"/>
        <source>Point</source>
        <translation>Punkt</translation>
    </message>
    <message>
        <location filename="ec_realFreePoint.cpp" line="73"/>
        <source>Move the point along the curve, or move it to its home position.</source>
        <oldsource>Use the mouse to move the point along the curve, or move it to its home position.</oldsource>
        <translation>Bewegen Sie den Punkt auf der Kurve oder ziehen Sie den Punkt zurück in die Punktekiste.</translation>
    </message>
    <message>
        <location filename="ec_realFreePoint.cpp" line="89"/>
        <source>This point cannot be moved to the curve right now, because the curve is not an elliptic curve. Before moving the point, use the sliders to change the shape of the curve.</source>
        <translation>Dieser Punkt kann im Moment nicht auf die Kurve gezogen werden, weil die Kurve keine elliptische Kurve ist. Benutzen Sie die Schieberegler, um die Gestalt der Kurve zu ändern, bevor Sie den Punkt bewegen.</translation>
    </message>
    <message>
        <location filename="ec_realFreePoint.cpp" line="99"/>
        <source>Drag the point to the curve!</source>
        <oldsource>Use the mouse to drag the point to the curve!</oldsource>
        <translation>Ziehen Sie den Punkt auf die elliptische Kurve!</translation>
    </message>
</context>
<context>
    <name>ec_realMinusPoint</name>
    <message>
        <location filename="ec_realMinusPoint.cpp" line="86"/>
        <source>&lt;h4&gt;Auxiliary Line&lt;/h4&gt;&lt;p&gt;The point &lt;b&gt;%1&lt;/b&gt; is constructed as the image of the point &lt;b&gt;%2&lt;/b&gt; under reflection against the x-axis. This auxiliary line connects the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%1&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;Hilfslinie&lt;/h4&gt;&lt;p&gt;Man konstruiert den Punkt &lt;b&gt;%1&lt;/b&gt;, indem man den Punkt &lt;b&gt;%2&lt;/b&gt; an der x-Achse spiegelt. Diese Hilfslinie verbindet die Punkte &lt;b&gt;%2&lt;/b&gt; und &lt;b&gt;%1&lt;/b&gt; und illustriert die Konstruktion.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realMinusPoint.cpp" line="57"/>
        <source>The point &lt;b&gt;%1&lt;/b&gt; is the inverse of the point &lt;b&gt;%2&lt;/b&gt; according to the group law of the elliptic curve. Geometrically, the point &lt;b&gt;%1&lt;/b&gt; is constructed as the image of the point &lt;b&gt;%2&lt;/b&gt; under reflection against the x-axis.</source>
        <oldsource>The point &lt;b&gt;%1&lt;/b&gt; is the inverse of the point &lt;b&gt;%2&lt;/b&gt; according to the group law of the elliptic curve. Geometrically, the point &lt;b&gt;%1&lt;/b&gt; is constructed as the image of the point &lt;b&gt;%2&lt;/b&gt; under reflection along the x-axis.</oldsource>
        <translation>Der Punkt &lt;b&gt;%1&lt;/b&gt; ist das Inverse des Punkte &lt;b&gt;%2&lt;/b&gt; bezüglich des Gruppengesetzes der elliptischen Kurve. Man konstruiert den Punkt &lt;b&gt;%1&lt;/b&gt; geometrisch, indem man den Punkt &lt;b&gt;%2&lt;/b&gt; an der x-Achse spiegelt.</translation>
    </message>
    <message>
        <location filename="ec_realMinusPoint.cpp" line="93"/>
        <source>Inverse of the Point &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Inverses des Punktes &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="ec_realMinusPoint.cpp" line="95"/>
        <source>Move the point &lt;b&gt;%1&lt;/b&gt; to see how the inverse changes along with &lt;b&gt;%1&lt;/b&gt;. You can also change the shape of the curve to see how the whole construction changes.</source>
        <oldsource>Move the point &lt;b&gt;%1&lt;/b&gt; with the mouse to see how the inverse changes along with &lt;b&gt;%1&lt;/b&gt;. You can also change the shape of the curve to see how the whole construction changes.</oldsource>
        <translation>Bewegen Sie den Punkt &lt;b&gt;%1&lt;/b&gt; entlang der Kurve um zu sehen, wie sich dabei das Inverse ändert. Sie können auch mit Hilfe der Schieberegler die Gestalt der elliptischen Kurve variieren, um zu schauen, wie sich dabei die Gesamtkonstruktion ändert.</translation>
    </message>
</context>
<context>
    <name>ec_realPoint</name>
    <message>
        <location filename="ec_realPoint.cpp" line="125"/>
        <source>Point</source>
        <translation>Punkt</translation>
    </message>
    <message>
        <location filename="ec_realPoint.cpp" line="149"/>
        <source>The point &lt;b&gt;%1&lt;/b&gt; is the point at infinity and therefore not shown on the screen.</source>
        <translation>Der Punkt &lt;b&gt;%1&lt;/b&gt; ist der unendlich ferne Punkt der elliptischen Kurve und kann deshalb nicht angezeigt werden.</translation>
    </message>
    <message>
        <location filename="ec_realPoint.cpp" line="152"/>
        <source>The coordinates of the point &lt;b&gt;%1&lt;/b&gt; are approximately (%2, %3).</source>
        <translation>Die Koordinaten des Punktes &lt;b&gt;%1&lt;/b&gt; sind näherungsweise (%2, %3).</translation>
    </message>
    <message>
        <location filename="ec_realPoint.cpp" line="154"/>
        <source>The point is &lt;b&gt;%1&lt;/b&gt; is a special point of the elliptic curve. If you use the group law of the curve to add the point &lt;b&gt;%1&lt;/b&gt; to itself, the point at infinity is obtained as a result. The inverse of &lt;b&gt;%1&lt;/b&gt; is the same as &lt;b&gt;%1&lt;/b&gt;. Mathematicians say that &lt;b&gt;%1&lt;/b&gt; is a 2-torsion point of the elliptic curve.</source>
        <translation>Der Punkt &lt;b&gt;%1&lt;/b&gt; ist ein besonderer Punkt der elliptischen Kurve. Wenn man den Punkt &lt;b&gt;%1&lt;/b&gt; mit Hilfe des Gruppengesetzes verdoppelt, ergibt sich der unendlich ferne Punkt. Zusätzlich gilt, dass der Punkte &lt;b&gt;%1&lt;/b&gt; sein eigenes Inverses ist. In der Mathematik sagt man, dass &lt;b&gt;%1&lt;/b&gt; ein 2-Torsionspunkt der elliptischen Kurve ist.</translation>
    </message>
    <message>
        <location filename="ec_realPoint.cpp" line="159"/>
        <source> Because the coordinates are so large, the point is offscreen and therefore not shown.</source>
        <translation> Da die Koordinaten sehr groß sind, liegt der Punkt außerhalb des Anzeigebereiches.</translation>
    </message>
</context>
<context>
    <name>ec_realSumPoint</name>
    <message>
        <location filename="ec_realSumPoint.cpp" line="123"/>
        <source>&lt;h4&gt;Auxiliary Line&lt;/h4&gt;&lt;p&gt;The point &lt;b&gt;%1&lt;/b&gt; is constructed as the image of the small yellow point under reflection against the x-axis. This auxiliary line connects the points &lt;b&gt;%1&lt;/b&gt; and the small yellow point.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;Hilfslinie&lt;/h4&gt;&lt;p&gt;Man konstruiert den Punkt &lt;b&gt;%1&lt;/b&gt;, indem man den kleinen gelben Punkt an der x-Achse spiegelt. Diese Hilfslinie verbindet den Punkt &lt;b&gt;%1&lt;/b&gt; mit dem kleinen gelben Punkt und illustriert die Konstruktion.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="128"/>
        <source>&lt;h4&gt;Auxiliary Point&lt;/h4&gt;&lt;p&gt;This is an auxiliary point used in the construction of the sum &lt;b&gt;%1&lt;/b&gt;. The sum is constructed by looking at the secant line through &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;, The secant line is guaranteed to intersect the elliptic curve in precisely one further point, the point you are looking at right now.&lt;/p&gt;&lt;p&gt;Move the mouse pointer to the point &lt;b&gt;%1&lt;/b&gt; to learn more about the construction.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;Hilfspunkt&lt;/h4&gt;&lt;p&gt;Dies ist ein Hilfpunkt, der bei der Konstruktion der Summe &lt;b&gt;%1&lt;/b&gt; verwendet wird. Man konstruiert die Summe, indem man die Sekante durch die Punkte &lt;b&gt;%2&lt;/b&gt; und &lt;b&gt;%3&lt;/b&gt; betrachtet. Die Sekante schneidet die elliptische Kurve dann in genau einem weiteren Punkt, nämlich dem Punkt, den Sie gerade betrachten.&lt;/p&gt;&lt;p&gt;Bewegen Sie den Mauszeiger auf die Sekante oder auf den Punkt &lt;b&gt;%1&lt;/b&gt; um mehr über die Konstruktion der Verdopplung zu erfahren.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="66"/>
        <source>The point &lt;b&gt;%1&lt;/b&gt; is the sum of the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt; according to the group law of the elliptic curve. Geometrically, the point &lt;b&gt;%1&lt;/b&gt; is constructed by taking the line through &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;. This line will intersect the elliptic curve in the exactly one further point, which is offscreen and therefore not shown in the picture. The sum &lt;b&gt;%1&lt;/b&gt; is then constructed as the image of the third point under reflection against the x-axis.</source>
        <translation>Der Punkt &lt;b&gt;%1&lt;/b&gt; ist die Summe der Punkte &lt;b&gt;%2&lt;/b&gt; und &lt;b&gt;%3&lt;/b&gt; gemäß dem Gruppengesetz der elliptischen Kurve. Man konstruiert den Punkt &lt;b&gt;%1&lt;/b&gt; geometrisch indem man die Sekante durch die Punkte &lt;b&gt;%2&lt;/b&gt; und &lt;b&gt;%3&lt;/b&gt; betrachtet. Die Sekante schneidet die elliptische Kurve dann in genau einem weiteren Punkt, der allerdings außerhalb des Anzeigebereiches liegt. Man konstruiert die Summe &lt;b&gt;%1&lt;/b&gt; dann, indem man den weiteren Punkt an der x-Achse spiegelt.</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="72"/>
        <source>The point &lt;b&gt;%1&lt;/b&gt; is the sum of the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt; according to the group law of the elliptic curve. Geometrically, the point &lt;b&gt;%1&lt;/b&gt; is constructed by taking the line through &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;. This line will intersect the elliptic curve in the exactly one further point, shown as a small yellow circle. The sum &lt;b&gt;%1&lt;/b&gt; is then constructed as the image of the third point under reflection against the x-axis.</source>
        <translation>Der Punkt &lt;b&gt;%1&lt;/b&gt; ist die Summe der Punkte &lt;b&gt;%2&lt;/b&gt; und &lt;b&gt;%3&lt;/b&gt; gemäß dem Gruppengesetz der elliptischen Kurve. Man konstruiert den Punkt &lt;b&gt;%1&lt;/b&gt; geometrisch indem man die Sekante durch die Punkte &lt;b&gt;%2&lt;/b&gt; und &lt;b&gt;%3&lt;/b&gt; betrachtet. Die Sekante schneidet die elliptische Kurve dann in genau einem weiteren Punkt, der als kleiner gelber Punkt eingezeichnet ist. Man konstruiert die Summe &lt;b&gt;%1&lt;/b&gt; dann, indem man den kleinen gelben Punkt an der x-Achse spiegelt.</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="90"/>
        <source>The figure shows that the point &lt;b&gt;%1&lt;/b&gt; can also be constructed as the sum of the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;. This illustrates the fundamental fact that the order does not play any role when adding points on an elliptic curve. Mathematicians express this fact by saying that &apos;addition on an elliptic curve is associative&apos;. Associativity is a consequence of &apos;Noether&apos;s residual intersection theorem&apos;, named after the german mathematician Max Noether (1844-1921).</source>
        <translation>Die Zeichnung zeigt, dass der Punkt &lt;b&gt;%1&lt;/b&gt; auch konstruiert werden kann, indem man die Summe der Punkte &lt;b&gt;%2&lt;/b&gt; und &lt;b&gt;%3&lt;/b&gt; bildet. Dies illustriert die fundamentale Tatsache, dass die Reihenfolge der Summation bei Punkten auf der elliptischen Kurve keine Rolle spielt. In der Fachsprache der Mathematiker sagt man, &apos;die Addition auf elliptischen Kurven in assoziativ&apos;. Assoziativität ist eine Konsequenz aus &apos;Noether&apos;s Fundamentalsatz&apos;, benannt nach dem deutschen Mathematiker Max Noether (1844-1921).</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="138"/>
        <source>Sum of the points &lt;b&gt;%1&lt;/b&gt; and  &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>Summe der Punkte &lt;b&gt;%1&lt;/b&gt; und &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="140"/>
        <source>Move one of the points &lt;b&gt;%1&lt;/b&gt; or &lt;b&gt;%2&lt;/b&gt; to see how the sum changes. You can also change the shape of the curve to see how the whole construction changes.</source>
        <oldsource>Move one of the points &lt;b&gt;%1&lt;/b&gt; or &lt;b&gt;%2&lt;/b&gt; with the mouse to see how the sum changes. You can also change the shape of the curve to see how the whole construction changes.</oldsource>
        <translation>Bewegen Sie den Punkt &lt;b&gt;%1&lt;/b&gt; oder &lt;b&gt;%2&lt;/b&gt; entlang der Kurve um zu sehen, wie sich dabei die Summe ändert. Sie können auch mit Hilfe der Schieberegler die Gestalt der elliptischen Kurve variieren, um zu schauen, wie sich dabei die Gesamtkonstruktion ändert.</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="145"/>
        <source>&lt;h4&gt;Secant Line&lt;/h4&gt;&lt;p&gt;This line is the secant line through the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;, an auxiliary line used in the construction of the point &lt;b&gt;%1&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;Sekante&lt;/h4&gt;&lt;p&gt;Diese Gerade ist die Sekante durch die Punket &lt;b&gt;%2&lt;/b&gt; und &lt;b&gt;%3&lt;/b&gt;. Die Sekante spielt bei der Konstruktion des Punktes &lt;b&gt;%1&lt;/b&gt; eine Rolle.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="149"/>
        <source>&lt;p&gt;Apart from the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;, the secant line is guaranteed to intersect the elliptic curve in precisely one third point, which is offscreen and therefore not shown in the picture.&lt;/p&gt;&lt;p&gt;The point &lt;b&gt;%1&lt;/b&gt; is constructed as the image of the third intersection under reflection against the x-axis.&lt;/p&gt;</source>
        <oldsource>&lt;p&gt;Apart from the points  the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;, the secant line is guaranteed to intersect the elliptic curve in precisely one third point, which is offscreen and therefore not shown in the picture.&lt;/p&gt;&lt;p&gt;The point &lt;b&gt;%1&lt;/b&gt; is constructed as the image of the third intersection under reflection against the x-axis.&lt;/p&gt;</oldsource>
        <translation>&lt;p&gt;Abgesehen von den Punkten &lt;b&gt;%2&lt;/b&gt; und &lt;b&gt;%3&lt;/b&gt; schneidet die Sekante die elliptische Kurve in genau einem dritten Punkt, der allerdings außerhalb des Anzeigebereiches liegt.&lt;/p&gt;&lt;p&gt;Man konstruiert den Punkt &lt;b&gt;%1&lt;/b&gt;, indem man den dritten Schnittpunkt an der x-Achse spiegelt.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="154"/>
        <source>&lt;p&gt;Apart from the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;, the secant line is guaranteed to intersect the elliptic curve in precisely one third point&lt;/p&gt;&lt;p&gt;Move the mouse pointer to the point &lt;b&gt;%1&lt;/b&gt; to learn more about the construction.&lt;/p&gt;</source>
        <oldsource>&lt;p&gt;Apart from the points  the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;, the secant line is guaranteed to intersect the elliptic curve in precisely one third point&lt;/p&gt;&lt;p&gt;Move the mouse pointer to the point &lt;b&gt;%1&lt;/b&gt; to learn more about the construction.&lt;/p&gt;</oldsource>
        <translation>&lt;p&gt;Abgesehen von den Punkten &lt;b&gt;%2&lt;/b&gt; und &lt;b&gt;%3&lt;/b&gt; schneidet die Sekante die elliptische Kurve in genau einem dritten Punkt.&lt;/p&gt;&lt;p&gt;Bewegen Sie den Mauszeiger zum Punkt &lt;b&gt;%1&lt;/b&gt;, um mehr über die Konstruktion zu erfahren.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>gfxExportDialog</name>
    <message>
        <location filename="gfxExportDialog.ui" line="14"/>
        <source>Export Options</source>
        <translation>Export Optionen</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="26"/>
        <source>Bitmap Graphic Options</source>
        <oldsource>Bitmap Options</oldsource>
        <translation>Graphik Optionen</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="38"/>
        <source>Width</source>
        <translation>Breite</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="54"/>
        <location filename="gfxExportDialog.ui" line="92"/>
        <source> Pixel</source>
        <translation> Pixel</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="76"/>
        <source>Height</source>
        <translation>Höhe</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="105"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If checked, the background of the image becomes transparent rather than white.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If checked, the background of the image becomes transparent rather than whilte.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Falls Sie diesen Punkt wählen, wird der Hintergrund der Graphikdatei nicht weiß, sondern durchscheinend.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="112"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If checked, the background of the image becomes transparent rather than white.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This can be useful if you wish to merge the graphic into an existing picture.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If checked, the background of the image becomes transparent rather than whilte.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This can be useful if you wish to merge the graphic into an existing picture.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Falls Sie diese Option wählen, wird der Hintergrund der Graphikdatei nicht weiß, sondern durchscheinend.&lt;/p&gt;&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Dies kann nützlich sein, wenn Sie die Graphik in ein bestehendes Bild montieren möchten.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="120"/>
        <source>Transparent Background</source>
        <translation>Tranparenter Hintergrund</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="130"/>
        <source>Features Shown</source>
        <translation>Bildelemente</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="136"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Include the light gray coordinate grid when exporting to a graphics file.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Hinterlege das Bild mit einem hellgrauen Koordinatengitter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="143"/>
        <source>Show Grid</source>
        <translation>Zeige Koordinatengitter</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="153"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Include the dark coordinate axes when exporting to a graphics file.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zeichne dunkelgraue Koordinatenachsen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="156"/>
        <source>Show Axes</source>
        <translation>Zeige Koordinatenachsen</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="192"/>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="204"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>infoDialog</name>
    <message>
        <location filename="infoDialog.ui" line="14"/>
        <source>About Elliptic Curves</source>
        <translation>Über den Elliptic Curve Plotter</translation>
    </message>
    <message>
        <location filename="infoDialog.ui" line="24"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;;&quot;&gt;Sketching elliptic curves over the real numbers.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;;&quot;&gt;Sketching elliptic curves over the real numbers.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;;&quot;&gt;Ein Zeichenprogramm für relle elliptischen Kurven&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="infoDialog.ui" line="80"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:14px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Questions and Experiments&lt;/span&gt;&lt;/p&gt;
&lt;ol style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Where is the neutral group element? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Try to use the Elliptic Curve Plotter to illustrate the associativity law. &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;What is the group-theoretic meaning of the inflection points? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choose a=2 and b=-10. You will obtain an elliptic curve without oval, which is rather flat. The inflection points are therefore hard to locate visually. Using the group law, can you devise a way to use the Elliptic Curve Plotter that helps you find the inflection points precisely? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The elliptic curves are real Lie groups. Which one?&lt;/li&gt;&lt;/ol&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:14px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Questions and Experiments&lt;/span&gt;&lt;/p&gt;
&lt;ol style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Where is the neutral group element? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Try to use the Elliptic Curve Plotter to illustrate the associativity law. &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;What is the group-theoretic meaning of the inflection points? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choose a=2 and b=-10. You will obtain an elliptic curve without oval, which is rather flat. The inflection points are therefore hard to locate visually. Using the group law, can you devise a way to use the Elliptic Curve Plotter that helps you find the inflection points precisely? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The elliptic curves are real Lie groups. Which one?&lt;/li&gt;&lt;/ol&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:14px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Fragen und Vorschl&amp;auml;ge zum Experimentieren&lt;/span&gt;&lt;/p&gt;
&lt;ol style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Wo ist das neutrale Element der Gruppe?&lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Benutzen Sie den Elliptic Curve Plotter um das Assoziativit&amp;auml;tsgesetz zu illustrieren.&lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sehen Sie die die gruppentheoretische Bedeutung der Wendepunkte?&lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;W&amp;auml;hlen Sie a=2 und b=-10. Sie erhalten eine elliptische Kurve ohne Oval, die sehr flach ist. Die Wendepunkte sind demzufolge schwer zu sehen. Benutzen Sie das Gruppengesetz um einen Weg zu finden, wie man die Wendepunkte pr&amp;auml;zise finden kann?&lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Alle elliptischen Kurven sind reelle Lie-Gruppen. Welche?&lt;/li&gt;&lt;/ol&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="infoDialog.ui" line="100"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Report Problems or Suggestions&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you find a bug in the Elliptic Curve Plotter, or if you have a suggestion for improvement, please contact the author at &lt;span style=&quot; font-weight:600;&quot;&gt;stefan.kebekus@math.uni-freiburg.de&lt;/span&gt;. We appreciate your input!&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you are reporting a bug, please include details about your setup:&lt;/p&gt;
&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;What version of the Elliptic Curve Plotter are you using? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;What platform are you using? Linux? MacOS? Windows? What distribution and what version? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Where did you get the program from?&lt;/li&gt;&lt;/ul&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Also, please remember that it is almost impossible to fix a bug that we cannot reproduce, so please include all information necessary to reproduce the bug. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Report Problems or Suggestions&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you find a bug in the Elliptic Curve Plotter, or if you have a suggestion for improvement, please contact the author at &lt;span style=&quot; font-weight:600;&quot;&gt;stefan.kebekus@math.uni-freiburg.de&lt;/span&gt;. We appreciate your input!&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you are reporting a bug, please include details about your setup:&lt;/p&gt;
&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;What version of the Elliptic Curve Plotter are you using? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;What platform are you using? Linux? MacOS? Windows? What distribution and what version? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Where did you get the program from?&lt;/li&gt;&lt;/ul&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Also, please remember that it is almost impossible to fix a bug that we cannot reproduce, so please include all information necessary to reproduce the bug. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Probleme und W&amp;uuml;nsche&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Wenn Sie im Elliptic Curve Plotter einen Fehler finden oder wenn Sie einen Verbesserungsvorschlag haben, schreiben Sie uns bitte unter &lt;span style=&quot;font-weight: bold;&quot;&gt;stefan.kebekus@math.uni-freiburg.de&lt;/span&gt;. Wir freuen uns, von Ihnen zu h&amp;ouml;ren!&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Falls Sie wegen eines Fehlers schreiben, f&amp;uuml;gen Sie Ihrem Fehlerbericht die folgenden Details an:&lt;/p&gt;
&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Welche Version des Elliptic Curve Plotter benutzen Sie?&lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Welches Betriebssystem benutzen Sie? Linux? MacOS? Windows? Welche Version?&lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Woher haben Sie das Programm erhalten?&lt;/li&gt;&lt;/ul&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Bitte denken Sie auch daran, dass es f&amp;uuml;r uns fast unm&amp;ouml;glich ist, Fehler zu beheben, die wir nicht reproduzieren k&amp;ouml;nnen!&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="infoDialog.ui" line="121"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:14px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Help to improve the Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Come up with new ideas&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you have suggestion how the Elliptic Curve Plotter could be improved, let us know!&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Help to implement new features&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you have programming skills, you can help to implement new features or streamline the existing program. The Elliptic Curve Plotter is written in the standard C++ programming language and uses the excellent and very well-documented Qt library for its user interface.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Translate the Elliptic Curve Plotter into new languages&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you know a language that the Elliptic Curve Plotter does not yet support, join us to add language support. Programming skills are not required.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:14px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Help to improve the Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Come up with new ideas&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you have suggestion how the Elliptic Curve Plotter could be improved, let us know!&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Help to implement new features&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you have programming skills, you can help to implement new features or streamline the existing program. The Elliptic Curve Plotter is written in the standard C++ programming language and uses the excellent and very well-documented Qt library for its user interface.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Translate the Elliptic Curve Plotter into new languages&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you know a language that the Elliptic Curve Plotter does not yet support, join us to add language support. Programming skills are not required.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:14px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Helfen Sie uns, den Elliptic Curve Plotter zu verbessern&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Haben Sie neue Ideen?&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Bitte kontaktieren Sie uns, wenn Sie Vorschl&amp;auml;ge haben, wie der Elliptic Curve Plotter verbessert werden kann!&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Helfen Sie uns, neue Funktionalit&amp;auml;t zu programmieren!&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Wenn Sie programmieren k&amp;ouml;nnen, k&amp;ouml;nnen Sie uns helfen, indem Sie neue Funktionalit&amp;auml;t hinzuf&amp;uuml;gen oder das existierende Programm verbessern. Der Elliptic Curve Plotter ist in der Programmiersprache C++ geschrieben und verwendet die (sehr gut dokumentierte) Qt Bibliothek.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;&amp;Uuml;bersetzen Sie den Elliptic Curve Plotter in andere Sprachen!&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Wenn Sie eine Sprache gut beherrschen, die vom Elliptic Curve Plotter noch nicht unterst&amp;uuml;tzt wird, helfen Sie uns, die Sprache hinzuzuf&amp;uuml;gen. Programmierkenntnisse sind dabei nicht erforderlich.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>mainWindow</name>
    <message>
        <location filename="mainWindow.ui" line="22"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This field shows a sketch of the curve defined over the real numbers by the equation &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Dieses Feld zeigt eine Skizze der Kurve, die durch die Gleichung &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; gegeben ist.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="45"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Curve Equation: &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;%1%2&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Kurvengleichung: &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;%1%2&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="36"/>
        <source>Elliptic Curve Graph</source>
        <translation>Zeichnung</translation>
    </message>
    <message>
        <source>Elliptic Curve Plotter</source>
        <translation type="obsolete">Elliptic Curve Plotter</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="227"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="231"/>
        <source>Export Drawing</source>
        <translation>Zeichnung exportieren</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="245"/>
        <source>&amp;Curves</source>
        <translation>&amp;Kurven</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="256"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="269"/>
        <source>Group Law</source>
        <translation>Gruppengesetz</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="282"/>
        <source>View</source>
        <translation>Ansicht</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="305"/>
        <source>toolBar</source>
        <translation>Werkzeugleiste</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="85"/>
        <source>Equation Parameters</source>
        <translation>Koeffizienten der Kurvengleichung</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="198"/>
        <source>Curve Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="286"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="337"/>
        <source>&amp;Quit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="340"/>
        <source>Exits the application.</source>
        <translation>Beendet den Elliptic Curve Plotter.</translation>
    </message>
    <message>
        <source>Ctrl+Q</source>
        <translation type="obsolete">Ctrl+Q</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="351"/>
        <source>Elliptic curve with oval</source>
        <translation>Elliptische Kurve mit Oval</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="354"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choses values for &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; and &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; such that the curve defined by the equation &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; is an elliptic curve that consists of two disjoint components.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Wählt voreingestellte Werte für &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; und &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt;, so dass die Kurve, die durch die Gleichung &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; definiert ist, eine elliptische Kurve ist, die aus zwei unterschiedlichen Komponente besteht.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="363"/>
        <source>Elliptic curve without oval</source>
        <translation>Elliptische Kurve ohne Oval</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="366"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choses values for &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; and &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; such that the curve defined by the equation &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; is an elliptic curve that consists of one component only.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Wählt voreingestellte Werte für &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; und &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt;, so dass die Kurve, die durch die Gleichung &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; definiert ist, eine elliptische Kurve ist, die nur aus einer Komponente besteht.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="375"/>
        <source>Neil Parabola</source>
        <translation>Neilsche Parabel</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="378"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choses values for &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; and &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; such that the curve defined by the equation &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; is a semicubical parabola.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Wählt voreingestellte Werte für &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; and &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt;, so dass die Kurve, die durch die Gleichung &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; gegeben ist, eine Neilsche Parabel wird.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="387"/>
        <source>Nodal Plane Cubic</source>
        <translation>Ebene Knotenkurve</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="390"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choses values for &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; and &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; such that the curve defined by the equation &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; is a nodel plane cubic.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Wählt voreingestellte Werte für &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; and &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt;, so dass die Kurve, die durch die Gleichung &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; gegeben ist, eine ebene Knotenkurve wird.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="399"/>
        <source>Curve with Isolated Point</source>
        <translation>Kurve mit isoliertem Punkt</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="402"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choses values for &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; and &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; such that the curve defined by the equation &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; is a singular elliptic curve that consists of a parabola and an isolated point.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Wählt voreingestellte Werte für &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; und &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt;, so dass die Kurve, die durch die Gleichung &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; definiert ist, eine elliptische Kurve ist, die aus einer &quot;Parabel&quot; und einem isolierten Punkt besteht.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="343"/>
        <source>Exits the Elliptic Curve Plotter</source>
        <translation>Beendet den Elliptic Curve Plotter</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="414"/>
        <source>About the Elliptic Curve Plotter ...</source>
        <translation>Über den Elliptic Curve Plotter ...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="417"/>
        <source>Shows information about the Elliptic Curve Plotter and its license.</source>
        <oldsource>Shows information about the Elliptic Curve Plotter and its license.</oldsource>
        <translation>Zeigt Informationen über den Elliptic Curve Plotter.</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="429"/>
        <source>What&apos;s This?</source>
        <translation>Was ist das?</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="432"/>
        <source>What&apos;s This? Click on this item and then on any field to obtain a detailed explanation.</source>
        <translation>Was ist das? Wählen Sie diesen Eintrag und klicken Sie dann mit der Maus ein ein beliebiges Feld, um eine Beschreibung des Feldes zu erhalten.</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="435"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/whatsthis.png&quot; /&gt;Click on this item and then on any other field or menu item to obtain a more detailed description of the field.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/whatsthis.png&quot; /&gt;Wählen Sie diesen Punkt und klicken Sie dann auf irgendein Feld, Knopf oder anderes Objekt, um mehr über eine detaillierte Beschreibung des Obektes zu erhalten.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Shift+F1</source>
        <translation type="obsolete">Umschalt+F1</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="453"/>
        <source>Compute 2P</source>
        <translation>Berechne 2P</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="459"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this action to compute the sum of the point &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; with itself according to the group law of the elliptic curve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Verdoppelt den Punkt &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; gemäß dem Gruppengesetz der elliptischen Kurve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="473"/>
        <source>Compute P+Q</source>
        <translation>Berechne P+Q</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="479"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this action to compute the sum of the points &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; and &lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt; according to the group law of the elliptic curve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Berechnet die Summe der Punkte &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; und &lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt; gemäß dem Gruppengesetz der elliptischen Kurve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="490"/>
        <source>Compute -P</source>
        <translation>Berechne -P</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="496"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this action to compute the inverse of the point &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; according to the group law of the elliptic curve&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Berechnet das Inverse des Punktes &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; gemäß dem Gruppengesetz der elliptischen Kurve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="510"/>
        <source>Compute Q+R</source>
        <translation>Berechne Q+R</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="516"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this action to compute the sum of the points &lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt; and &lt;span style=&quot; font-weight:600;&quot;&gt;R &lt;/span&gt;according to the group law of the elliptic curve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Berechnet die Summe der Punkte &lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt; und &lt;span style=&quot; font-weight:600;&quot;&gt;R&lt;/span&gt; gemäß dem Gruppengesetz der elliptischen Kurve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="527"/>
        <source>Compute P+(Q+R)</source>
        <translation>Berechne P+(Q+R)</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="533"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this action to compute the sum of the points &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; and &lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt;+&lt;span style=&quot; font-weight:600;&quot;&gt;R&lt;/span&gt; according to the group law of the elliptic curve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Berechnet die Summe der Punkte &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; und &lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt;+&lt;span style=&quot; font-weight:600;&quot;&gt;R&lt;/span&gt; gemäß dem Gruppengesetz der elliptischen Kurve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="544"/>
        <location filename="mainWindow.ui" line="550"/>
        <source>Compute (P+Q)+R</source>
        <translation>Berechne (P+Q)+R</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="553"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this action to compute the sum of the points &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt;+&lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt; and &lt;span style=&quot; font-weight:600;&quot;&gt;R&lt;/span&gt; according to the group law of the elliptic curve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Berechnet die Summe der Punkte &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt;+&lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt; und &lt;span style=&quot; font-weight:600;&quot;&gt;R&lt;/span&gt; gemäß dem Gruppengesetz der elliptischen Kurve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="562"/>
        <source>About Qt ...</source>
        <translation>Über die Qt Bibliothek...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="565"/>
        <source>Shows information about the Qt library, a software product that is used internally by the Elliptic Curve Plotter.</source>
        <oldsource>Shows information about the Qt library, a software product that is used internally by the Elliptic Curve Plotter.</oldsource>
        <translation>Zeigt Informationen über die Qt Bibliothek, ein Softwareprodukt, das intern vom Elliptic Curve Plotter verwendet wird.</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="576"/>
        <source>Export to Vector Graphics File...</source>
        <translation>Zeichnung als Vektorgraphik exportieren...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="579"/>
        <location filename="mainWindow.ui" line="586"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Saves the elliptic curve graph as a Scalable Vector Graphics (SVG) file. SVG files can be edited with specialized programs (such as &lt;span style=&quot; font-weight:600;&quot;&gt;karbon&lt;/span&gt; or &lt;span style=&quot; font-weight:600;&quot;&gt;inkscape&lt;/span&gt;), and are suitable for high quality printouts.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Speichert die Skizze der elliptischen Kurve als Vektorgraphik (SVG) Datei. Die Datei kann mit Programmen wie &lt;span style=&quot; font-weight:600;&quot;&gt;karbon&lt;/span&gt; oder &lt;span style=&quot; font-weight:600;&quot;&gt;inkscape&lt;/span&gt; nachbearbeitet werden und eignet sich für Ausdrucke in hoher Qualität.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="595"/>
        <source>&amp;Print...</source>
        <translation>&amp;Drucken...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="600"/>
        <source>&amp;Open...</source>
        <translation>&amp;Öffnen...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="605"/>
        <source>Save &amp;As...</source>
        <translation>&amp;Speichern als...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="610"/>
        <source>Report Problem or Suggestion...</source>
        <translation>Probleme berichten oder Vorschläge machen...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="613"/>
        <location filename="mainWindow.ui" line="616"/>
        <source>If you find a bug in the Elliptic Curve Plotter, or if you have a suggestion for future improvements, use this menu entry to contact the author.</source>
        <oldsource>If you find a bug in the Elliptic Curve Plotter, or if you have a suggestion for future improvements, use this menu entry to contact the author.</oldsource>
        <translation>Wenn Sie im Elliptic Curve Plotter einen Fehler finden oder wenn Sie einen Verbesserungsvorschlag haben, können Sie mit diesem Menüpunkt den Autor benachrichtigen.</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="621"/>
        <location filename="mainWindow.ui" line="648"/>
        <source>How to help...</source>
        <translation>Helfen Sie, das Programm zu verbessern...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="629"/>
        <source>Export to Raster Graphics File...</source>
        <translation>Zeichnung als Rastergraphik exportieren...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="632"/>
        <location filename="mainWindow.ui" line="635"/>
        <source>Saves the elliptic curve graph as a raster graphics file in Portable Network Graphics (PNG) format. PNG files can easily be included in web pages, but have limited printout quality.</source>
        <translation>Speichert die Skizze der elliptischen Kurve als Rastergraphik im PNG-Format. Die Graphikdatei kann leicht in Webseiten eingebunden werden, liefert aber nur beschränkte Qualität beim Ausdrucken.</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="640"/>
        <source>Questions and Experiments...</source>
        <translation>Fragen und Vorschläge zum Experimentieren...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="643"/>
        <source>Questions and Suggestions for Experiments</source>
        <translation>Fragen und Vorschläge zum Experimentieren</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="651"/>
        <source>Use this action to learn how to help improving the Elliptic Curve Plotter</source>
        <oldsource>Use this action to learn how to help improving the Elliptic Curve Plotter</oldsource>
        <translation>Erläutert, wie Sie helfen können, den Elliptic Curve Plotter zu verbessern</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="659"/>
        <source>Kiosk Mode...</source>
        <translation>Ausstellungs-Modus...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="667"/>
        <source>Reset User Interface</source>
        <translation>Benutzeroberfläche zurücksetzen</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="675"/>
        <source>Use System Default Language</source>
        <translation>Systemsprache verwenden</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="678"/>
        <location filename="mainWindow.ui" line="681"/>
        <source>Use the system default language if translations exist. Otherwise, English is used.</source>
        <translation>Verwende die Systemsprache, falls Übersetzungen für diese Sprache existieren. Ansonsten wird Englisch verwendet.</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="254"/>
        <source>English</source>
        <comment>Translate this to the name of YOUR language, in your language (e.g. &apos;Deutsch&apos;, &apos;Italiano&apos;, &apos;Español&apos;)</comment>
        <translation>Deutsch</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="390"/>
        <source>Curve Equation: %1</source>
        <translation>Kurvengleichung %1</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="446"/>
        <source>&lt;p&gt;The file name &lt;b&gt;%1&lt;/b&gt; does not end in &lt;b&gt;%2&lt;/b&gt;. This might make it difficult for the file manager and other programs to recognize the file type.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Der Dateiname &lt;b&gt;%1&lt;/b&gt; endet nicht in &lt;b&gt;%2&lt;/b&gt;. Das macht es für Ihren Dateimanager und viele andere Programme schwierig, die Art der Datei korrekt zu erkennen.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="449"/>
        <source>&lt;p&gt;Would you like to add the ending to the file name &lt;b&gt;and overwrite the existing file %1&lt;/b&gt;?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Möchten Sie die dem Dateinamen die korrekte Endung anfügen &lt;b&gt;und die existierende Datei %1 überschreiben&lt;/b&gt;?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="451"/>
        <source>&lt;p&gt;Would you like to add the ending to the file name?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Möchten Sie die dem Dateinamen die korrekte Endung anfügen?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="479"/>
        <source>Scalable Vector Graphics File (*.svg)</source>
        <translation>Scalable Vector Graphics File (*.svg)</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="491"/>
        <source>Elliptic Curve</source>
        <translation>Elliptische Kurve</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="503"/>
        <location filename="mainWindow.cpp" line="535"/>
        <source>File Error</source>
        <translation>Dateifehler</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="504"/>
        <location filename="mainWindow.cpp" line="536"/>
        <source>&lt;p&gt;The file %1 could not be written.&lt;/p&gt;&lt;p&gt;The system reported the following error: %2&lt;/p&gt;</source>
        <translation>&lt;p&gt;Die Datei %1 konnte nicht geschrieben werden.&lt;/p&gt;&lt;p&gt;Die folgende Fehlermeldung wurde vom Betriebssystem zurückgemeldet: %2&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="515"/>
        <source>Portable Network Graphics (*.png)</source>
        <translation>Portable Network Graphics (*.png)</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="546"/>
        <source>Report Problem or Suggestion</source>
        <translation>Probleme berichten oder Vorschläge machen</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="558"/>
        <source>How to Help</source>
        <translation>Helfen Sie, das Programm zu verbessern</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="570"/>
        <source>Questions and Experiments</source>
        <translation>Fragen und Vorschläge zum Experimentieren</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="620"/>
        <location filename="mainWindow.cpp" line="635"/>
        <source>Resetting User Interface</source>
        <translation>Benutzeroberfläche zurücksetzen</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="636"/>
        <source>Several Minutes without user interaction.</source>
        <translation>Mehrere Minuten ohne Benutzereingabe.</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="719"/>
        <source>Do you really wish to enter the Kiosk Mode?</source>
        <translation>Möchten Sie wirklich in den Ausstellungs-Modus wechseln?</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="719"/>
        <source>Elliptic Curve Plotter - Kiosk Mode</source>
        <translation>Elliptic Curve Plotter - Ausstellungs-Modus</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="722"/>
        <source>&lt;p&gt;The Kiosk Mode is a restricted mode useful when running the Elliptic Curve Plotter on interactive kiosks that are open to the public.&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Several menu items are disabled, so users cannot exit the program or access the file system.&lt;/li&gt;&lt;li&gt;The program switches to full-screen mode.&lt;/li&gt;&lt;li&gt;The user interface will be reset after three minutes of inactivity.&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;&lt;b&gt;There is no way to leave the Kiosk Mode other than exiting the program.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Hint:&lt;/b&gt; To start the Elliptic Curve Plotter in Kiosk Mode without showing this warning, start the Elliptic Curve Plotter from the command line using the command line option &lt;b&gt;-kiosk&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Der Ausstellungs-Modus ist ein eingeschränkter Modus, der nützlich ist, wenn das Programm auf öffentlich zugänglichen Terminals läuft.&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Einige Menüpunkte sind in diesem Modus deaktiviert, so dass die Benutzer und nicht auf das Dateisystem zugreifen können.&lt;/li&gt;&lt;li&gt;Das Programm wechselt in den Vollbild-Modus.&lt;/li&gt;&lt;li&gt;Nach drei Minuten ohne Benutzereingabe wird die Benutzeroberfläche automatisch zurückgesetzt.&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;&lt;b&gt;Es gibt keine Möglichkeit, den Ausstellungs-Modus zu verlassen ohne das Programm zu beenden.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Hinweis:&lt;/b&gt; Um den Elliptic Curve Plotter direkt im Ausstellungs-Modus zu starten, rufen Sie das Programm von der Kommandozeile auf und verwenden Sie die Option &lt;b&gt;-kiosk&lt;/b&gt;.&lt;/p&gt;</translation>
    </message>
</context>
</TS>
