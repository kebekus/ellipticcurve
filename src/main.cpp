/***************************************************************************
 *   Copyright (C) 2018-2024 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QDebug>
#include <QLibraryInfo>
#include <QLocale>
#include <QTextStream>
#include <QTranslator>

// Include Windows Integration Plugin into statically linked windows binaries
#ifdef _WIN32
#include <QtPlugin>
Q_IMPORT_PLUGIN(QWindowsIntegrationPlugin)
#endif

#include "mainWindow.h"

using namespace Qt;


int main(int argc, char *argv[])
{
  QApplication const app(argc, argv);

  QCoreApplication::setOrganizationName(u"Albert-Ludwigs-Universitaet Freiburg"_s);
  QCoreApplication::setOrganizationDomain(u"uni-freiburg.de"_s);
  QCoreApplication::setApplicationName(u"Elliptic Curve Plotter"_s);
#if (QT_VERSION >= QT_VERSION_CHECK(5, 7, 0)) && defined(Q_OS_LINUX)
  //  QCoreApplication::setDesktopFileName("de.unifreiburg.ellipticcurve");
  QGuiApplication::setDesktopFileName(u"de.unifreiburg.ellipticcurve"_s);
#endif
  
  
  QStringList args = QApplication::arguments();

  // Check if we are running in "kiosk" mode
  bool kioskMode = false;

  // Evaluate command line arguments unless we are under android
#ifndef ANDROID
  auto const index = args.indexOf("-kiosk", 1);
  if (index >= 0) {
    kioskMode = true;
    args.takeAt(index);
  }

  // This application does not take any other arguments. So, if any non-qt
  // argument was given, print a usage message in the system language and exit.
  QString const prog = args.takeFirst();
  if (!args.isEmpty()) {
    // Install translators, so that the message appears in the appropriate
    // language
    QString const locale = QLocale::system().name();
    QTranslator translator;
    (void) translator.load(u":ellipticcurve_"_s + locale.left(2));
    QApplication::installTranslator(&translator);
    QTranslator Qt_translator;
    (void) Qt_translator.load("qt_" + locale.left(2), QLibraryInfo::path(QLibraryInfo::TranslationsPath));
    QApplication::installTranslator(&Qt_translator);
    
    QTextStream out(stdout);
    out << "\n"
    << "Elliptic Curve Plotter" << "\n"
    << QObject::tr("Sketching elliptic curves over the real numbers") << "\n"
    << "\n"
    << "Version " << PROJECT_VERSION << "\n"
    << "Copyright (C) " << COPYRIGHTYEAR << " Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>" << "\n"
    << "\n"
	<< QObject::tr("This program comes with ABSOLUTELY NO WARRANTY; for details\n"
		       "use the menu item 'About the Elliptic Curve Plotter' from\n"
		       "the 'Help' menu. This is free software, and you are welcome\n"
		       "to redistribute it under certain conditions; use the menu\n"
		       "item 'About the Elliptic Curve Plotter' from the 'Help'\n"
		       "menu for details.\n"
		       "\n"
		       "Usage: %1 [Options]\n"
		       "\n"
		       "-help  Show this help text.\n"
		       "\n"
		       "-kiosk Run the in Kiosk Mode, a restricted full-screen mode\n"
		       "       useful for running the program on terminals or kiosks\n"
		       "       that are open to the public.\n"
		       "\n").arg(prog)
        << "\n";
    out.flush();
    exit(0);
  }
#endif

  // Now start the GUI
  mainWindow dlg(kioskMode);

  dlg.show();
  return QApplication::exec();
} 
