/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QDebug>
#include <QGraphicsPathItem>
#include <QResizeEvent>

#include "ecGraphicsScene.h"
#include "ecPlotter.h"
#include "ec_real.h"
#include "ec_realFreePoint.h"

using namespace std::chrono_literals;
using namespace Qt;

ecPlotter::ecPlotter(QWidget *parent)
    : QGraphicsView(parent)
    , scene(new ecGraphicsScene(this))
{
    setMouseTracking(true);

    timer.setSingleShot(true);
    timer.setInterval(200ms);
    connect(&timer, &QTimer::timeout, this, &ecPlotter::updateGUI);

    setScene(scene);
}

void ecPlotter::drawGrid(const QRectF &boundingBox)
{
    bBox = boundingBox;
    scene->setBackgroundBrush(Qt::white);

    //
    // Create coordinate grid
    //
    QPainterPath coordGrid;
    QPen pen(Qt::lightGray);
    pen.setWidthF(0.01);
    qreal x = bBox.left()+1.0;
    while( x <= bBox.right()-1.0 ) {
        coordGrid.moveTo(x, bBox.bottom());
        coordGrid.lineTo(x, bBox.top());
        x += 1.0;
    }
    qreal y = bBox.top()+1;
    while( y <= bBox.bottom()-1.0 ) {
        coordGrid.moveTo(bBox.left(), y);
        coordGrid.lineTo(bBox.right(), y);
        y += 1.0;
    }
    grid = scene->addPath(coordGrid, pen);
    grid->setZValue(-10);

    //
    // Create coordinate axis
    //
    QPainterPath axis;
    QPen pen2(Qt::darkGray);
    pen2.setWidthF(0.05);
    axis.moveTo(bBox.left(),       0);
    axis.lineTo(bBox.right(),       0);
    axis.moveTo(      0, bBox.top());
    axis.lineTo(      0, bBox.bottom());
    axis.moveTo(bBox.right()-0.25, -0.2 );
    axis.lineTo(bBox.right()     ,  0   );
    axis.lineTo(bBox.right()-0.25,  0.2 );
    axis.moveTo(-0.2, bBox.top()+0.25 );
    axis.lineTo(0    , bBox.top()      );
    axis.lineTo( 0.2, bBox.top()+0.25 );
    axes =  scene->addPath(axis, pen2);
    axes->setZValue(-9);
    labelX = scene->addSimpleText( u"x"_s );
    labelX->setScale(0.04);
    labelX->setPos( bBox.right(), 0 );
    labelX->setZValue(-9);
    labelY = scene->addSimpleText( u"y"_s );
    labelY->setScale(0.04);
    labelY->setPos( 0.4, bBox.top()-0.4 );
    labelY->setZValue(-9);

    //
    // Add point box
    //
    rect =  scene->addRect( 7, -10, 2.5, 4.2, QPen(Qt::darkGray, 0.02), QBrush(QColor(250,250,240)) );
    rect->setZValue(-9);
    label = scene->addSimpleText(QString());
    label->setPos( 0, bBox.top()+0.2 );
    pen2.setWidthF(0.02);
    label->setPen( pen2 );
    label->setScale(0.03);
    label->setZValue(-9);

    scene->setSceneRect( bBox.adjusted(-2, -0.5, 4, 0.5 ) );

    updateGUI();
}


void ecPlotter::setCurve(ec_real *_curve)
{
    // Paranoid safety checks.
    if (_curve == nullptr) {
        qCritical() << "ecPlotter::setCurve() called with curve=0";
    }
    if (!curve.isNull()) {
        qCritical() << "ecPlotter::setCurve() called when curve was already set";
    }

    curve = _curve;

    // Add items to the scene. Note: the items are now owned by the scene and
    // will be deleted when the scene gets deleted.
    scene->addItem(curve);

    connect(curve, SIGNAL(changed()), &timer, SLOT(start()));
}


void ecPlotter::addFreePoint(ec_realFreePoint *pt)
{
    // This also does the paranoid safety checks
    addPoint(pt);

    freePoints.append(pt);
}


void ecPlotter::addPoint(ec_realPoint *pt)
{
    // Paranoid safety checks.
    if (curve == nullptr) {
        qCritical() << "ecPlotter::setCurve() called with curve=0";
    }
    if (curve.isNull()) {
        qCritical() << "ecPlotter::setFreePoint() called when no curve was set";
    }

    // Add the point to the scene. Note: the point is now owned by the scene and
    // will be deleted when the scene gets deleted.
    scene->addItem(pt);
    connect(pt, SIGNAL(changed()), &timer, SLOT(start()));

    pt->setZValue(3);
}


void ecPlotter::paint(QPainter &painter, bool showAxes, bool showGrid, bool transparent)
{
    grid->setVisible(showGrid);
    axes->setVisible(showAxes);
    labelX->setVisible(showAxes);
    labelY->setVisible(showAxes);
    if (transparent) {
        scene->setBackgroundBrush(Qt::NoBrush);
    }

    scene->render(&painter, QRectF(), augmentedBox() );

    grid->show();
    axes->show();
    labelX->show();
    labelY->show();
    scene->setBackgroundBrush(Qt::white);
}


void ecPlotter::resizeEvent(QResizeEvent *event)
{
    fitInView( sceneRect(), Qt::KeepAspectRatio);
    event->accept();
}


void ecPlotter::updateGUI()
{
    label->setText( tr("Point Box") );
    label->setPos( 7.0 + (1.25 - label->boundingRect().width()*0.015), label->y());
    rect->setToolTip( tr("<h4>Point Box</h4><p>Drag the points from this box to the curve and vice versa.</p>") );

    if (curve.isNull()) {
        setStatusTip(QString());
        return;
    }
    if (!curve->isSmooth()) {
        setStatusTip(QString());
        return;
    }

    bool haveInvalidFreePoints = false;
    bool haveValidFreePoints = false;
    for (const auto & freePoint : std::as_const(freePoints)) {
        ec_realFreePoint *pt = freePoint;
        if (pt == nullptr) {
            continue;
        }

        if (pt->getState() == ec_realPoint::affine) {
            haveValidFreePoints = true;
        } else {
            haveInvalidFreePoints = true;
        }
    }

    if (haveInvalidFreePoints) {
        if (haveValidFreePoints) {
            setStatusTip(tr("Drag the points to the curve, and use the 'Group Law' menu to experiment with the group structure."));
        } else {
            setStatusTip(tr("Drag the points to the curve."));
        }
    } else {
        setStatusTip(tr("Drag the points along the curve, and use the 'Group Law' menu to "
                        "experiment with the group structure."));
    }
}
