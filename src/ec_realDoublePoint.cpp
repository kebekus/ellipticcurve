/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QBrush>
#include <QDebug>
#include <QGraphicsLineItem>
#include <QPen>

#include "ec_real.h"
#include "ec_realDoublePoint.h"

ec_realDoublePoint::ec_realDoublePoint(const QRectF &boundingBox,
                                       ec_real *_parentCurve,
                                       ec_realPoint *_parentPoint,
                                       QGraphicsItem *parentItem)
    : ec_realPoint(boundingBox, _parentCurve, parentItem, "2" + _parentPoint->getName(), true, true)
{
  // Paranoid safety checks
  if (_parentCurve == nullptr) {
      qCritical() << "ec_realDoublePoint::ec_realDoublePoint called with parentCurve == 0";
  }
  if (_parentPoint == nullptr) {
      qCritical() << "ec_realDoublePoint::ec_realDoublePoint called with parentPoint == 0";
  }

  compute      = false;

  parentPoint = _parentPoint;
  connect(parentPoint, SIGNAL(changed()), this, SLOT(updateGraphics()));


  circle->setBrush(QBrush(Qt::yellow));
}


QString ec_realDoublePoint::description() const
{
  // Paranoid safety checks
  if (parentCurve.isNull()) {
    qWarning() << "ec_realMinusPoint::description() called with parentCurve == 0";
    return {};
  }

  QString descr;
  switch(getState()) {
  case infinity:
    descr = tr("The point <b>%1</b> is the double of the point <b>%2</b> according to the group law of the elliptic curve. "
	       "Geometrically, the point <b>%1</b> is constructed by taking the tangent line through <b>%2</b>. "
           "This tangent line intersects the elliptic curve in the point <b>%2</b>, and in the point at infinity.").arg(getName(), parentPoint->getName());
    break;
  case affine:
    descr = tr("The point <b>%1</b> is the double of the point <b>%2</b> according to the group law of the elliptic curve. "
	       "Geometrically, the point <b>%1</b> is constructed by taking the tangent line through <b>%2</b>. "
	       "This tangent line will intersect the elliptic curve in the point <b>%2</b>, and in exactly one second point. "
           "The double of <b>%2</b> is then constructed as the image of the second point under reflection against the x-axis.").arg(getName(), parentPoint->getName());
    break;
  default:
    break;
  }
  descr += " " + ec_realPoint::description();
  return descr;
}


bool ec_realDoublePoint::isComputable() const
{
    if (parentPoint.isNull()) {
        return false;
    }

  return (parentPoint->getState() != invalid);
}


void ec_realDoublePoint::setCompute(bool doCompute)
{
    if (doCompute == compute) {
        return;
    }

  compute = doCompute;
  updateGraphics();
}


void ec_realDoublePoint::setToolTip()
{
    if (parentPoint.isNull()) {
        return;
    }

  QString descr;
  descr = tr("<h4>Auxiliary Line</h4>"
	     "<p>The point <b>%1</b> is constructed as the image of the small yellow point under reflection against the x-axis. "
	     "This auxiliary line connects the point <b>%1</b> and the small yellow point.</p>"
	     "<p>Move the mouse pointer to the point <b>%1</b> to learn more about the construction.</p>").arg(getName());
  vertLine->setToolTip(descr);
  
  descr = tr("<h4>Auxiliary Point</h4>"
	     "<p>This is an auxiliary point used in the construction of the double of the point <b>%1</b>. "
	     "The double of <b>%1</b> is constructed by looking at the tangent line through <b>%1</b>. "
	     "The tangent line is guaranteed to intersect the elliptic curve in the point <b>%1</b> and "
	     "in precisely one second point, the point you are looking at right now.</p>"
	     "<p>To learn more about the construction, move the mouse pointer to the tangent line or to "
         "the point <b>%2</b>.</p>").arg(parentPoint->getName(), getName());
  smallCircle->setToolTip(descr);

  QString tt;
  tt += tr("<h4>Double of the point <b>%1</b></h4>").arg(parentPoint->getName());
  tt += "<p>" + description() + "</p>";
  tt += tr("<p>Move the point <b>%1</b> to see how the double of <b>%1</b> changes along with <b>%1</b>. "
	   "You can also use the sliders to change the shape of the elliptic curve, and to see how the whole "
	   "construction changes.</p>").arg(parentPoint->getName());
  circle->setToolTip(tt); 
  
  tt = tr("<h4>Tangent Line</h4>"
	  "<p>This line is the tangent line through <b>%2</b>, an auxiliary line used in the construction "
      "of the point <b>%1</b>.</p>").arg(getName(), parentPoint->getName());
  if (isOffscreen()) {
      tt += tr("<p>The tangent line is the unique line which contains <b>%1</b> and just touches "
               "the elliptic"
               "curve, without intersecting it transversely. The tangent line is guaranteed to "
               "intersect the "
               "elliptic curve in the point <b>%1</b> and in precisely one second point, which is "
               "offscreen "
               "and therefore not shown in the picture.</p><p>The point <b>%2</b> is constructed "
               "as the "
               "image of the second intersection under reflection against the x-axis.</p>")
                .arg(parentPoint->getName(), getName());
  } else {
      tt += tr("<p>The tangent line is the unique line which contains <b>%1</b> and just touches "
               "the elliptic"
               "curve, without intersecting it transversely. The tangent line is guaranteed to "
               "intersect the "
               "elliptic curve in the point <b>%1</b> and in precisely one second point.</p>"
               "<p>Move the mouse pointer to the point <b>%2</b> to learn more about the "
               "construction.</p>")
                .arg(parentPoint->getName(), getName());
  }
  secantLine->setToolTip(tt);
}


void ec_realDoublePoint::updateGraphics()
{
  // Paranoid safety checks
  if (parentCurve.isNull()) {
      qCritical() << "ec_realDoublePoint::pointChanged called with parentCurve == 0";
  }
  if (parentPoint.isNull()) {
      qCritical() << "ec_realDoublePoint::pointChanged called with parentPoint == 0";
  }

  emit computabilityChanged();
  if ((!compute) || !isComputable()) {
    setInvalid();
    return;
  }

  // If the parent point is infinity, the double is infinity
  if (parentPoint->getState() == infinity) {
    setInfinity();
    return;
  }

  secantLine->setLine(computeLineSegment(parentPoint->getCoords(), parentCurve->tangent(parentPoint->getCoords())));

  if (qAbs(parentPoint->getCoords().y()) < 0.01) {
    setInfinity();
  } else {
      qreal const lambda = (3 * parentPoint->getCoords().x() * parentPoint->getCoords().x()
                            + parentCurve->a())
                           / (2 * parentPoint->getCoords().y());
      qreal const mu = parentPoint->getCoords().y() - lambda * parentPoint->getCoords().x();
      qreal const x = lambda * lambda - 2 * parentPoint->getCoords().x();
      QPointF const nc(x, -lambda * x - mu);

      setCoords(nc);
  }

  // Experimentation shows that if you add a child to a hidden
  // QGraphicsItem, then the child will be shown, somewhat
  // counter-intuitively. In order to properly hide the child, we briefly
  // show *this, and then hide it again, thereby also hiding the child.
  if (!isVisible()) {
    show();
    hide();
  }
}
