/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef EC_REAL
#define EC_REAL 1


#include "ec.h"

#include <cmath>
#include <QGraphicsItem>
#include <QPainterPath>
#include <QPointF>


/**
 * Graphical representation of a short Weierstrass equation for an
 * elliptic curve, defined over the real numbers.
 *
 * This class give a graphical representation of an elliptic curve (singular
 * or smooth), defined over the real numbers. The elliptic curve is given as a
 * short Weierstrass equation, i.e., as an equation of the form y^2 = x^3 +
 * a*x + b. This class contains many convenience functions.
 * 
 * @author Stefan Kebekus
 */


class ec_real : public QObject, public QGraphicsPathItem
{
  Q_OBJECT
    
 public:
  /**
   * Default constructor
   *
   * Constructs an elliptic curve, with coefficients "a" and "b" are both set
   * to zero, and a graphical representation of the part of the curve that
   * lies within the specified bounding box.  For simplicity of
   * implementation, the method makes the following assumptions about the
   * boundingBox:
   *
   * - The boundingBox must be symmetric about the x-Axis (i.e. it must
   * satisfy boundingBox.top() > 0 and boundingBox.top() =
   * -boundingBox.bottom()
   *
   * - No point of the elliptic curve must be to the left of the boundingBox
   *
   * - The bounding box should not be more than about 100 units wide
   *
   * @param bBox Region for which the path is generated. This region
   * must satisfy the requirements mentioned above, or else graphical nonsense
   * will be returned.
   *
   * @param parentQObject This parameter is passed on to the QObject
   * constructor
   *
   * @param parentQGraphicsItem This parameter is passed on to the
   * QGraphicsPathItem constructor
   */
     ec_real(const QRectF &bBox,
             QObject *parentQObject = nullptr,
             QGraphicsItem *parentQGraphicsItem = nullptr);

     /**
   * Returns the coefficient 'a' of the short Weierstrass equation y^2
   * = x^3 + a*x + b.
   *
   * @see setA()
   */
     qreal a() const { return _a; };

     /**
   * Returns the coefficient 'b' of the short Weierstrass equation y^2
   * = x^3 + a*x + b.
   *
   * @see setB()
   */
     qreal b() const { return _b; };

     /**
   * Returns the Delta-invariant of the short Weierstrass equation y^2 = x^3 + a*x + b.
   *
   * The Delta-invariant is defined as Delta = -16*(4*a^3+27*b^2). It
   * has the following meaning.
   *
   * - The curve defined by the equation is singular if and only if Delta = 0.
   *
   * - If Delta != 0, then the curve consists of two connected components if
   *   and only if Delta > 0. Otherwise, the curve is connected,
   */
     qreal Delta() const { return -16 * (4 * _a * _a * _a + 27 * _b * _b); };

     /**
   * Checks if the curve is smooth.
   */
     bool isSmooth() const { return (Delta() != 0.0); }

     /**
   * Returns the type of the curve.
   */
     ec::realType getType() const;

     /**
   * Represents the equation as a string
   *
   * @returns A representation of the equation, as a string with HTML-Tags
   */
     QString equation() const;

     /**
   * Returns a verbose description of the curve in HTML format
   */
     QString description() const;

     /**
   * Evaluates the equation x^3 + a*x + b - y^2 at the point P.
   *
   * @param P Point where the equation is evaluated.
   * @returns value of equation x^3 + a*x + b - y^2 at the point P.
   */
     qreal value(QPointF P) const
     {
         return P.x() * P.x() * P.x() + _a * P.x() + _b - P.y() * P.y();};

  /**
   * Computes the gradient of the equation x^3 + a*x + b - y^2 at the point P.
   *
   * @param P Point where the gradient is evaluated.
   *
   * @returns Gradient of equation x^3 + a*x + b - y^2 at the point P.
   */
  QPointF gradient(QPointF P) const;

  /**
   * Computes the tangent line to the level set of the equation x^3 + a*x + b - y^2 at the point P.
   *
   * If the gradient of the x^3 + a*x + b - y^2 if not zero at the point P,
   * this method computes a direction vector for the tangent space of the
   * level set at P, so that the tangent line is given as { P + t*tangent | t
   * a real number}. More precisely, the method really returns the gradient of
   * the equation x^3 + a*x + b - y^2 at the point P, turned by 90 degrees. If
   * the gradient is (near) zero, this method therefore also returns a near
   * zero vector.
   *
   * @param P Point where the tangent is computed
   *
   * @returns If the gradient is non-zero, this methos returns the tangent
   * vector of the level set through P. Otherwise, a very small vector is
   * returned.
   */
  QPointF tangent(QPointF P) const;
  
  /**
   * Graphical representation of the curve
   *
   * This method produces a graphical representation of the curve as a
   * QPainterPath which can then be used to draw the curve. Only the portion
   * of the curve is drawn that roughly lies within a specified
   * boundingBox.
   * 
   * @returns A QPainterPath that represents the portion of the curve that
   * roughly lies within the bounding box.
   */
  QPainterPath path() const;

  /**
   * Finds a point on the curve, close to a given point.
   *
   * This method numerically computes a point P that is nearly a zero of the
   * equation x^3 + a*x + b - y^2, and is close to a given starting point. If
   * the computation succeeds, the absolute value of the equation x^3 + a*x +
   * b - y^2 at the point P is less than 0.01. The computation is usually very
   * fast. If the computation does not succeed, a random vector is returned.
   *
   * @param startPos The starting point of the numerical computation. If the
   * starting point has a y-coordinate which is precisely, the returned point
   * will again have a y-coordinate that is precisely zero.
   *
   * @param error The address of a bool. If not 0, the bool is set to 'true'
   * is an error occured, and set to 'false' if the computation succeeded.
   * 
   * @returns If the computation succeeded, a point P such that
   * isOnCurve(P)==true. In this case, if the startPos is close to the
   * elliptic curve, then the point P will be close to startPos. If the
   * computation does not succeed, a random vector is returned.
   */
  QPointF pointOnCurve(QPointF startPos, bool *error=nullptr) const;

  /**
   * For points that are reasonably close to the origin, this method checks if
   * the given point is approximately on the curve.
   *
   * This method works only if distance of the point to the origin is less
   * than 1000 units. For points which are further away, floating point
   * inaccurancies make it hard to properly guarantee the the point is close
   * to the curve.
   *
   * @param pt Point which is checked.
   *
   * @returns True if the distance from the origin is more than 1000 units, or
   * if the absolute value of the equation x^3 + a*x + b - y^2 at the point pt
   * is less than 0.01. False otherwise.
   */
  bool isOnCurve(QPointF pt) const;

  /**
   * Convenience function that returns the Euclidian norm of a vector.
   */
  static qreal length(QPointF v) {return sqrt(v.x()*v.x()+v.y()*v.y());};


 public slots:
  /**
   * Convenience function that updates the user-visible text and emits the
   * signal 'changed' even though nothing really changed.
   */
  void touch() {setAB(_a, _b);};

 /**
  * Sets the coefficient 'a' of the equation x^3 + a*x + b - y^2.
  *
  * This method sets the coefficient 'a' of the equation x^3 + a*x + b -
  * y^2. The signal 'changed' is emitted.
  * 
  * @param a The new coefficient for the equation. This value must be in the
  * range -10..10. If 'a' is outside of that range, 'a' will be set to either
  * -10 or 10.
  */
  void setA(qreal a) {setAB(a, _b);};

 /**
  * Sets the coefficient 'b' of the equation x^3 + a*x + b - y^2.
  *
  * This method sets the coefficient 'b' of the equation x^3 + a*x + b -
  * y^2. The signal 'changed' is emitted.
  * 
  * @param b The new coefficient for the equation. This value must be in the
  * range -10..10. If 'b' is outside of that range, 'b' will be set to either
  * -10 or 10.
  */
  void setB(qreal b) {setAB(_a, b);};

 /**
  * Sets the coefficients 'a' and 'b' of the equation x^3 + a*x + b - y^2.
  *
  * This method sets the coefficients 'a' and 'b' of the equation x^3 + a*x +
  * b - y^2. The signal 'changed' is emitted.
  * 
  * @param a The new coefficient for the equation. This value must be in the
  * range -10..10. If 'a' is outside of that range, 'a' will be set to either
  * -10 or 10.
  *
  * @param b The new coefficient for the equation. This value must be in the
  * range -10..10. If 'b' is outside of that range, 'b' will be set to either
  * -10 or 10.
  */
  void setAB(qreal a, qreal b);

 signals:
  /**
   * This signal is emitted when the coefficient of the equation x^3 + a*x + b
   * - y^2 change.
   */
  void changed();

 private:
  /**
   * Evaluates the equation x^3+ax+b
   */
  qreal f(qreal x) const {return x*x*x+ _a*x+ _b;};

  /**
   * Coefficients in the Weierstrass equations of the curve
   */
  qreal _a, _b;

  /**
   * Bounding box for the graphical representation of this curve
   */
  QRectF boundingBox;
};

#endif
