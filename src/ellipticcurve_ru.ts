<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="68"/>
        <source>Sketching elliptic curves over the real numbers</source>
        <translation>Изображение эллиптических кривых над полем вещественных чисел</translation>
    </message>
    <message>
        <location filename="main.cpp" line="73"/>
        <source>This program comes with ABSOLUTELY NO WARRANTY; for details
use the menu item &apos;About the Elliptic Curve Plotter&apos; from
the &apos;Help&apos; menu. This is free software, and you are welcome
to redistribute it under certain conditions; use the menu
item &apos;About the Elliptic Curve Plotter&apos; from the &apos;Help&apos;
menu for details.

Usage: %1 [Options]

-help  Show this help text.

-kiosk Run the in Kiosk Mode, a restricted full-screen mode
       useful for running the program on terminals or kiosks
       that are open to the public.

</source>
        <translation>Эта программа предоставляется БЕЗ ВСЯКИХ ГАРАНТИЙ; более подробно читайте пункт &apos;О Elliptic Curve Plotter&apos; в меню &apos;Помощь&apos;. Эта бесплатное приложение и Вы можете его распространять при определённых условиях; Для подробностей, смотрите пункт &apos;О Elliptic Curve Plotter&apos; в меню &apos;Помощь&apos;.

Использование: %1 [Опции]

-помощь  Показывать вспомогательный текст.

-режим киоска Запустите в режиме киоска, ограниченный полноэкранный режм удобен для запуска программы на терминалах или киосках открытых для общественного использования.
</translation>
    </message>
</context>
<context>
    <name>aboutDialog</name>
    <message>
        <location filename="aboutDialog.ui" line="14"/>
        <source>About the Elliptic Curve Plotter</source>
        <translation>О Elliptic Curve Plotter</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="24"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;;&quot;&gt;Sketching elliptic curves over the real numbers.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;;&quot;&gt;Изображение эллиптической кривой над полем вещественных чисел&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="74"/>
        <source>Author</source>
        <translation>Автор</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="89"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:16px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/kebekus.png&quot; style=&quot;float: right;&quot; /&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Stefan Kebekus&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:600;&quot;&gt;Address&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;Stefan Kebekus&lt;br /&gt;Albert-Ludwigs-Universität Freiburg&lt;br /&gt;Ernst-Zermelo-Straße 1&lt;br /&gt;79104 Freiburg im Breisgau&lt;br /&gt;GERMANY&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:600;&quot;&gt;E-Mail&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;mailto:stefan.kebekus@math.uni-freiburg.de&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; text-decoration: underline; color:#0057ae;&quot;&gt;stefan.kebekus@math.uni-freiburg.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:600;&quot;&gt;Homepage&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://cplx.vm.uni-freiburg.de&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; text-decoration: underline; color:#0057ae;&quot;&gt;https://cplx.vm.uni-freiburg.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:16px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/kebekus.png&quot; style=&quot;float: right;&quot; /&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Stefan Kebekus&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Address&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Stefan Kebekus&lt;br /&gt;Albert-Ludwigs-Universität Freiburg&lt;br /&gt;Ernst-Zermelo-Straße 1&lt;br /&gt;79104 Freiburg im Breisgau&lt;br /&gt;GERMANY&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;E-Mail&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;mailto:stefan.kebekus@math.uni-freiburg.de&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;stefan.kebekus@math.uni-freiburg.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Homepage&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://home.mathematik.uni-freiburg.de/kebekus&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://home.mathematik.uni-freiburg.de/kebekus&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Благодарность&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:16px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/kebekus.png&quot; style=&quot;float: right;&quot; /&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Stefan Kebekus&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Адрес&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Stefan Kebekus&lt;br /&gt;Albert-Ludwigs-Universität Freiburg&lt;br /&gt;Ernst-Zermelo-Straße 1&lt;br /&gt;79104 Freiburg im Breisgau&lt;br /&gt;GERMANY&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Электронный адрес&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;mailto:stefan.kebekus@math.uni-freiburg.de&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;stefan.kebekus@math.uni-freiburg.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Домашняя станица&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://cplx.vm.uni-freiburg.de&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;https://cplx.vm.uni-freiburg.de&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="113"/>
        <source>Acknowledgements</source>
        <translation>Благодарность</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="119"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Inspiration&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;This program was inspired by Java applets included in the &amp;quot;ECC Tutorial&amp;quot; found on the homepage of Certicom Corp. This program does not share code or other components with any of Certicom&apos;s products.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;Link:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;monospace&apos;; font-size:9pt;&quot;&gt; &lt;/span&gt;&lt;a href=&quot;http://www.certicom.com/index.php/ecc-tutorial&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.certicom.com/index.php/ecc-tutorial&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Artwork&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;Several icons used in this program are versions of icons found in the &lt;/span&gt;&lt;a href=&quot;www.kde.org&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; text-decoration: underline; color:#0057ae;&quot;&gt;K Desktop Environment&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;, Version 4.0, or modified version of these icons.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Благодарность&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Вдохновение&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Вдохновением для создания этой программы послужили Java applets включённые в &amp;quot;ECC Tutorial&amp;quot;, находящиеся на домашней странице Certicom Corp. Эта программа не использует ни код ни другие компоненты каких-либо продуктов Certicom Corp.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Ссылка:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;monospace&apos;;&quot;&gt; &lt;/span&gt;&lt;a href=&quot;http://www.certicom.com/index.php/ecc-tutorial&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.certicom.com/index.php/ecc-tutorial&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Художественное оформление&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Несколько иконок используемых в этой программе являются модифицированными версиями иконок поставляемых с &lt;/span&gt;&lt;a href=&quot;www.kde.org&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;K Desktop Environment&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;, версия 4.0, или модифицированными версиями этих иконок.&lt;/span&gt;&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="144"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Translations&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;The following people have kindly helped to translate the Elliptic Curve Plotter into other languages.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Переводы&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Следующие люди помогли перевести $NAME на другие языки.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="172"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;license&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:600;&quot;&gt;Icons&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt;&quot;&gt;Several icons used in this program are modified versions of icons distributed with the K Desktop Environment, Version 4.0. These icons are licensed under GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;license&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Эта программа является бесплатным приложением: Вы можете распространять и/или изменять её согласно условиям GNU General Public License опубликованным Free Software Foundation - это касается версии 3 лицензии или (по Вашему выбору) любой более поздней версии.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Эта программа поставляется в надежде, что она будет полезной, но БЕЗ ВСЯКИХ ГАРАНТИЙ; даже не включая гарантию MERCHANTABILITY или FITNESS FOR A PARTICULAR PURPOSE. Смотрите детали в GNU General Public License.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Иконки&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Несколько иконок используемых в этой программе являются модифицированными версиями иконок поставляемых с K Desktop Environment, версия 4.0. Эти иконки лицензированы GNU General Public License опубликованным Free Software Foundation - это касается версии 3 лицензии или (по Вашему выбору) любой более поздней версии.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Inspiration&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;This program was inspired by Java applets included in the &amp;quot;ECC Tutorial&amp;quot; found on the homepage of Certicom Corp. This program does not share code or other components with any of Certicom&apos;s products.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Link:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;monospace&apos;;&quot;&gt; &lt;/span&gt;&lt;a href=&quot;http://www.certicom.com/index.php/ecc-tutorial&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.certicom.com/index.php/ecc-tutorial&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Artwork&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Several icons used in this program are versions of icons found in the &lt;/span&gt;&lt;a href=&quot;www.kde.org&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;K Desktop Environment&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;, Version 4.0, or modified version of these icons.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Software used in the development&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/ermine.png&quot; style=&quot;float: right;&quot; /&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;The portable binaries for Linux were created using the program &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Ermine Light&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;. The authors of Ermine Light kindly supported the development of the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Elliptic CurvePlotter&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt; with a free license.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Link:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;monospace&apos;;&quot;&gt; &lt;/span&gt;&lt;a href=&quot;http://www.magicermine.com&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.magicermine.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Благодарность&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Вдохновение&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Вдохновением для создания этой программы послужили Java applets включённые в &amp;quot;ECC Tutorial&amp;quot;, находящиеся на домашней странице Certicom Corp. Эта программа не использует ни код ни другие компоненты каких-либо продуктов Certicom Corp.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Ссылка:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;monospace&apos;;&quot;&gt; &lt;/span&gt;&lt;a href=&quot;http://www.certicom.com/index.php/ecc-tutorial&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.certicom.com/index.php/ecc-tutorial&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Художественное оформление&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Несколько иконок используемых в этой программе являются модифицированными версиями иконок поставляемых с &lt;/span&gt;&lt;a href=&quot;www.kde.org&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;K Desktop Environment&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;, версия 4.0, или модифицированными версиями этих иконок.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Программное обеспечение использованное при разработке&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/ermine.png&quot; style=&quot;float: right;&quot; /&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Портативные бинарные файлы для Linux были созданы с помощью программы &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Ermine Light&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;. Авторы Ermine Light поддержали разработку &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Elliptic CurvePlotter&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt; предоставив бесплатную лицензию.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Ссылка:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;monospace&apos;;&quot;&gt; &lt;/span&gt;&lt;a href=&quot;http://www.magicermine.com&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.magicermine.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="138"/>
        <source>Translations</source>
        <translation>Переводы</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Translations&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The following people have kindly helped to translate the Elliptic Curve Plotter into other languages.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Переводы&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Следующие люди помогли перевести Elliptic Curve Plotter на другие языки.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="160"/>
        <source>License</source>
        <translation>Лицензия</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;license&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Copyright (C) 2018 Stefan Kebekus&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Icons&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Several icons used in this program are modified versions of icons distributed with the K Desktop Environment, Version 4.0. These icons are licensed under GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;license&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Авторские права (C) 2018 Stefan Kebekus&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Эта программа является бесплатным приложением: Вы можете распространять и/или изменять её согласно условиям GNU General Public License опубликованным Free Software Foundation - это касается версии 3 лицензии или (по Вашему выбору) любой более поздней версии.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Эта программа поставляется в надежде, что она будет полезной, но БЕЗ ВСЯКИХ ГАРАНТИЙ; даже не включая гарантию MERCHANTABILITY или FITNESS FOR A PARTICULAR PURPOSE. Смотрите детали в GNU General Public License.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;; font-weight:600;&quot;&gt;Иконки&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Ubuntu&apos;;&quot;&gt;Несколько иконок используемых в этой программе являются модифицированными версиями иконок поставляемых с K Desktop Environment, версия 4.0. Эти иконки лицензированы GNU General Public License опубликованным Free Software Foundation - это касается версии 3 лицензии или (по Вашему выбору) любой более поздней версии.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="201"/>
        <source>GPL</source>
        <translation>GPL</translation>
    </message>
    <message>
        <location filename="aboutDialog.ui" line="342"/>
        <source>FDL</source>
        <translation>FDL</translation>
    </message>
</context>
<context>
    <name>ecDescription</name>
    <message>
        <location filename="ecDescription.cpp" line="85"/>
        <source>Points on the elliptic curve</source>
        <translation>Точки на эллиптической кривой</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="89"/>
        <source>Experiment with the curve and its group law!</source>
        <translation>Экспериментируйте с кривой и её групповым законом!</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="91"/>
        <source>Use the sliders to change the shape of the curve.</source>
        <translation>Используйте ползунок для изменения формы кривой.</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="93"/>
        <source>Use the sliders to change the shape of the curve, or drag some points to the curve and use the menu &apos;Group Law&apos; to experiment with the group law.</source>
        <translation>Используйте ползунок для изменения формы кривой, или перетяните точки кривой и используйте меню &quot;Групповой закон&quot; для экспериментов с групповым законом.</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="105"/>
        <source>The inverse of the point &lt;b&gt;P&lt;/b&gt;</source>
        <translation>Обратная точки &lt;b&gt;P&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="118"/>
        <source>The double of the point &lt;b&gt;P&lt;/b&gt;</source>
        <translation>Удвоенная точка&lt;b&gt;P&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="135"/>
        <source>Sums of points</source>
        <translation>суммы точек</translation>
    </message>
    <message>
        <location filename="ecDescription.cpp" line="154"/>
        <source>Sums of three points</source>
        <translation>суммы трёх точек</translation>
    </message>
</context>
<context>
    <name>ecPlotter</name>
    <message>
        <location filename="ecPlotter.cpp" line="187"/>
        <source>Point Box</source>
        <translation>Бокс точек</translation>
    </message>
    <message>
        <location filename="ecPlotter.cpp" line="189"/>
        <source>&lt;h4&gt;Point Box&lt;/h4&gt;&lt;p&gt;Drag the points from this box to the curve and vice versa.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;Бокс точек&lt;/h4&gt;&lt;p&gt;Переместите точки из этой рамки на кривую и наоборот.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ecPlotter.cpp" line="215"/>
        <source>Drag the points to the curve, and use the &apos;Group Law&apos; menu to experiment with the group structure.</source>
        <translation>Переместите точки на кривую, и используйте меню &quot;Групповой закон&quot; для експериментов со структурой группы.</translation>
    </message>
    <message>
        <location filename="ecPlotter.cpp" line="217"/>
        <source>Drag the points to the curve.</source>
        <translation>Переместите точки на кривую.</translation>
    </message>
    <message>
        <location filename="ecPlotter.cpp" line="220"/>
        <source>Drag the points along the curve, and use the &apos;Group Law&apos; menu to experiment with the group structure.</source>
        <translation>Переместите точки вдоль кривой, и используйте меню &quot;Групповой закон&quot; для експериментов со структурой группы.</translation>
    </message>
</context>
<context>
    <name>ec_real</name>
    <message>
        <location filename="ec_real.cpp" line="66"/>
        <source>&lt;h4&gt;Elliptic Curve&lt;/h4&gt;&lt;p&gt;The curve outlined in black is the elliptic curve defined by the equation&lt;/p&gt;&lt;p&gt;%1.&lt;/p&gt; </source>
        <translation>&lt;h4&gt;Эллиптическая кривая&lt;/h4&gt;&lt;p&gt;Кривая выделенная чёрным является эллиптической кривой, которая задаётся уравнением&lt;/p&gt;&lt;p&gt;%1.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_real.cpp" line="70"/>
        <source>&lt;h4&gt;Curve&lt;/h4&gt;&lt;p&gt;The curve outlined in black is the (non-elliptic) curve defined by the equation&lt;/p&gt;&lt;p&gt;%1.&lt;/p&gt; </source>
        <translation>&lt;h4&gt;Кривая&lt;/h4&gt;&lt;p&gt;Кривая выделенная чёрным является (не эллиптической) кривой, которая задаётся уравнением&lt;/p&gt;&lt;p&gt;%1.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_real.cpp" line="95"/>
        <source>&lt;h2&gt;Neil Parabola&lt;/h2&gt;&lt;p&gt;This curve is not an elliptic curve. It is a singular curve,  known as the &lt;span style=&quot;font-style: italic;&quot;&gt;Neil parabola&lt;/span&gt;  or &lt;span style=&quot;font-style: italic;&quot;&gt;semicubical parabola&lt;/span&gt;.  The singularity at the origin is usually called a &lt;span    style=&quot;font-style: italic;&quot;&gt;cusp&lt;/span&gt;. The Neil parabola is a  rational curve. This means that it can be parameterized using only  rational functions and without using transzendental functions such  as sine or cosine. One such parameterization is given as t &amp;#8594;  (t&amp;sup2;, t&amp;sup3;).&amp;nbsp;&lt;/p&gt;&lt;p&gt;This curve does not have a group structure. There is, however, a  group structure on the smooth locus of the curve. This group is  isomorphic to the group of real numbers with addition. The Neil  parabola is named after William Neil (1637&amp;#8212;1670) who first  computed its arc length. Historically, the Neil parabola was the  first algebraic curve whose arc length could be computed.&lt;/p&gt;</source>
        <translation>&lt;h2&gt;Парабола Нейла&lt;/h2&gt;&lt;p&gt;Эта кривая не является эллиптической кривой. Эта сингулярная кривая, известная под именем &lt;span style=&quot;font-style: italic;&quot;&gt;Парабола Нейла&lt;/span&gt; или &lt;span style=&quot;font-style: italic;&quot;&gt;Полукубическая парабола&lt;/span&gt;. Сингулярность в начале координат обычно называется &lt;span    style=&quot;font-style: italic;&quot;&gt;точкой возврата&lt;/span&gt;. Парабола Нейла является рациональной кривой. Это означает, что её можно параметризовать используя только рациональные функции, не включая трансцендентные функции типа синуса или косинуса. Такая параметризация задается например t &amp;#8594;  (t&amp;sup2;, t&amp;sup3;).&amp;nbsp;&lt;/p&gt;&lt;p&gt;Эта кривая не имеет групповой структуры. Но, существует структура группы на гладкой части этой кривой. Эта группа изоморфна группе вещественных чисел со сложением. Парабола Нейла названа в честь Уильяма Нейла (1637&amp;#8212;1670), который был первым, кто сумел рассчитать её длину. Исторически, Парабола Нейла была первой алгебраической кривой, чью длину удалось рассчитать.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_real.cpp" line="112"/>
        <source>&lt;h2&gt;Nodal Plane Cubic&lt;/h2&gt;&lt;p&gt;This curve is not an elliptic curve. It is a singular curve,  known as the &lt;span style=&quot;font-style: italic;&quot;&gt;Nodal Plane Cubic&lt;/span&gt;.  The singularity is usually called a &lt;span style=&quot;font-style:    italic;&quot;&gt;node&lt;/span&gt;.&lt;/p&gt;&lt;p&gt;The nodal plane cubic is a rational curve. This means that it can  be parameterized using only rational functions and without using  transzendental functions such as sine or cosine. One such  parameterization is given as t &amp;#8594; ( t&amp;sup2;-2, t&amp;sup3;-3t).&lt;/p&gt;&lt;p&gt; This curve does not have a group structure. There is, however, a  group structure on the smooth locus of the curve. This group is  isomorphic to the group of non-zero real numbers with  multiplication.&lt;/p&gt;</source>
        <translation>&lt;h2&gt;Нодальная плоская кубика&lt;/h2&gt;&lt;p&gt;Эта кривая не является эллиптической кривой. Эта сингулярная кривая, известная как &lt;span style=&quot;font-style: italic;&quot;&gt;нодальная плоская кубика&lt;/span&gt;. Сингулярность такого типа обычно называют &lt;span style=&quot;font-style:    italic;&quot;&gt;узлом&lt;/span&gt;.&lt;/p&gt;&lt;p&gt;.Нодальная плоская кубика является рациональной кривой. Это означает, что её можно параметризовать используя только рациональные функции, не включая трансцендентные функции типа синуса или косинуса. Такая параметризация задается например t &amp;#8594; ( t&amp;sup2;-2, t&amp;sup3;-3t).&lt;/p&gt;&lt;p&gt; Эта кривая не имеет групповой структуры. Но, существует структура группы на гладкой части этой кривой. Эта группа изоморфна группе вещественных чисел без нуля с умножением.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_real.cpp" line="126"/>
        <source>&lt;h2&gt;Curve with Isolated Point&lt;/h2&gt;&lt;p&gt;This is a singular curve, and not a smooth elliptic curve. Over the  real numbers, the equation defines the curve that you see on the left,  and an additional point with coordinates (-1, 0). If you move the  slider for the value &lt;span style=&quot;font-style: italic;&quot;&gt;b&lt;/span&gt; a  little you can see that the point is a degenerate form of the oval that  you see in many smooth elliptic curves.&lt;/p&gt;&lt;p&gt;The curve with the isolated point does not form a group.&lt;/p&gt;</source>
        <translation>&lt;h2&gt;Кривая с изолированной точкой&lt;/h2&gt;&lt;p&gt;Это сингулярная кривая, которая не является эллиптической. Над вещественными числами уравнение определяет кривую, которую Вы видите слева и дополнительную точку с координатами (-1, 0). Если Вы немного передвините слайдер для значения &lt;span style=&quot;font-style: italic;&quot;&gt;b&lt;/span&gt;, то увидите, что точка является вырожденной формой овала, что можно наблюдать во многих гладких эллиптических кривых.&lt;/p&gt;&lt;p&gt; Кривая с изолированной точкой не образует группу.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_real.cpp" line="135"/>
        <source>&lt;h2&gt;Elliptic Curve with Oval&lt;/h2&gt;&lt;p&gt;This curve is a smooth elliptic curve. The points of this curve  form a group.&lt;/p&gt;&lt;p&gt;This elliptic curve consists of two parts, classically called  &quot;oval&quot; and &quot;parabola&quot;. Interestingly, it is not possible to define  the oval or the parabola alone with algebraic equations: any  polynomial function that vanishes along one component must  necessarily also vanish along the other.&lt;/p&gt;</source>
        <translation>&lt;h2&gt;Эллиптическая кривая с овалом&lt;/h2&gt;&lt;p&gt;Эта кривая является гладкой эллиптической кривой. Точки этой кривой образуют группу.&lt;/p&gt;&lt;p&gt;Эта эллиптическая кривая состоит из двух частей, которые (классически) называются &quot;овал&quot; и &quot;парабола&quot;. Невозможно определить только овал или только параболу за счёт алгебраических уравнений: каждая полиномиальная функция, которая исчезает на одном из компонентов, необходимо исчезает и на другом.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_real.cpp" line="144"/>
        <source>&lt;h2&gt;Elliptic Curve without Oval&lt;/h2&gt;&lt;p&gt;This curve is a smooth elliptic curve. The points of this curve  form a group.&lt;/p&gt;</source>
        <translation>&lt;h2&gt;Эллиптическая кривая без овала&lt;/h2&gt;&lt;p&gt;Эта кривая является гладкой эллиптической кривой. Точки этой кривой образуют группу.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>ec_realDoublePoint</name>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="59"/>
        <source>The point &lt;b&gt;%1&lt;/b&gt; is the double of the point &lt;b&gt;%2&lt;/b&gt; according to the group law of the elliptic curve. Geometrically, the point &lt;b&gt;%1&lt;/b&gt; is constructed by taking the tangent line through &lt;b&gt;%2&lt;/b&gt;. This tangent line intersects the elliptic curve in the point &lt;b&gt;%2&lt;/b&gt;, and in the point at infinity.</source>
        <translation>Точка &lt;b&gt;%1&lt;/b&gt; является, по групповому законы эллиптической кривой, двойным от точки &lt;b&gt;%2&lt;/b&gt;. Геометрически точка &lt;b&gt;%1&lt;/b&gt; строится, взятием касательной через &lt;b&gt;%2&lt;/b&gt;. Эта касательная пересекает эллиптическую кривую в точке &lt;b&gt;%2&lt;/b&gt;, и в бесконечно отдаленной точке.</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="64"/>
        <source>The point &lt;b&gt;%1&lt;/b&gt; is the double of the point &lt;b&gt;%2&lt;/b&gt; according to the group law of the elliptic curve. Geometrically, the point &lt;b&gt;%1&lt;/b&gt; is constructed by taking the tangent line through &lt;b&gt;%2&lt;/b&gt;. This tangent line will intersect the elliptic curve in the point &lt;b&gt;%2&lt;/b&gt;, and in exactly one second point. The double of &lt;b&gt;%2&lt;/b&gt; is then constructed as the image of the second point under reflection against the x-axis.</source>
        <translation>Точка &lt;b&gt;%1&lt;/b&gt; является, по групповому законы эллиптической кривой, двойным от точки &lt;b&gt;%2&lt;/b&gt;. Геометрически точка &lt;b&gt;%1&lt;/b&gt; строится, взятием касательной через &lt;b&gt;%2&lt;/b&gt;. Эта касательная пересекает эллиптическую кривую в точке &lt;b&gt;%2&lt;/b&gt;, и ровно в одной другой точке. Двойное точки &lt;b&gt;%2&lt;/b&gt; строится как образ второй точки при отражении отностительно оси х.</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="102"/>
        <source>&lt;h4&gt;Auxiliary Line&lt;/h4&gt;&lt;p&gt;The point &lt;b&gt;%1&lt;/b&gt; is constructed as the image of the small yellow point under reflection against the x-axis. This auxiliary line connects the point &lt;b&gt;%1&lt;/b&gt; and the small yellow point.&lt;/p&gt;&lt;p&gt;Move the mouse pointer to the point &lt;b&gt;%1&lt;/b&gt; to learn more about the construction.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;Вспомогательная линия&lt;/h4&gt;&lt;p&gt;Точка &lt;b&gt;%1&lt;/b&gt; строится как образ маленькой желтой точки при отражении относительно оси х. Эта вспомогательная линия соединяет точку &lt;b&gt;%1&lt;/b&gt; с маленькой желтой точкой.&lt;/p&gt;&lt;p&gt;Чтобы понять лучше конструкцию, наведите указатель мышки на точку &lt;b&gt;%1&lt;/b&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="108"/>
        <source>&lt;h4&gt;Auxiliary Point&lt;/h4&gt;&lt;p&gt;This is an auxiliary point used in the construction of the double of the point &lt;b&gt;%1&lt;/b&gt;. The double of &lt;b&gt;%1&lt;/b&gt; is constructed by looking at the tangent line through &lt;b&gt;%1&lt;/b&gt;. The tangent line is guaranteed to intersect the elliptic curve in the point &lt;b&gt;%1&lt;/b&gt; and in precisely one second point, the point you are looking at right now.&lt;/p&gt;&lt;p&gt;To learn more about the construction, move the mouse pointer to the tangent line or to the point &lt;b&gt;%2&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;Вспомогательная точка&lt;/h4&gt;&lt;p&gt;Это вспомогательная  точка используемая при конструкции двойного от точки &lt;b&gt;%1&lt;/b&gt;. Двойное от &lt;b&gt;%1&lt;/b&gt; конструируется с помощью касательной через &lt;b&gt;%1&lt;/b&gt;. Касательная гарантированно пересекает эллиптическую кривую в точке &lt;b&gt;%1&lt;/b&gt; и ровно в ещё одной точке, а именно в точке, на которую Вы сейчас смотрите.&lt;/p&gt;&lt;p&gt; Что бы лучше понять конструкцию, наведите указатель мыши на касательную или на точку &lt;b&gt;%2&lt;/b&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="118"/>
        <source>&lt;h4&gt;Double of the point &lt;b&gt;%1&lt;/b&gt;&lt;/h4&gt;</source>
        <translation>&lt;h4&gt;Двойное точки &lt;b&gt;%1&lt;/b&gt;&lt;/h4&gt;</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="120"/>
        <source>&lt;p&gt;Move the point &lt;b&gt;%1&lt;/b&gt; to see how the double of &lt;b&gt;%1&lt;/b&gt; changes along with &lt;b&gt;%1&lt;/b&gt;. You can also use the sliders to change the shape of the elliptic curve, and to see how the whole construction changes.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Переместите точку &lt;b&gt;%1&lt;/b&gt; чтобы увидеть, как меняется двойное точки &lt;b&gt;%1&lt;/b&gt; при изменении точки &lt;b&gt;%1&lt;/b&gt;. Вы можете использовать ползунки, чтобы изменить форму кривой и чтобы увидеть, как изменяется вся конструкция.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="125"/>
        <source>&lt;h4&gt;Tangent Line&lt;/h4&gt;&lt;p&gt;This line is the tangent line through &lt;b&gt;%2&lt;/b&gt;, an auxiliary line used in the construction of the point &lt;b&gt;%1&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;Касательная&lt;/h4&gt;&lt;p&gt;Эта прямая является касательной через &lt;b&gt;%2&lt;/b&gt;, которая является вспомогательной линией, используемой при конструкции точки &lt;b&gt;%1&lt;/b&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="129"/>
        <source>&lt;p&gt;The tangent line is the unique line which contains &lt;b&gt;%1&lt;/b&gt; and just touches the ellipticcurve, without intersecting it transversely. The tangent line is guaranteed to intersect the elliptic curve in the point &lt;b&gt;%1&lt;/b&gt; and in precisely one second point, which is offscreen and therefore not shown in the picture.&lt;/p&gt;&lt;p&gt;The point &lt;b&gt;%2&lt;/b&gt; is constructed as the image of the second intersection under reflection against the x-axis.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Касательная является единственной прямой, которая содержит &lt;b&gt;%1&lt;/b&gt; и только касается эллиптической кривой не пересекая её. Касательная гарантированно пересекает эллиптическую кривую в точке &lt;b&gt;%1&lt;/b&gt; и ещё ровно в одной другой точке, которая находится вне экрана и, поэтому, не показана на рисунке.&lt;/p&gt;&lt;p&gt;Точка &lt;b&gt;%2&lt;/b&gt; строится как образ второй точки пересечения при отражении относительно оси x.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realDoublePoint.cpp" line="135"/>
        <source>&lt;p&gt;The tangent line is the unique line which contains &lt;b&gt;%1&lt;/b&gt; and just touches the ellipticcurve, without intersecting it transversely. The tangent line is guaranteed to intersect the elliptic curve in the point &lt;b&gt;%1&lt;/b&gt; and in precisely one second point.&lt;/p&gt;&lt;p&gt;Move the mouse pointer to the point &lt;b&gt;%2&lt;/b&gt; to learn more about the construction.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Касательная является единственной прямой, которая содержит &lt;b&gt;%1&lt;/b&gt; и только касается эллиптической кривой не пересекая её. Касательная гарантированно пересекает эллиптическую кривую в точке &lt;b&gt;%1&lt;/b&gt; и ещё ровно в одной другой точке.&lt;/p&gt;&lt;p&gt; Наведите указатель мыши на точку &lt;b&gt;%2&lt;/b&gt; что бы лучше понять конструкцию.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>ec_realFreePoint</name>
    <message>
        <location filename="ec_realFreePoint.cpp" line="71"/>
        <source>Point</source>
        <translation>Точка</translation>
    </message>
    <message>
        <location filename="ec_realFreePoint.cpp" line="73"/>
        <source>Move the point along the curve, or move it to its home position.</source>
        <translation>Переместите точку вдоль кривой, или переместить его в исходное положение.</translation>
    </message>
    <message>
        <location filename="ec_realFreePoint.cpp" line="89"/>
        <source>This point cannot be moved to the curve right now, because the curve is not an elliptic curve. Before moving the point, use the sliders to change the shape of the curve.</source>
        <translation>Эта точка не может быть перемещена на кривую, поскольку кривая не является эллиптической кривой. Перед перемещением точки, используйте ползунки, чтобы изменить форму кривой.</translation>
    </message>
    <message>
        <location filename="ec_realFreePoint.cpp" line="99"/>
        <source>Drag the point to the curve!</source>
        <translation>Перетащите точку на кривую!</translation>
    </message>
</context>
<context>
    <name>ec_realMinusPoint</name>
    <message>
        <location filename="ec_realMinusPoint.cpp" line="57"/>
        <source>The point &lt;b&gt;%1&lt;/b&gt; is the inverse of the point &lt;b&gt;%2&lt;/b&gt; according to the group law of the elliptic curve. Geometrically, the point &lt;b&gt;%1&lt;/b&gt; is constructed as the image of the point &lt;b&gt;%2&lt;/b&gt; under reflection against the x-axis.</source>
        <translation>Точка &lt;b&gt;%1&lt;/b&gt; является, по групповому законы эллиптической кривой, обратной точки &lt;b&gt;%2&lt;/b&gt;. Геометрически точка &lt;b&gt;%1&lt;/b&gt; строится как образ точки &lt;b&gt;%2&lt;/b&gt; при отражении относительно оси х.</translation>
    </message>
    <message>
        <location filename="ec_realMinusPoint.cpp" line="86"/>
        <source>&lt;h4&gt;Auxiliary Line&lt;/h4&gt;&lt;p&gt;The point &lt;b&gt;%1&lt;/b&gt; is constructed as the image of the point &lt;b&gt;%2&lt;/b&gt; under reflection against the x-axis. This auxiliary line connects the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%1&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>Вспомогательная линия&lt;/h4&gt;&lt;p&gt;Точка &lt;b&gt;%1&lt;/b&gt; строится как образ точки &lt;b&gt;%2&lt;/b&gt; при отражении относительно оси х. Эта вспомогательная линия соединяет точку &lt;b&gt;%2&lt;/b&gt; с точкой &lt;b&gt;%1&lt;/b&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realMinusPoint.cpp" line="93"/>
        <source>Inverse of the Point &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Обратная точки &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="ec_realMinusPoint.cpp" line="95"/>
        <source>Move the point &lt;b&gt;%1&lt;/b&gt; to see how the inverse changes along with &lt;b&gt;%1&lt;/b&gt;. You can also change the shape of the curve to see how the whole construction changes.</source>
        <translation>Переместите точку &lt;b&gt;%1&lt;/b&gt; чтобы увидеть, как меняется обратная точки при изменении точки &lt;b&gt;%1&lt;/b&gt;. Вы также можете изменить форму кривой чтобы увидеть, как изменяется вся конструкция.</translation>
    </message>
</context>
<context>
    <name>ec_realPoint</name>
    <message>
        <location filename="ec_realPoint.cpp" line="125"/>
        <source>Point</source>
        <translation>Точка</translation>
    </message>
    <message>
        <location filename="ec_realPoint.cpp" line="149"/>
        <source>The point &lt;b&gt;%1&lt;/b&gt; is the point at infinity and therefore not shown on the screen.</source>
        <translation>Точка &lt;b&gt;%1&lt;/b&gt; является точкой в бесконечности и поэтому не показывается на экране.</translation>
    </message>
    <message>
        <location filename="ec_realPoint.cpp" line="152"/>
        <source>The coordinates of the point &lt;b&gt;%1&lt;/b&gt; are approximately (%2, %3).</source>
        <translation>Координаты точки &lt;b&gt;%1&lt;/b&gt; приблизительно равны (%2, %3).</translation>
    </message>
    <message>
        <location filename="ec_realPoint.cpp" line="154"/>
        <source>The point is &lt;b&gt;%1&lt;/b&gt; is a special point of the elliptic curve. If you use the group law of the curve to add the point &lt;b&gt;%1&lt;/b&gt; to itself, the point at infinity is obtained as a result. The inverse of &lt;b&gt;%1&lt;/b&gt; is the same as &lt;b&gt;%1&lt;/b&gt;. Mathematicians say that &lt;b&gt;%1&lt;/b&gt; is a 2-torsion point of the elliptic curve.</source>
        <translation>Точка &lt;b&gt;%1&lt;/b&gt; является особой точкой эллиптической кривой. Если Вы используете групповой закон, что бы прибавить точку &lt;b&gt;%1&lt;/b&gt; к самой себе, то в результате получите бесконечно отдалённую точку. Обратной точки&lt;b&gt;%1&lt;/b&gt; является сама точка &lt;b&gt;%1&lt;/b&gt;. Математики говорят, что &lt;b&gt;%1&lt;/b&gt; является точкой 2-кручения эллиптической кривой.</translation>
    </message>
    <message>
        <location filename="ec_realPoint.cpp" line="159"/>
        <source> Because the coordinates are so large, the point is offscreen and therefore not shown.</source>
        <translation>Поскольку координаты настолько велики, то точка находится вне экрана и, поэтому, не показана.</translation>
    </message>
</context>
<context>
    <name>ec_realSumPoint</name>
    <message>
        <location filename="ec_realSumPoint.cpp" line="66"/>
        <source>The point &lt;b&gt;%1&lt;/b&gt; is the sum of the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt; according to the group law of the elliptic curve. Geometrically, the point &lt;b&gt;%1&lt;/b&gt; is constructed by taking the line through &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;. This line will intersect the elliptic curve in the exactly one further point, which is offscreen and therefore not shown in the picture. The sum &lt;b&gt;%1&lt;/b&gt; is then constructed as the image of the third point under reflection against the x-axis.</source>
        <translation>Точка &lt;b&gt;%1&lt;/b&gt; является суммой точек &lt;b&gt;%2&lt;/b&gt; и &lt;b&gt;%3&lt;/b&gt; согласно групповому закону эллиптической кривой. Геометрически точка &lt;b&gt;%1&lt;/b&gt; конструируется с помощью прямой проведённой через &lt;b&gt;%2&lt;/b&gt; и &lt;b&gt;%3&lt;/b&gt;. Эта прямая пересекает эллиптическую кривую ровно в ещё одной точке, которая находится вне экрана и, поэтому, не показана на рисунке. Сумма &lt;b&gt;%1&lt;/b&gt; строится как образ третьей точки при отражении третьей точки относительно оси x.</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="72"/>
        <source>The point &lt;b&gt;%1&lt;/b&gt; is the sum of the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt; according to the group law of the elliptic curve. Geometrically, the point &lt;b&gt;%1&lt;/b&gt; is constructed by taking the line through &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;. This line will intersect the elliptic curve in the exactly one further point, shown as a small yellow circle. The sum &lt;b&gt;%1&lt;/b&gt; is then constructed as the image of the third point under reflection against the x-axis.</source>
        <translation>Точка &lt;b&gt;%1&lt;/b&gt; является суммой точек &lt;b&gt;%2&lt;/b&gt; и &lt;b&gt;%3&lt;/b&gt; согласно групповому закону эллиптической кривой. Геометрически точка &lt;b&gt;%1&lt;/b&gt; конструируется с помощью прямой проведённой через &lt;b&gt;%2&lt;/b&gt; и &lt;b&gt;%3&lt;/b&gt;. Эта прямая пересекает эллиптическую кривую ровно в ещё одной точке, обозначенной маленьким жёлтым кружочком. Сумма &lt;b&gt;%1&lt;/b&gt; как образ третьей точки при отражении третьей точки относительно оси x.</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="90"/>
        <source>The figure shows that the point &lt;b&gt;%1&lt;/b&gt; can also be constructed as the sum of the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;. This illustrates the fundamental fact that the order does not play any role when adding points on an elliptic curve. Mathematicians express this fact by saying that &apos;addition on an elliptic curve is associative&apos;. Associativity is a consequence of &apos;Noether&apos;s residual intersection theorem&apos;, named after the german mathematician Max Noether (1844-1921).</source>
        <translation>Из изображения видно, что точку &lt;b&gt;%1&lt;/b&gt; также можно сконструировать как сумму точек &lt;b&gt;%2&lt;/b&gt; и &lt;b&gt;%3&lt;/b&gt;. Это иллюстрирует фундаментальный факт, что порядок  не играет роли при сложении точек на эллиптической кривой. Математики выражают этот факт говоря, что &apos;сложение на эллиптической кривой является ассоциативным&apos;. Ассоциативность является следствием &apos;фундаментальной теоремы Нётера&apos;, названной в честь немецкого математика Макса Нётера (1844-1921).</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="123"/>
        <source>&lt;h4&gt;Auxiliary Line&lt;/h4&gt;&lt;p&gt;The point &lt;b&gt;%1&lt;/b&gt; is constructed as the image of the small yellow point under reflection against the x-axis. This auxiliary line connects the points &lt;b&gt;%1&lt;/b&gt; and the small yellow point.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;Вспомогательная прмая&lt;/h4&gt;&lt;p&gt;Точка &lt;b&gt;%1&lt;/b&gt; конструируется как образ маленькой жёлтой точки при отражении относительно оси x. Эта вспомогательная прямая соединяет точку&lt;b&gt;%1&lt;/b&gt; с маленькой желтой точкой.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="128"/>
        <source>&lt;h4&gt;Auxiliary Point&lt;/h4&gt;&lt;p&gt;This is an auxiliary point used in the construction of the sum &lt;b&gt;%1&lt;/b&gt;. The sum is constructed by looking at the secant line through &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;, The secant line is guaranteed to intersect the elliptic curve in precisely one further point, the point you are looking at right now.&lt;/p&gt;&lt;p&gt;Move the mouse pointer to the point &lt;b&gt;%1&lt;/b&gt; to learn more about the construction.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;Вспомогательная точка&lt;/h4&gt;&lt;p&gt;Это вспомогательная очка используемая при конструкции суммы &lt;b&gt;%1&lt;/b&gt;. Сумма конструируется при помощи секущей через &lt;b&gt;%2&lt;/b&gt; и &lt;b&gt;%3&lt;/b&gt;, Секущая гарантированно пересекает эллиптическую кривую ровно ещё в одной точке, а именно в точке, на которую Вы сейчас смотрите.&lt;/p&gt;&lt;p&gt; Наведите указатель мыши на точку &lt;b&gt;%1&lt;/b&gt; чтобы лучше понять конструкцию.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="138"/>
        <source>Sum of the points &lt;b&gt;%1&lt;/b&gt; and  &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>Сумма точек &lt;b&gt;%1&lt;/b&gt; и &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="140"/>
        <source>Move one of the points &lt;b&gt;%1&lt;/b&gt; or &lt;b&gt;%2&lt;/b&gt; to see how the sum changes. You can also change the shape of the curve to see how the whole construction changes.</source>
        <translation>Двигайте одну из точек &lt;b&gt;%1&lt;/b&gt; или &lt;b&gt;%2&lt;/b&gt; чтобы увидеть, как их сумма меняется. Вы также можете изменить форму кривой чтобы увидеть, как изменяется вся конструкция.</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="145"/>
        <source>&lt;h4&gt;Secant Line&lt;/h4&gt;&lt;p&gt;This line is the secant line through the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;, an auxiliary line used in the construction of the point &lt;b&gt;%1&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;Секущая&lt;/h4&gt;&lt;p&gt;Эта прямая является секущей через точки &lt;b&gt;%2&lt;/b&gt; и &lt;b&gt;%3&lt;/b&gt;, которая является вспомогательной линией, используемой при конструкции точки &lt;b&gt;%1&lt;/b&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="149"/>
        <source>&lt;p&gt;Apart from the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;, the secant line is guaranteed to intersect the elliptic curve in precisely one third point, which is offscreen and therefore not shown in the picture.&lt;/p&gt;&lt;p&gt;The point &lt;b&gt;%1&lt;/b&gt; is constructed as the image of the third intersection under reflection against the x-axis.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Помимо точек &lt;b&gt;%2&lt;/b&gt; и &lt;b&gt;%3&lt;/b&gt;, секущая гарантированно пересекает эллиптическую кривую ещё ровно в одной третей точке, которая находится вне экрана и, поэтому, не показана на рисунке.&lt;/p&gt;&lt;p&gt;Точка &lt;b&gt;%1&lt;/b&gt; строится как образ третьей точки при отражении третьей точки относительно оси x.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="ec_realSumPoint.cpp" line="154"/>
        <source>&lt;p&gt;Apart from the points &lt;b&gt;%2&lt;/b&gt; and &lt;b&gt;%3&lt;/b&gt;, the secant line is guaranteed to intersect the elliptic curve in precisely one third point&lt;/p&gt;&lt;p&gt;Move the mouse pointer to the point &lt;b&gt;%1&lt;/b&gt; to learn more about the construction.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Помимо точек &lt;b&gt;%2&lt;/b&gt; и &lt;b&gt;%3&lt;/b&gt;, секущая гарантированно пересекает эллиптическую кривую ещё ровно в одной третей точке&lt;/p&gt;&lt;p&gt;Наведите указатель мыши на точку &lt;b&gt;%1&lt;/b&gt; чтобы лучше понять конструкцию.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>gfxExportDialog</name>
    <message>
        <location filename="gfxExportDialog.ui" line="14"/>
        <source>Export Options</source>
        <translation>Опции экспорта</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="26"/>
        <source>Bitmap Graphic Options</source>
        <translation>Графические опции Bitmap</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="38"/>
        <source>Width</source>
        <translation>Ширина</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="54"/>
        <location filename="gfxExportDialog.ui" line="92"/>
        <source> Pixel</source>
        <translation>Пиксель</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="76"/>
        <source>Height</source>
        <translation>Высота</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="105"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If checked, the background of the image becomes transparent rather than white.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Если выбрано, то фон рисунка становится прозрачным, а не белым.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="112"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If checked, the background of the image becomes transparent rather than white.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This can be useful if you wish to merge the graphic into an existing picture.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; Если выбрано, то фон рисунка становится прозрачным, а не белым.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Это може оказаться полезнымб если Вы хотите наложить график на существующий рисунок.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="120"/>
        <source>Transparent Background</source>
        <translation>Прозрачный фон</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="130"/>
        <source>Features Shown</source>
        <translation>Отображать элементы</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="136"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Include the light gray coordinate grid when exporting to a graphics file.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Наложить светло серую координатную сеть при экспорте в графический файл.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="143"/>
        <source>Show Grid</source>
        <translation>Отображать сетку</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="153"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Include the dark coordinate axes when exporting to a graphics file.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Включить тёмные координатные оси при экспорте в графический файл.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="156"/>
        <source>Show Axes</source>
        <translation>Показывать оси</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="192"/>
        <source>Preview</source>
        <translation>Предварительый просмотр</translation>
    </message>
    <message>
        <location filename="gfxExportDialog.ui" line="204"/>
        <source>TextLabel</source>
        <translation>Этикетка для текста</translation>
    </message>
</context>
<context>
    <name>infoDialog</name>
    <message>
        <location filename="infoDialog.ui" line="14"/>
        <source>About Elliptic Curves</source>
        <translation>О эллиптических кривых</translation>
    </message>
    <message>
        <location filename="infoDialog.ui" line="24"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;;&quot;&gt;Sketching elliptic curves over the real numbers.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;; font-weight:600;&quot;&gt;Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;DejaVu Sans&apos;;&quot;&gt;Изображение эллиптических кривых над полем вещественных чисел.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;DejaVu Sans&apos;; font-size:4pt;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="infoDialog.ui" line="80"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:14px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Questions and Experiments&lt;/span&gt;&lt;/p&gt;
&lt;ol style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Where is the neutral group element? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Try to use the Elliptic Curve Plotter to illustrate the associativity law. &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;What is the group-theoretic meaning of the inflection points? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choose a=2 and b=-10. You will obtain an elliptic curve without oval, which is rather flat. The inflection points are therefore hard to locate visually. Using the group law, can you devise a way to use the Elliptic Curve Plotter that helps you find the inflection points precisely? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The elliptic curves are real Lie groups. Which one?&lt;/li&gt;&lt;/ol&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Благодарность&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:14px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Вопросы и Эксперименты &lt;/span&gt;&lt;/p&gt;
&lt;ol style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Где нейтральный элемент группы? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Попробуйте использовать Elliptic Curve Plotter для иллюстрации ассоциативного закона. &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Каково значение точки перегиба в смысле теории групп? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choose a=2 and b=-10. Вы получите эллиптическую кривую без овала, которая является довольно плоской. Поэтому точки перегиба сложно визуально локализовать. Можете ли вы, используя групповой закон, разработать способ использования Elliptic Curve Plotter , который поможет Вам найти точки перегиба точно? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Эллиптические кривые являются вещественными группами Ли. Какой именно в данном случае?&lt;/li&gt;&lt;/ol&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="infoDialog.ui" line="100"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Report Problems or Suggestions&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you find a bug in the Elliptic Curve Plotter, or if you have a suggestion for improvement, please contact the author at &lt;span style=&quot; font-weight:600;&quot;&gt;stefan.kebekus@math.uni-freiburg.de&lt;/span&gt;. We appreciate your input!&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you are reporting a bug, please include details about your setup:&lt;/p&gt;
&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;What version of the Elliptic Curve Plotter are you using? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;What platform are you using? Linux? MacOS? Windows? What distribution and what version? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Where did you get the program from?&lt;/li&gt;&lt;/ul&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Also, please remember that it is almost impossible to fix a bug that we cannot reproduce, so please include all information necessary to reproduce the bug. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt; Благодарность &lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Сообщите о Проблемах и Предложениях &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Если Вы найдёте баг в Elliptic Curve Plotter, или, если у Вас есть предложения по улучшению, пожалуйста свяжитесь с автором по &lt;span style=&quot; font-weight:600;&quot;&gt;stefan.kebekus@math.uni-freiburg.de&lt;/span&gt;. Мы благодарны за Ваш вклад!&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Если вы сообщаете о баге, то укажите, пожалуйста, детали Вашей установки:&lt;/p&gt;
&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Какую версию Elliptic Curve Plotter Вы используете? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Какую платформу Вы используете? Linux? MacOS? Windows? Какой дистрибутив и какую версию? &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Откуда у Вас программа?&lt;/li&gt;&lt;/ul&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Также помните, пожалуйста, что почти невозможно устранить баг, который мы не можем воспроизвести, поэтому, пожалуйста, укажите всю необходимую информацию для воспроизведения бага. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="infoDialog.ui" line="121"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt;Acknowledgements&lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:14px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Help to improve the Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Come up with new ideas&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you have suggestion how the Elliptic Curve Plotter could be improved, let us know!&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Help to implement new features&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you have programming skills, you can help to implement new features or streamline the existing program. The Elliptic Curve Plotter is written in the standard C++ programming language and uses the excellent and very well-documented Qt library for its user interface.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Translate the Elliptic Curve Plotter into new languages&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you know a language that the Elliptic Curve Plotter does not yet support, join us to add language support. Programming skills are not required.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;title&gt; Благодарность &lt;/title&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:14px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;Помогите улучшить Elliptic Curve Plotter&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Добро пожаловать с новыми идеями &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Если у Вас есть предложения, как Elliptic Curve Plotter может быть улучшена, дайте нам знать!&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Помогите внедрить новшества&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Если у Вас есть навыки программирования, вы можете помочь внедрить новшества или  or рационализировать существующую программу. Elliptic Curve Plotter написана на стандартном языке программирования C++ и использует отличную и очень хорошо документированную библиотеку Qt для своего интерфейса.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Переведите Elliptic Curve Plotter на новые языки &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Если Вы владеете языком, который Elliptic Curve Plotter ещё не поддерживает, присоединяйтесь к нам, чтобы расширить список поддерживаемых языков. Навыки программирования не требуются.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>mainWindow</name>
    <message>
        <location filename="mainWindow.ui" line="22"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This field shows a sketch of the curve defined over the real numbers by the equation &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Это поле изображает набросок кривой определённой над вещественными числами уравнением &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="36"/>
        <source>Elliptic Curve Graph</source>
        <translation>График эллиптической кривой</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="45"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Curve Equation: &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;%1%2&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Уравнение Кривой: &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;%1%2&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="85"/>
        <source>Equation Parameters</source>
        <translation>Параметры уравнения</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="198"/>
        <source>Curve Description</source>
        <translation>Описание кривой</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="227"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="231"/>
        <source>Export Drawing</source>
        <translation>Экспортировать рисунок</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="245"/>
        <source>&amp;Curves</source>
        <translation>&amp;Кривые</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="256"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="269"/>
        <source>Group Law</source>
        <translation>Групповой закон</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="282"/>
        <source>View</source>
        <translation>Вид</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="286"/>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="305"/>
        <source>toolBar</source>
        <translation>панель инструментов</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="337"/>
        <source>&amp;Quit</source>
        <translation>&amp;Выход</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="340"/>
        <source>Exits the application.</source>
        <translation>Выходит из приложения.</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="343"/>
        <source>Exits the Elliptic Curve Plotter</source>
        <translation>Выходит из Elliptic Curve Plotter</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="351"/>
        <source>Elliptic curve with oval</source>
        <translation>Эллиптическая кривая с овалом</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="354"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choses values for &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; and &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; such that the curve defined by the equation &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; is an elliptic curve that consists of two disjoint components.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Выбирает такие значения для &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; и &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; , чтобы кривая определённая уравнением &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; была эллиптической кривой состоящей из двух непересекающихся компонентов.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="363"/>
        <source>Elliptic curve without oval</source>
        <translation>Эллиптическая кривая без овала</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="366"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choses values for &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; and &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; such that the curve defined by the equation &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; is an elliptic curve that consists of one component only.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Выбирает такие значения для &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; и &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; , чтобы кривая определённая уравнением &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; была эллиптической кривой состоящей только из одного компонента.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="375"/>
        <source>Neil Parabola</source>
        <translation>Парабола Нейла</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="378"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choses values for &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; and &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; such that the curve defined by the equation &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; is a semicubical parabola.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; Выбирает такие значения для &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; и &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; , чтобы кривая определённая уравнением &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; была полукубической параболой.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="387"/>
        <source>Nodal Plane Cubic</source>
        <translation>Нодальная плоская кубика</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="390"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choses values for &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; and &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; such that the curve defined by the equation &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; is a nodel plane cubic.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; Выбирает такие значения для &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; и &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; such &gt; , чтобы кривая определённая уравнением &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; была нодальной плоской кубикой.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="399"/>
        <source>Curve with Isolated Point</source>
        <translation>Кривая без изолированной точки</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="402"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choses values for &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; and &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; such that the curve defined by the equation &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; is a singular elliptic curve that consists of a parabola and an isolated point.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; Выбирает такие значения для &lt;span style=&quot; font-style:italic;&quot;&gt;a&lt;/span&gt; и &lt;span style=&quot; font-style:italic;&quot;&gt;b&lt;/span&gt; , чтобы кривая определённая уравнением &lt;span style=&quot; font-style:italic;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt; = x&lt;/span&gt;&lt;span style=&quot; font-style:italic; vertical-align:super;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;+ax+b&lt;/span&gt; была сингулярной эллиптической кривой состоящей из параболы и изолированной точки.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="414"/>
        <source>About the Elliptic Curve Plotter ...</source>
        <translation>О Elliptic Curve Plotter ...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="417"/>
        <source>Shows information about the Elliptic Curve Plotter and its license.</source>
        <translation>Показать информации о $НАМЕ и её лицензии.</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="429"/>
        <source>What&apos;s This?</source>
        <translation>Что это?</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="432"/>
        <source>What&apos;s This? Click on this item and then on any field to obtain a detailed explanation.</source>
        <translation>Что это? Нажмите сюда, а потом на любое поле для получения подробного объяснения.</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="435"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/whatsthis.png&quot; /&gt;Click on this item and then on any other field or menu item to obtain a more detailed description of the field.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/icons/whatsthis.png&quot; /&gt; Нажмите сюда, а потом на любое поле или пункт меню для получения более подробного объяснения этого поля.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="453"/>
        <source>Compute 2P</source>
        <translation>Вычислить 2P</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="459"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this action to compute the sum of the point &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; with itself according to the group law of the elliptic curve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Используйте это действие чтобы вычислить сумму точки &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; с самой собой согласно групповому закону эллиптической кривой.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="473"/>
        <source>Compute P+Q</source>
        <translation>Вычислить P+Q</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="479"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this action to compute the sum of the points &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; and &lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt; according to the group law of the elliptic curve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Используйте это действие чтобы вычислить сумму точек &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; и &lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt; согласно групповому закону эллиптической кривой.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="490"/>
        <source>Compute -P</source>
        <translation>Вычислить -P</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="496"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this action to compute the inverse of the point &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; according to the group law of the elliptic curve&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Используйте это действие чтобы вычислить обратную точки &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; согласно групповому закону эллиптической кривой &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="510"/>
        <source>Compute Q+R</source>
        <translation>Вычислить Q+R</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="516"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this action to compute the sum of the points &lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt; and &lt;span style=&quot; font-weight:600;&quot;&gt;R &lt;/span&gt;according to the group law of the elliptic curve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Используйте это действие чтобы вычислить сумму точек &lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt; и &lt;span style=&quot; font-weight:600;&quot;&gt;R &lt;/span&gt; согласно групповому закону эллиптической кривой.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="527"/>
        <source>Compute P+(Q+R)</source>
        <translation>Вычислить P+(Q+R)</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="533"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this action to compute the sum of the points &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; and &lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt;+&lt;span style=&quot; font-weight:600;&quot;&gt;R&lt;/span&gt; according to the group law of the elliptic curve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Используйте это действие чтобы вычислить сумму точек &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt; и &lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt;+&lt;span style=&quot; font-weight:600;&quot;&gt;R&lt;/span&gt; согласно групповому закону эллиптической кривой.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="544"/>
        <location filename="mainWindow.ui" line="550"/>
        <source>Compute (P+Q)+R</source>
        <translation>Вычислить (P+Q)+R</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="553"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use this action to compute the sum of the points &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt;+&lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt; and &lt;span style=&quot; font-weight:600;&quot;&gt;R&lt;/span&gt; according to the group law of the elliptic curve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; Используйте это действие чтобы вычислить сумму точек &lt;span style=&quot; font-weight:600;&quot;&gt;P&lt;/span&gt;+&lt;span style=&quot; font-weight:600;&quot;&gt;Q&lt;/span&gt; и &lt;span style=&quot; font-weight:600;&quot;&gt;R&lt;/span&gt; согласно групповому закону эллиптической кривой.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="562"/>
        <source>About Qt ...</source>
        <translation>О Qt ...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="565"/>
        <source>Shows information about the Qt library, a software product that is used internally by the Elliptic Curve Plotter.</source>
        <translation>Показывает информацию о библиотеке Qt - приложении, которое используется внутри Elliptic Curve Plotter.</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="576"/>
        <source>Export to Vector Graphics File...</source>
        <translation>Экспортировать в Файл Векторной Графики...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="579"/>
        <location filename="mainWindow.ui" line="586"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Saves the elliptic curve graph as a Scalable Vector Graphics (SVG) file. SVG files can be edited with specialized programs (such as &lt;span style=&quot; font-weight:600;&quot;&gt;karbon&lt;/span&gt; or &lt;span style=&quot; font-weight:600;&quot;&gt;inkscape&lt;/span&gt;), and are suitable for high quality printouts.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Сохраняет эллиптическую кривую в виде файла Масштабируемой Векторной Графики (SVG = Scalable Vector Graphics). SVG файлы можна редактировать при помошм специализированных программ (таких как &lt;span style=&quot; font-weight:600;&quot;&gt;karbon&lt;/span&gt; или &lt;span style=&quot; font-weight:600;&quot;&gt;inkscape&lt;/span&gt;), и пригодны для печати высокого качества.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="595"/>
        <source>&amp;Print...</source>
        <translation>&amp;Печатать...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="600"/>
        <source>&amp;Open...</source>
        <translation>&amp;Открыть...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="605"/>
        <source>Save &amp;As...</source>
        <translation>Сохранить &amp;Как...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="610"/>
        <source>Report Problem or Suggestion...</source>
        <translation>Сообщить о проблеме или предложении...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="613"/>
        <location filename="mainWindow.ui" line="616"/>
        <source>If you find a bug in the Elliptic Curve Plotter, or if you have a suggestion for future improvements, use this menu entry to contact the author.</source>
        <translation>Если вы найдёте баг в Elliptic Curve Plotter или, если у вас есть предложения по улучшению, используйте пункт меню, чтобы связаться с автором.</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="621"/>
        <location filename="mainWindow.ui" line="648"/>
        <source>How to help...</source>
        <translation>как помочь...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="629"/>
        <source>Export to Raster Graphics File...</source>
        <translation>Экспортировать в виде Сеточного Графического Файла...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="632"/>
        <location filename="mainWindow.ui" line="635"/>
        <source>Saves the elliptic curve graph as a raster graphics file in Portable Network Graphics (PNG) format. PNG files can easily be included in web pages, but have limited printout quality.</source>
        <translation>Сохраняет граф эллиптической кривой как сеточный графический файл в формате PNG. PNG файлы могут быть легко добавлены на веб станицу, но обладают ограниченным качеством при печати.</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="640"/>
        <source>Questions and Experiments...</source>
        <translation>Вопросы и эксперименты...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="643"/>
        <source>Questions and Suggestions for Experiments</source>
        <translation>Вопросы и предложения для экспериментов</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="651"/>
        <source>Use this action to learn how to help improving the Elliptic Curve Plotter</source>
        <translation>Используйте это действие, чтобы узнать, как помочь улучшить Elliptic Curve Plotter</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="659"/>
        <source>Kiosk Mode...</source>
        <translation>Полноэкранный режим...</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="667"/>
        <source>Reset User Interface</source>
        <translation>Сбросить настройки пользовательского интерфейса</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="675"/>
        <source>Use System Default Language</source>
        <translation>Язык Системы по умолчанию</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="678"/>
        <location filename="mainWindow.ui" line="681"/>
        <source>Use the system default language if translations exist. Otherwise, English is used.</source>
        <translation>Используется язык системы по умолчанию, если существует перевод. Иначе используется английский язык.</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="254"/>
        <source>English</source>
        <comment>Translate this to the name of YOUR language, in your language (e.g. &apos;Deutsch&apos;, &apos;Italiano&apos;, &apos;Español&apos;)</comment>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="390"/>
        <source>Curve Equation: %1</source>
        <translation>Уравнение кривой: %1</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="446"/>
        <source>&lt;p&gt;The file name &lt;b&gt;%1&lt;/b&gt; does not end in &lt;b&gt;%2&lt;/b&gt;. This might make it difficult for the file manager and other programs to recognize the file type.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Имя файла &lt;b&gt;%1&lt;/b&gt; не заканчивается в &lt;b&gt;%2&lt;/b&gt;. У файл менеджера и других программ могут возникнуть трудности с распознаванием типа файла.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="449"/>
        <source>&lt;p&gt;Would you like to add the ending to the file name &lt;b&gt;and overwrite the existing file %1&lt;/b&gt;?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Хотели бы Вы добавить окончание к имени файла &lt;b&gt;и переписать существующий файл %1&lt;/b&gt;?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="451"/>
        <source>&lt;p&gt;Would you like to add the ending to the file name?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Would Хотели бы Вы добавить окончание к имени файла?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="479"/>
        <source>Scalable Vector Graphics File (*.svg)</source>
        <translation>Scalable Vector Graphics File (*.svg)</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="491"/>
        <source>Elliptic Curve</source>
        <translation>Эллиптическая Кривая</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="503"/>
        <location filename="mainWindow.cpp" line="535"/>
        <source>File Error</source>
        <translation>Ошибка Файла</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="504"/>
        <location filename="mainWindow.cpp" line="536"/>
        <source>&lt;p&gt;The file %1 could not be written.&lt;/p&gt;&lt;p&gt;The system reported the following error: %2&lt;/p&gt;</source>
        <translation>&lt;p&gt;Файл %1 не мог быть записан.&lt;/p&gt;&lt;p&gt;Система выдала следующую ошибку: %2&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="515"/>
        <source>Portable Network Graphics (*.png)</source>
        <translation>Portable Network Graphics (*.png)</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="546"/>
        <source>Report Problem or Suggestion</source>
        <translation>Сообщить о Проблемах и Пожеланиях</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="558"/>
        <source>How to Help</source>
        <translation>Как помочь</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="570"/>
        <source>Questions and Experiments</source>
        <translation>Вопросы и эксперименты</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="620"/>
        <location filename="mainWindow.cpp" line="635"/>
        <source>Resetting User Interface</source>
        <translation>Сброс настроек пользовательского интерфейса</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="636"/>
        <source>Several Minutes without user interaction.</source>
        <translation>Несколько минут без взаимодействия с пользователем.</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="719"/>
        <source>Elliptic Curve Plotter - Kiosk Mode</source>
        <translation>Elliptic Curve Plotter - Полноэкранный режим</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="719"/>
        <source>Do you really wish to enter the Kiosk Mode?</source>
        <translation>Вы действительно хотите войти в полноэкранный режим?</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="722"/>
        <source>&lt;p&gt;The Kiosk Mode is a restricted mode useful when running the Elliptic Curve Plotter on interactive kiosks that are open to the public.&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Several menu items are disabled, so users cannot exit the program or access the file system.&lt;/li&gt;&lt;li&gt;The program switches to full-screen mode.&lt;/li&gt;&lt;li&gt;The user interface will be reset after three minutes of inactivity.&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;&lt;b&gt;There is no way to leave the Kiosk Mode other than exiting the program.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Hint:&lt;/b&gt; To start the Elliptic Curve Plotter in Kiosk Mode without showing this warning, start the Elliptic Curve Plotter from the command line using the command line option &lt;b&gt;-kiosk&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Полноэкранный режим это ограниченный режим, который полезен при использовании Elliptic Curve Plotter на общественных терминалах.&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Несколько пунктов меню неактивны, поэтому пользователи не могут получить доступ к системному фалу.&lt;/li&gt;&lt;li&gt;Программа переходит в полноэкранный режим.&lt;/li&gt;&lt;li&gt;Интерфейс сбрасывается после трёх минут без активности пользователя.&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;&lt;b&gt;Полноэкранный режим можно покинуть только выйдя из программы.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Совет:&lt;/b&gt; Что бы запустить Elliptic Curve Plotter в Полноэкранном Режиме без этого предупреждения, запустите Elliptic Curve Plotter из командной строки используя опцию &lt;b&gt;-kiosk&lt;/b&gt;.&lt;/p&gt;</translation>
    </message>
</context>
</TS>
