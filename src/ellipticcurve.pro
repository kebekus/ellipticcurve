QT += gui svg widgets
CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code APIsaway from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the  deprecated before Qt 6.0.0

DEFINES += PROJECT_VERSION=\\\"1.1.6\\\"
DEFINES += COPYRIGHTYEAR=\\\"2018\\\"

SOURCES += aboutDialog.cpp animator.cpp ec.cpp ecDescription.cpp ecGraphicsScene.cpp ecPlotter.cpp ec_real.cpp ec_realDoublePoint.cpp ec_realFreePoint.cpp ec_realMinusPoint.cpp ec_realPoint.cpp ec_realSumPoint.cpp gfxExportDialog.cpp inactivityDetector.cpp intervalF.cpp main.cpp mainWindow.cpp

HEADERS += aboutDialog.h animator.h ecDescription.h ecGraphicsScene.h ec.h ecPlotter.h ec_realDoublePoint.h ec_realFreePoint.h ec_real.h ec_realMinusPoint.h ec_realPoint.h ec_realSumPoint.h gfxExportDialog.h inactivityDetector.h intervalF.h main.h mainWindow.h

FORMS += aboutDialog.ui gfxExportDialog.ui infoDialog.ui mainWindow.ui

RESOURCES += ellipticcurve.qrc  ellipticcurve_translations.qrc

TRANSLATIONS += ellipticcurve_de.ts ellipticcurve_ru.ts ellipticcurve_zh_TW.ts
