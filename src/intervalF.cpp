/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QDebug>

#include "intervalF.h"


intervalF::intervalF(double leftBd, double rightBd)
{
  if (leftBd > rightBd) {
    qWarning() << "intervalF::intervalF(double, double) called with left > right";
    setEmpty();
    return;
  }

  empty        = false;
  leftBound    = leftBd;
  rightBound   = rightBd;
}

intervalF::intervalF(const intervalF &other)
    : empty(other.empty)
{
    if (!empty) {
        leftBound = other.leftBound;
        rightBound = other.rightBound;
    }
}

void intervalF::set( double leftBd, double rightBd )
{
  if (leftBd > rightBd) {
    qWarning() << "intervalF::set() called with leftBd > rightBd";
    setEmpty();
    return;
  }

  empty        = false;
  leftBound    = leftBd;
  rightBound   = rightBd;
}


void intervalF::setLeft( double leftBd )
{
  // If the interval is empty, set both the left and the right boundary to be
  // equal to the new left boundary.
  if ( isEmpty() ) {
    empty        = false;
    leftBound    = leftBd;
    rightBound   = leftBd;
    return;
  }

  // Now we know that the interval is not empty.

  // If the right boundary is smaller than the new left boundary, set the
  // inverval to empty.
  if ( right() < leftBd ) {
    setEmpty();
    return;
  }

  // Now we know that the interval is not empty, and that the new left bound
  // is smaller than the existing right bound.
  empty        = false;
  leftBound    = leftBd;
}


double intervalF::left() const
{
  // If the interval is empty => warning
  if (isEmpty()) {
      qWarning() << "intervalF::left() is called on empty interval";
  }

  return leftBound;
}


void intervalF::setRight( double rightBd )
{
  // If the interval is empty, set both the left and the right boundary to be
  // equal to the new left boundary.
  if ( isEmpty() ) {
    empty        = false;
    leftBound    = rightBd;
    rightBound   = rightBd;
    return;
  }

  // Now we know that the interval is not empty.

  // If the left boundary is larger than the new right boundary, set the
  // interval to empty
  if ( left() > rightBd ) {
    setEmpty();
    return;
  }

  // Now we know that the interval is not empty, and that the new left bound
  // is smaller than the existing right bound.
  empty        = false;
  rightBound   = rightBd;
}


double intervalF::right() const
{
  // If the interval is empty => warning
  if (isEmpty()) {
      qWarning() << "intervalF::right() is called on empty interval";
  }

  return rightBound;
}


intervalF intervalF::intersection( const intervalF &other ) const
{
    if (isEmpty() || other.isEmpty()) {
        return {};
    }

    double const l = qMax(left(), other.left());
    double const r = qMin(right(), other.right());

    if (l > r) {
        return {};
    }

    return {l, r};
}


bool intervalF::operator==(const intervalF &other) const
{
    if (isEmpty() != other.isEmpty()) {
        return false;
    }

    if (isEmpty()) {
        return true;
    }

  return (left() == other.left()) && (right() == other.right());
}
