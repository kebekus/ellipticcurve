/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                                  *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QBrush>
#include <QDebug>
#include <QGraphicsEllipseItem>
#include <QPen>
#include <cfloat>

#include "ec_real.h"
#include "ec_realPoint.h"
#include "intervalF.h"

ec_realPoint::ec_realPoint(const QRectF &_boundingBox,
                           QObject* _parentCurve,
                           QGraphicsItem *parentItem,
                           const QString &name,
                           bool _showVertLine,
                           bool _showSecantLine)
    : QObject(_parentCurve),
    QGraphicsItem(parentItem),
    parentCurve(qobject_cast<ec_real*>(_parentCurve))
{
    // Paranoid safety checks
    if (parentCurve == nullptr) {
        qCritical() << "ec_realPoint::ec_realPoint() call with parentCurve == 0";
    }

    state = invalid;
    coords = QPointF(0,0);
    boundingBox = _boundingBox.normalized();
    showVertLine = _showVertLine;
    showSecantLine = _showSecantLine;

    circle = new QGraphicsEllipseItem(-0.15, -0.15, 0.3, 0.3, this);
    circle->setPen(QPen(Qt::black, 0.03));
    circle->setBrush(QBrush(Qt::red));

    label = new QGraphicsSimpleTextItem(circle);
    label->setScale(0.04);

    QPen linePen(Qt::darkRed);
    linePen.setWidthF(0.03);

    secantLine = new QGraphicsLineItem(this);
    secantLine->setLine(QLineF(0.0, 0.0, 1.0, 1.0));
    secantLine->setPen(linePen);
#if QT_VERSION >= 0x040500
    secantLine->setFlags(QGraphicsItem::ItemStacksBehindParent); // make sure the line really stays in the background
#endif
    secantLine->hide();

    vertLine = new QGraphicsLineItem(circle);
    vertLine->setLine(QLineF(0.0, 0.0, 1.0, 1.0));
    vertLine->setPen(linePen);
#if QT_VERSION >= 0x040500
    vertLine->setFlags(QGraphicsItem::ItemStacksBehindParent); // make sure the line really stays in the background
#endif
    vertLine->hide();

    smallCircle = new QGraphicsEllipseItem(circle);
    smallCircle->setRect(-0.1, -0.1, 0.2, 0.2);
    smallCircle->setPen(QPen(Qt::black, 0.03));
    smallCircle->setBrush(QBrush(Qt::yellow));
#if QT_VERSION >= 0x040500
    smallCircle->setFlags(QGraphicsItem::ItemStacksBehindParent); // make sure the circle stays in the background
#endif
    smallCircle->hide();

    connect(parentCurve.data(), &ec_real::changed, this, &ec_realPoint::updateGraphics);
    connect(parentCurve, SIGNAL(changed()), this, SLOT(setToolTip()));
    connect(this, &ec_realPoint::changed, this, &ec_realPoint::updateVisibility);
    connect(this, SIGNAL(changed()), this, SLOT(setToolTip()));

    setInvalid();
    setName(name);
}


QPointF ec_realPoint::getCoords() const
{
    if (getState() != affine) {
        qWarning() << "Internal Error: calling ec_realPoint::getCoords() on point " << getName() << " where state!=affine";
        return {0,0};
    }

    return coords;
}


void ec_realPoint::setInvalid()
{
    // No matter what, always make the point and its label invisible.
    circle->hide();

    // If the point is invalid already, nothing needs to be done.
    if (getState() == invalid) {
        return;
    }

    state = invalid;
    emit changed();
}


QRectF ec_realPoint::boundingRect() const
{
    return circle->boundingRect().translated(coords);
}


void ec_realPoint::setToolTip()
{
    QString const tt = "<h4>" + tr("Point") + " <b>%1</b></h4>" + "<P>" + description() + "</P>";
    circle->setToolTip(tt.arg(getName())); 
}

void ec_realPoint::paint(QPainter * /*painter*/,
                         const QStyleOptionGraphicsItem * /*option*/,
                         QWidget * /*widget*/)
{
}


void ec_realPoint::setName(const QString& name)
{
    label->setText(name);
    positionLabel();
}


QString ec_realPoint::description() const
{
    QString descr;

    switch(getState()) {
    case infinity:
        descr = tr("The point <b>%1</b> is the point at infinity and therefore not shown on the screen.").arg(getName());
        break;
    case affine:
        descr = tr("The coordinates of the point <b>%1</b> are approximately (%2, %3).").arg(getName()).arg(coords.x(),0,'g',3).arg(-coords.y(),0,'g',3);
        if (coords.y() == 0.0) {
            descr
                += " "
                   + tr("The point is <b>%1</b> is a special point of the elliptic curve. "
                        "If you use the group law of the curve to add the point <b>%1</b> to itself, "
                        "the point at infinity is obtained as a result. The inverse of <b>%1</b> is "
                        "the same as <b>%1</b>. "
                        "Mathematicians say that <b>%1</b> is a 2-torsion point of the elliptic curve.")
                         .arg(getName());
        }
        if (isOffscreen(coords)) {
            descr += tr(" Because the coordinates are so large, the point is offscreen and therefore "
                        "not shown.");
        }
        break;
    default:
        break;
    }

    return descr;
}


bool ec_realPoint::setInfinity()
{
    // Paranoid safety checks
    if (parentCurve.isNull()) {
        qCritical() << "ec_realPoint::setInfinity() called when parend has been destructed";
    }

    // If the curve is singular, this method really sets 'invalid'
    if (!parentCurve->isSmooth()) {
        setInvalid();
        return false;
    }

    // If the curve is infinity already, nothing needs to be done.
    if (getState() == infinity) {
        return true;
    }

    state = infinity;
    circle->hide();
    emit changed();

    return true;
}


bool ec_realPoint::setCoords(QPointF pt)
{
    // Paranoid safety checks
    if (parentCurve.isNull()) {
        qCritical() << "ec_realPoint::setInfinity() called when parend has been destructed";
    }

    // If the curve is singular, this method really sets 'invalid'
    if (!parentCurve->isSmooth()) {
        setInvalid();
        return false;
    }

    // If the point has not changed, nothing needs to be done.
    if ((getState() == affine) && (pt == coords)) {
        return true;
    }

    if (!parentCurve->isOnCurve(pt)) {
        setInvalid();
        return false;
    }

    coords = pt;
    state = affine;

    if (isOffscreen(coords)) {
        circle->hide();
    } else {
        // Position the circle and make it visible
        circle->setPos(coords);
        setToolTip();

        // Position the label
        positionLabel();

        circle->show();

        // Experimentation shows that if you add a child to a hidden
        // QGraphicsItem, then the child will be shown, somewhat
        // counter-intuitively. In order to properly hide the child, we briefly
        // show *this, and then hide it again, thereby also hiding the child.
        if (!isVisible()) {
            show();
            hide();
        }
    }

    emit changed();
    return true;
}


void ec_realPoint::positionLabel()
{
    if (getState() != affine) {
        return;
    }

    // Position the label
    QRectF const br = label->mapToScene(label->boundingRect()).boundingRect();
    QPointF pos = -QPointF(br.width()/2, br.height()/2);
    QPointF grad = parentCurve->gradient(coords);
    grad /= ec_real::length(grad);
    grad *= 0.6*ec_real::length(QPointF(br.width(),br.height()));
    pos += grad;
    label->setPos(pos);
}


QLineF ec_realPoint::computeLineSegment(QPointF point, QPointF direction) const
{
    // If direction is nearly zero, set it to a random value to avoid
    // division by zero
    qreal const len = ec_real::length(direction);
    if (qAbs(len) < 0.001) {
        direction.setX(1.0);
        direction.setX(0.0);
    } else {
        // Normalize the direction vector.
        direction /= ec_real::length(direction);
    }

    intervalF intvl1;
    intervalF intvl2;

    if (qAbs(direction.x()) < 0.0001) {
        intvl1.set(-DBL_MAX, DBL_MAX);
    } else {
        double const t1 = (boundingBox.left() - point.x()) / direction.x();
        double const t2 = (boundingBox.right() - point.x()) / direction.x();
        intvl1.set(qMin(t1, t2), qMax(t1, t2));
    }

    if (qAbs(direction.y()) < 0.0001) {
        intvl2.set(-DBL_MAX, DBL_MAX);
    } else {
        double const t1 = (boundingBox.top() - point.y()) / direction.y();
        double const t2 = (boundingBox.bottom() - point.y()) / direction.y();
        intvl2.set(qMin(t1, t2), qMax(t1, t2));
    }

    intervalF const intvl = intvl1.intersection(intvl2);

    if (intvl.isEmpty()) {
        return {};
    }

    return {point+intvl.left()*direction, point+intvl.right()*direction};
}


void ec_realPoint::updateVisibility()
{
    switch(getState()) {
    case affine:
        if (showSecantLine) {
            secantLine->show();
        } else {
            secantLine->hide();
        }

        if (!showVertLine || isOffscreen(getCoords())) {
            // The point is off screen, so we hide most graphical entities
            vertLine->hide();
            smallCircle->hide();
        } else {
            // The point is on screen, so we show most graphical entities
            vertLine->setLine( 0, qMax(-qAbs(getCoords().y())-1, boundingBox.y())-getCoords().y(), 0,
                              qMin(qAbs(getCoords().y())+1, boundingBox.y()+boundingBox.height())-getCoords().y() );
            vertLine->show();
            smallCircle->setPos(0, -2*getCoords().y());
            smallCircle->show();
        }
        break;

    case infinity:
        if (showSecantLine) {
            secantLine->show();
        } else {
            secantLine->hide();
        }

        vertLine->hide();
        smallCircle->hide();
        break;

    case invalid:
        vertLine->hide();
        smallCircle->hide();
        secantLine->hide();
        break;
    }

    // Experimentation shows that if you add a child to a hidden
    // QGraphicsItem, then the child will be shown, somewhat
    // counter-intuitively. In order to properly hide the child, we briefly
    // show *this, and then hide it again, thereby also hiding the child.
    if (!isVisible()) {
        show();
        hide();
    }
}
