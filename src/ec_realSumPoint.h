/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef EC_REALSUMPOINT
#define EC_REALSUMPOINT 1


#include "ec_realPoint.h"

class QGraphicsLineItem;


/**
 * Graphic presentation of the double of a point on an elliptic curve, defined
 * over the real numbers.
 *
 * This class represents a point on a smooth elliptic curve which is defined
 * as the double of a given point. It gives a graphic representation, which
 * includes a conveniently placed label, a nice tooltip and a sketch of the
 * construction.
 * 
 * Visibility and the state of the point is set automatically and cannot be
 * set manually. Whenever the curve changes, the point tries to change along
 * with the curve, if possible. If changing along with the curve is not
 * possible (e.g. when the curve becomes singular, or then the point is on the
 * oval, which vanishes during the change of the curve), the state is set to
 * invalid.
 *
 * @author Stefan Kebekus
 */

class ec_realSumPoint : public ec_realPoint
{
  Q_OBJECT
    
 public:
  /**
   * Default constructor
   *
   * @param boundingBox The point is only shown if it is inside the
   * boundingBox, which is defined here.
   *
   * @param parentCurve The curve to which this point belongs. It is possible
   * to delete the curve while this point exists, but the point will become
   * useless after that.
   * 
   * @param point1 The first point that is added. It is possible to delete the
   * point1 while this point exists, but the point will become useless after
   * that.
   *
   * @param point2 The second point that is added. It is possible to delete
   * the point1 while this point exists, but the point will become useless
   * after that.
   *
   * @param parentItem This pointer is passed on to the constructor of the
   * QGraphicsItem.
   */
     ec_realSumPoint(const QRectF &boundingBox,
                     ec_real *parentCurve,
                     ec_realPoint *point1,
                     ec_realPoint *point2,
                     QGraphicsItem *parentItem);

     /**
   * Re-implemented from ec_realPoint
   */
     QString description() const override;

     /**
   * Special description methode which explain associatiyity
   *
   * This special description function makes sense only for the second point in
   * the associativity construction. The explanation produces a text "The point
   * ... can also be constructed as the sum of .... The order of addition does
   * not matter...".
   */
     QString descriptionForAssociativityConstruction() const;

     /**
   * Find out if this point is currently computable or not
   */
     bool isComputable() const;


 public slots:
  /**
   * Specify if the double of the parent point should be computed
   *
   * The double of the parent point is computed only if setCompute(true) has
   * been called. Otherwise, all entities of this point are hidden, and the
   * point is set to invalid.
   *
   * @param doCompute specify if computation should be performed
   */
  void setCompute(bool doCompute);


 protected:
  /**
   * Re-implemented from ec_realPoint
   */
  void setToolTip() override;


 signals:
  /**
   * This signal is emitted when the point becomes (un)compuable. 
   * 
   * Depending on the parant point, it might or might not be possible to
   * compute the double of the parent ---in essence, the double is computable
   * if the parent point is a valid point of an elliptic curve.
   */
  void computabilityChanged();


 protected slots:
  /**
   * Modify the point whenever the parent points change
   *
   * This slot is used to monitor the parent point, in order to adjust the poin't
   * position and state whenever the curve changes.
   */
  void updateGraphics() override;


 private:
  /**
   * Pointer to the first parent point of this sum
   */
  QPointer<ec_realPoint> parentPoint1;

  /**
   * Pointer to the second parent point of this sum
   */
  QPointer<ec_realPoint> parentPoint2;

  /**
   * Specifies if the inverse should be computed or not
   */
  bool compute;
};

#endif
