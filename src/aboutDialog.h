/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "ui_aboutDialog.h"


/**
 * About Dialog
 *
 * This is a trivial about dialog which is entirely constructed in the Qt
 * designer and has no other functionality than to show a picture of the
 * programmer, display a bit of text and show the license agreements.
 */

class aboutDialog : public QDialog
{
  Q_OBJECT

public:
  /**
   * Standard constructor for QDialogs
   * 
   * @param parent The parent widget, passed on to the constructor of the
   * QDialog
   */
  aboutDialog(QWidget *parent = nullptr);

  // No copy constructor
  aboutDialog(aboutDialog const&) = delete;

  // No assign operator
  aboutDialog& operator =(aboutDialog const&) = delete;

  // No move constructor
  aboutDialog(aboutDialog&&) = delete;

  // No move assignment operator
  aboutDialog& operator=(aboutDialog&&) = delete;

  /**
   * Standard destructor
   */
  ~aboutDialog() override;

 private:
  Ui::aboutDialog ui{};
};
