/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QDebug>
#include <QGraphicsSceneMouseEvent>
#include <cmath>

#include "ecGraphicsScene.h"
#include "ec_realFreePoint.h"


ecGraphicsScene::ecGraphicsScene(QObject *parent)
  : QGraphicsScene(parent)
{}

void ecGraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
  QGraphicsScene::mousePressEvent(mouseEvent);

  if (mouseEvent->isAccepted()) {
      return;
  }

  QList<QGraphicsItem *> const allItems = items();
  QList<ec_realFreePoint *> points;
  for (const auto &allItem : allItems) {
      if (allItem->type() == QGraphicsItem::UserType + 1) {
          points << dynamic_cast<ec_realFreePoint *>(allItem);
      }
  }

  if (points.isEmpty()) {
      return;
  }

  ec_realFreePoint *closestPt = nullptr;
  double closestDist = -1.0;

  QPointF const mc = mouseEvent->scenePos();
  for(auto & point : points) {
    QPointF pc;
    if (point->getState() == ec_realPoint::infinity) {
        continue;
    }
    if (point->getState() == ec_realPoint::invalid) {
        pc = point->getHomePos();
    }
    if (point->getState() == ec_realPoint::affine) {
        pc = point->getCoords();
    }
    double const dist = sqrt((mc.x() - pc.x()) * (mc.x() - pc.x())
                             + (mc.y() - pc.y()) * (mc.y() - pc.y()));

    if ((closestDist < 0.0)||(closestDist > dist)) {
      closestDist = dist;
      closestPt = point;
    }
  }

  if ((closestPt == nullptr) || (closestDist > 1.0)) {
      return;
  }
  grabber = closestPt;

}


void ecGraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent * mouseEvent)
{
  QGraphicsScene::mouseMoveEvent(mouseEvent);

  if (mouseEvent->isAccepted()) {
      return;
  }

  if (grabber != nullptr) {
      grabber->mouseMoveEvent(mouseEvent);
  }
}


void ecGraphicsScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
  QGraphicsScene::mouseReleaseEvent(mouseEvent);
  grabber = nullptr;
}
