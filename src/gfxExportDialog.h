/***************************************************************************
 *   Copyright (C) 2012 by Stefan Kebekus                                  *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GFXEXPORTDIALOG
#define GFXEXPORTDIALOG

#include <QPointer>

#include "ui_gfxExportDialog.h"

class ecPlotter;
class QResizeEvent;


/**
 * Dialog for the 'export to graphics' feature
 *
 * This dialog is shown before exporting the picture of the elliptic curve to a
 * graphics file. Uses can choose features to be shown in the
 * picture. Optionally, a bitmap size can be chosen.
 */

class gfxExportDialog : public QDialog
{
  Q_OBJECT

public:
  /**
   * Standard constructor for QDialogs
   * 
   * @param plotter Pointer to the plotter which can be used to draw the
   * elliptic curve. This plotter must not be deleted while the gfxExportDialog
   * exists.
   *
   * @param bitmap If true, input fields are shown that allow the user to choose
   * the bitmap size.
   *
   * @param parent The parent widget, passed on to the constructor of the
   * QDialog
   */
  gfxExportDialog(ecPlotter *plotter, bool bitmap, QWidget *parent = nullptr);

    // No copy constructor
  gfxExportDialog(gfxExportDialog const&) = delete;

  // No assign operator
  gfxExportDialog& operator =(gfxExportDialog const&) = delete;

  // No move constructor
  gfxExportDialog(gfxExportDialog&&) = delete;

  // No move assignment operator
  gfxExportDialog& operator=(gfxExportDialog&&) = delete;

  /**
   * Standard destructor
   */
  ~gfxExportDialog() override;

  /**
   * Returns true if the user wants the coordinate axes to be included in the picture 
   */
  bool showAxes() const {return ui.axesBox->isChecked();}

  /**
   * Returns true if the user wants a coordinate grid to be included in the picture 
   */
  bool showGrid() const {return ui.gridBox->isChecked();}

  /**
   * Transparent graphics background
   * 
   * If 'bitmap' has been set to 'true' in the constructor, the user has the
   * option to choose a transparent (as opposed to opaque-white)
   * background. This method returns true if the user wants 'transparent'. If
   * 'bitmap' has not been set to 'true', the result is undefined.
   */
  bool transparent() const {return ui.transparentBox->isChecked();}

  /**
   * Bitmap width
   * 
   * If 'bitmap' has been set to 'true' in the constructor, the user has the
   * option to choose the bitmap size. This method returns the bitmap width in
   * pixels. If 'bitmap' has not been set to 'true', the result is undefined.
   */
  int width() const {return ui.widthBox->value();}

  /**
   * Bitmap height
   * 
   * If 'bitmap' has been set to 'true' in the constructor, the user has the
   * option to choose the bitmap size. This method returns the bitmap height in
   * pixels. If 'bitmap' has not been set to 'true', the result is undefined.
   */
  int height() const {return ui.heightBox->value();}

 protected:
   /**
    * Re-implemented from QWidget
    *
    * This method is re-implemented, in order to adjust the view whenever the
    * size of the widget changes
    */
   void resizeEvent(QResizeEvent *event) override;

 private slots:
  void updatePreview();
  void widthChanged();
  void heightChanged();

 private:
  double ratio;
  QPointer<ecPlotter> plotter;

  Ui::gfxExportDialog ui{};
};

#endif
