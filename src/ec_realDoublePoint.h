/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef EC_REALDOUBLEPOINT
#define EC_REALDOUBLEPOINT 1


#include "ec_realPoint.h"

class QGraphicsLineItem;


/**
 * Graphic presentation of the double of a point on an elliptic curve, defined
 * over the real numbers.
 *
 * This class represents the double of a given parent point on a smooth
 * elliptic curve. It gives a graphic representation, which includes a nice
 * tooltip and a sketch of the construction.
 * 
 * Whenever the curve and the parent point change, the point tries to change
 * along with the curve, if possible. If this is not possible (e.g. when the
 * curve becomes singular, or then the point is on the oval, which vanishes
 * during the change of the curve), the state is set to invalid.
 *
 * @author Stefan Kebekus
 */

class ec_realDoublePoint : public ec_realPoint
{
  Q_OBJECT
    
 public:
  /**
   * Default constructor of the double of a given parent point
   *
   * @param boundingBox The point is only shown if it is inside the
   * boundingBox, which is defined here. This argument is passed on to the
   * constructor of the ec_realPoint class.
   *
   * @param parentCurve This object represents a point on the curve
   * "parentCurve". Changes of the parentCurve are monitored, and coordinates,
   * state and visibility of the point may change when the parentCurve
   * changes. The point is not deleted automatically when the parentCurve is
   * deleted, but becomes useless once the parentCurve is gone.
   *
   * @param point The point whose double is to be computed. Changes of the
   * parentPoint are monitored, and coordinates, state and visibility of the
   * point may change when the parentCurve changes. The point is not deleted
   * automatically when the parentPoint is deleted, but becomes useless once
   * the parentCurve is gone.
   *
   * @param parentItem The parentItem is passed on to the default constructor
   * of the QGraphicsItem class, which this class inherits.
   */
     ec_realDoublePoint(const QRectF &boundingBox,
                        ec_real *parentCurve,
                        ec_realPoint *point,
                        QGraphicsItem *parentItem);

     /**
   * Re-implemented from ec_realPoint
   */
     QString description() const override;

     /**
   * Find out if this point is currently computable or not
   */
     bool isComputable() const;


 public slots:
  /**
   * Specify if the double of the parent point should be computed
   *
   * The double of the parent point is computed only if setCompute(true) has
   * been called. Otherwise, all entities of this point are hidden, and the
   * point is set to invalid.
   *
   * @param doCompute specify if computation should be performed
   */
  void setCompute(bool doCompute);

 signals:
  /**
   * This signal is emitted when the point becomes (un)compuable. 
   * 
   * Depending on the parant point, it might or might not be possible to
   * compute the double of the parent ---in essence, the double is computable
   * if the parent point is a valid point of an elliptic curve.
   */
  void computabilityChanged();


 protected:
  /**
   * Re-implemented from ec_realPoint
   */
  void setToolTip() override;


 protected slots:
  /**
   * Update the graphics
   *
   * This slot is used to monitor the parent point, in order to adjust the poin't
   * position and state whenever the curve changes.
   */
  void updateGraphics() override;

 private:
  /**
   * Pointer to the parent point of this inverse
   */ 
  QPointer<ec_realPoint> parentPoint;

  /**
   * Specifies if the inverse should be computed or not
   */
  bool               compute;
};

#endif
