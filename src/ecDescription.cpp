/***************************************************************************
 *   Copyright (C) 2018-2024 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QDebug>

#include "ecDescription.h"
#include "ec_real.h"
#include "ec_realDoublePoint.h"
#include "ec_realFreePoint.h"
#include "ec_realMinusPoint.h"
#include "ec_realSumPoint.h"

using namespace std::chrono_literals;


ecDescription::ecDescription(QWidget * parent)
  : QTextBrowser(parent)
{
    timer.setSingleShot(true);
    timer.setInterval(100ms);
    connect(&timer, &QTimer::timeout, this, &ecDescription::updateDescription);
}


void ecDescription::setCurve(ec_real * _parentCurve)
{
  // Paranoid safety checks
  if (_parentCurve == nullptr) {
      qCritical() << "ecDescription::setCurve called with parentCurve == 0";
  }
  if (!parentCurve.isNull()) {
      qCritical() << "ecDescription::setCurve called when parentCurve was already set";
  }

  parentCurve = _parentCurve;

  connect(parentCurve, SIGNAL(changed()), &timer, SLOT(start()));
  timer.start(); // Update the display after a slight delay
}


void ecDescription::addPoint(ec_realPoint * pt)
{
  points.append(pt);
  connect(pt, SIGNAL(changed()), &timer, SLOT(start()));
  timer.start(); // Update the display after a slight delay
}


void ecDescription::updateDescription()
{
  // Paranoid safety check
  if (parentCurve.isNull()) {
    clear();
    return;
  }
  
  // Start with description of the curve
  setText(parentCurve->description());

  // Check if there are any free points to be described. If there are
  // none, add a suggestion what the user can possibly do next.
  QList<ec_realPoint *> pointList;
  for (const auto& point : std::as_const(points)) {
    ec_realPoint *pt =  qobject_cast<ec_realFreePoint *>(point);
    if (pt == nullptr) {
        continue;
    }
    if (pt->getState() == ec_realPoint::invalid) {
        continue;
    }
    pointList << pt;
  }
  if (!pointList.isEmpty()) {
    append("<h4>" + tr("Points on the elliptic curve") + "</h4>");
    for (auto &i : pointList) {
        append(i->description());
    }
  } else {
    append("<h4>" + tr("Experiment with the curve and its group law!") + "</h4>");
    if (parentCurve->Delta() == 0) {
        append("<p>" + tr("Use the sliders to change the shape of the curve.") + "</p>");
    } else {
        append("<p>"
               + tr("Use the sliders to change the shape of the curve, or drag some points to the "
                    "curve and use the menu 'Group Law' to experiment with the group law.")
               + "</p>");
    }
  }
  pointList.clear();

  // Check if there is a negative of a point to be described
  for (const auto& point : std::as_const(points)) {
    ec_realPoint *pt =  qobject_cast<ec_realMinusPoint *>(point);
    if (pt == nullptr) {
        continue;
    }
    if (pt->getState() == ec_realPoint::invalid) {
        continue;
    }

    append("<h4>" + tr("The inverse of the point <b>P</b>") + "</h4>");
    append(pt->description());
    break;
  }

  // Check if there is a negative free point to be described
  for (const auto& point : std::as_const(points)) {
    ec_realPoint *pt =  qobject_cast<ec_realDoublePoint *>(point);
    if (pt == nullptr) {
        continue;
    }
    if (pt->getState() == ec_realPoint::invalid) {
        continue;
    }

    append("<h4>" + tr("The double of the point <b>P</b>") + "</h4>");
    append(pt->description());
    break;
  }

  // Check if there are any sums of two points to be described
  for (const auto& point : std::as_const(points)) {
    ec_realPoint *pt =  qobject_cast<ec_realSumPoint *>(point);
    if (pt == nullptr) {
        continue;
    }
    if (pt->getState() == ec_realPoint::invalid) {
        continue;
    }
    if (pt->getName().count(QStringLiteral("+")) > 1) {
        continue;
    }
    pointList << pt;
  }
  if (!pointList.isEmpty()) {
    append("<h4>" + tr("Sums of points") + "</h4>");
    for (auto &i : pointList) {
        append(i->description());
    }
  }
  pointList.clear();

  // Check if there are any sums of more than two points to be described
  QList<ec_realSumPoint *> sumPointList;
  for (const auto& point : std::as_const(points)) {
    auto *pt =  qobject_cast<ec_realSumPoint *>(point);
    if (pt == nullptr) {
        continue;
    }
    if (pt->getState() == ec_realPoint::invalid) {
        continue;
    }
    if (pt->getName().count(QStringLiteral("+")) < 2) {
        continue;
    }
    sumPointList << pt;
  }
  if (!sumPointList.isEmpty()) {
    append("<h4>" + tr("Sums of three points") + "</h4>");

    append(sumPointList[0]->description());
    if (sumPointList.size() > 1) {
        append(sumPointList[1]->descriptionForAssociativityConstruction());
    }
  }
}
