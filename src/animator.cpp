/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QDebug>

#include "animator.h"
#include "ec.h"


animator::animator(ec *curve, QObject *parent)
    : QObject(parent)
{
    if (curve == nullptr) {
        qCritical() << "Internal programming error: animator::animator() has been called with illegal arguments";
        return;
    }

    animationStartA = curve->a();
    animationStartB = curve->b();
    intermediateA   = curve->a();
    intermediateB   = curve->b();
    animationEndA   = curve->a();
    animationEndB   = curve->b();

    timeLine.setEasingCurve(QEasingCurve::Type::Linear);
    timeLine.setUpdateInterval(10);

    connect(curve, &ec::changed, this, &animator::intCurveChanged);
    connect(&timeLine, &QTimeLine::valueChanged, this, &animator::animate);
}


void animator::intCurveChanged(ec *curve)
{
    if (curve == nullptr) {
        qCritical() << "Internal programming error: animator::intCurvechanged() has been called with illegal arguments";
        return;
    }

    timeLine.stop();

    // If the curve changes, and the target values of a and b are precisely what
    // we have now, stop the animation, but emit the signal changed at least
    // once. This typically happens if the curve is not changed at all, but if
    // curve->touch() has been called.
    if ((intermediateA == curve->a()) && (intermediateB == curve->b())) {
        emit changed(intermediateA, intermediateB);
        return;
    }

    animationStartA = intermediateA;
    animationStartB = intermediateB;
    animationEndA = curve->a();
    animationEndB = curve->b();

    timeLine.setDuration(200);
    timeLine.setCurrentTime(0);
    timeLine.start();
}


void animator::animate(qreal value)
{
    //  value = qBound(0.0, value, 1.0); // Reformulated because Android does not understand qBound
    if (value < 0.0) {
        value = 0.0;
    }
    if (value > 1.0) {
        value = 1.0;
    }

    intermediateA = (1.0-value)*animationStartA + value*animationEndA;
    if (qAbs(intermediateA - animationEndA) < 0.001) {
        intermediateA = animationEndA;
    }
    intermediateB = (1.0-value)*animationStartB + value*animationEndB;
    if (qAbs(intermediateB - animationEndB) < 0.001) {
        intermediateB = animationEndB;
    }

    emit changed(intermediateA, intermediateB);
}
