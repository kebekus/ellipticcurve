/***************************************************************************
 *   Copyright (C) 2018-2024 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QCloseEvent>
#include <QFileDialog>
#include <QImage>
#include <QMessageBox>
#include <QSettings>
#include <QSvgGenerator>
#include <QTranslator>
#include <QWhatsThis>

#include "aboutDialog.h"
#include "animator.h"
#include "ec_realDoublePoint.h"
#include "ec_realFreePoint.h"
#include "ec_realMinusPoint.h"
#include "ec_realSumPoint.h"
#include "gfxExportDialog.h"
#include "inactivityDetector.h"
#include "mainWindow.h"
#include "ui_infoDialog.h"

using namespace Qt;
using namespace std::chrono_literals;


mainWindow::mainWindow(bool kiosk, QWidget *parent)
    : QMainWindow(parent)
    , intCurve(new ec(this))
    , languageActionGroup(this)
{
    QSettings const settings;
    // Setup translations
    qApp->installTranslator(&appTranslator);
    qApp->installTranslator(&QtTranslator);
    QString const l = settings.value("mainWindow/language", "system").toString();
    setupTranslators(l);

    // Setup GUI
    ui.setupUi(this);
    ui.actionQuit->setShortcut(QKeySequence::Quit);
    ui.actionWhat_s_This->setShortcut(QKeySequence::WhatsThis);
    makeLanguageMenu();
    // Under Android, disable a few actions which do not make sense on these
    // devices.
#ifdef ANDROID
    ui.menuExport->setEnabled(false);
    ui.actionKioskMode->setEnabled(false);
#endif
#ifdef  __EMSCRIPTEN__
    ui.menuExport->setEnabled(false);
    ui.actionQuit->setEnabled(false);
    ui.actionKioskMode->setEnabled(false);
#endif

    // Restore size of main window
    restoreGeometry(settings.value("mainWindow/geometry").toByteArray());
    restoreState(settings.value("mainWindow/windowState").toByteArray());
    // Initialize and restore splitter
    ui.splitter->setCollapsible(0, false);
    ui.splitter->setCollapsible(1, true);
    ui.splitter->restoreState(settings.value("splitterSizes").toByteArray());
    connect(ui.splitter, &QSplitter::splitterMoved, this, &mainWindow::updateActions);
    // Initialize spliders
    int const a = qBound(-10, settings.value("ec/a", -6).toInt(), 10);
    int const b = qBound(-10, settings.value("ec/b",  6).toInt(), 10);
    ui.aSlider->setValue( a );
    ui.bSlider->setValue( b );

    // Set minimum width in text fields, to avoid flickering and layout changes
    // when sliders are moved.
    ui.aLabel_2->setMinimumWidth( ui.aLabel_2->fontMetrics().horizontalAdvance(u"-10"_s));
    ui.aLabel_2->setNum( a );
    ui.bLabel_2->setMinimumWidth( ui.bLabel_2->fontMetrics().horizontalAdvance(u"-10"_s));
    ui.bLabel_2->setNum( b );
    ui.equation->setMinimumWidth( ui.equation->fontMetrics().horizontalAdvance(u"Curve equation: y^ = x^ + 10x + 10M"_s) );

    // Construct an integral curve and connect the sliders with it

    intCurve->setA(a);
    intCurve->setB(b);
    connect(ui.aSlider, &QAbstractSlider::valueChanged, intCurve, &ec::setA);
    connect(ui.bSlider, &QAbstractSlider::valueChanged, intCurve, &ec::setB);
    intCurve->touch(); // necessary to get the curve drawn

    // Construct the animator
    auto *an = new animator(intCurve, this);

    // Draw the coordinate grid
    QRectF const bbox(-4, -10, 10, 20);
    ui.plotter->drawGrid(bbox);

    // Construct a real curve and connect the animator with it.
    realCurve = new ec_real(bbox, this);
    realCurve->setAB(a,b);
    ui.description->setCurve(realCurve);
    ui.plotter->setCurve(realCurve);
    connect(an, &animator::changed, realCurve, &ec_real::setAB);
    connect(realCurve, &ec_real::changed, this, &mainWindow::curveChanged);
    curveChanged();

    // Construct points
    P = new ec_realFreePoint(bbox, realCurve, QPointF(8,-8.7), nullptr, u"P"_s);
    P->setZValue(2);
    ui.description->addPoint(P);
    ui.plotter->addFreePoint(P); // The plotter now owns the point
    if (settings.value("points/P_affine", false).toBool()) {
        P->setCoords( settings.value("points/P", QPointF(0,0)).toPointF() );
    }

    twoP = new ec_realDoublePoint(bbox, realCurve, P, nullptr);
    twoP->setZValue(1);
    ui.description->addPoint(twoP);
    ui.plotter->addPoint(twoP); // The plotter now owns the point
    connect(twoP.data(), &ec_realDoublePoint::computabilityChanged, this, &mainWindow::updateActions);
    connect(ui.actionCompute_2P, &QAction::toggled, twoP.data(), &ec_realDoublePoint::setCompute);

    minusP = new ec_realMinusPoint(bbox, realCurve, P, nullptr);
    minusP->setZValue(1);
    ui.description->addPoint(minusP);
    ui.plotter->addPoint(minusP); // The plotter now owns the point
    connect(minusP.data(), &ec_realMinusPoint::computabilityChanged, this, &mainWindow::updateActions);
    connect(ui.actionCompute_minusP, &QAction::toggled, minusP.data(), &ec_realMinusPoint::setCompute);

    Q = new ec_realFreePoint(bbox, realCurve,  QPointF(8,-7.7), nullptr, u"Q"_s);
    Q->setZValue(2);
    ui.description->addPoint(Q);
    ui.plotter->addFreePoint(Q);  // The plotter now owns the point
    if (settings.value("points/Q_affine", false).toBool()) {
        Q->setCoords( settings.value("points/Q", QPointF(0,0)).toPointF() );
    }

    PPlusQ = new ec_realSumPoint(bbox, realCurve, P, Q, nullptr);
    PPlusQ->setZValue(1);
    ui.description->addPoint(PPlusQ);
    ui.plotter->addPoint(PPlusQ); // The plotter now owns the point
    connect(PPlusQ.data(), &ec_realSumPoint::computabilityChanged, this, &mainWindow::updateActions);
    connect(ui.actionCompute_PPlusQ, &QAction::toggled, PPlusQ.data(), &ec_realSumPoint::setCompute);

    R = new ec_realFreePoint(bbox, realCurve,  QPointF(8,-6.7), nullptr, u"R"_s);
    R->setZValue(2);
    ui.description->addPoint(R);
    ui.plotter->addFreePoint(R); // The plotter now owns the point
    if (settings.value("points/R_affine", false).toBool()) {
        R->setCoords( settings.value("points/R", QPointF(0,0)).toPointF() );
    }

    QPlusR = new ec_realSumPoint(bbox, realCurve, Q, R, nullptr);
    QPlusR->setZValue(1);
    ui.description->addPoint(QPlusR);
    ui.plotter->addPoint(QPlusR); // The plotter now owns the point
    connect(QPlusR.data(), &ec_realSumPoint::computabilityChanged, this, &mainWindow::updateActions);
    connect(ui.actionCompute_QPlusR, &QAction::toggled, QPlusR.data(), &ec_realSumPoint::setCompute);

    // (P+Q)+R
    PPlusQPlusR1 = new ec_realSumPoint(bbox, realCurve, PPlusQ, R, nullptr);
    PPlusQPlusR1->setZValue(1);
    ui.description->addPoint(PPlusQPlusR1);
    ui.plotter->addPoint(PPlusQPlusR1); // The plotter now owns the point
    connect(PPlusQPlusR1.data(), &ec_realSumPoint::computabilityChanged, this, &mainWindow::updateActions);
    connect(ui.actionCompute_PPlusQPlusR_2, &QAction::toggled, PPlusQPlusR1.data(), &ec_realSumPoint::setCompute);

    // P+(Q+R)
    PPlusQPlusR2 = new ec_realSumPoint(bbox, realCurve, P, QPlusR, nullptr);
    PPlusQPlusR2->setZValue(1);
    ui.description->addPoint(PPlusQPlusR2);
    ui.plotter->addPoint(PPlusQPlusR2); // The plotter now owns the point
    connect(PPlusQPlusR2.data(), &ec_realSumPoint::computabilityChanged, this, &mainWindow::updateActions);
    connect(ui.actionCompute_PPlusQPlusR_1, &QAction::toggled, PPlusQPlusR2.data(), &ec_realSumPoint::setCompute);

    connect(ui.actionReset, &QAction::triggered, this, &mainWindow::resetGUIFeedback);
    connect(ui.actionKioskMode, &QAction::triggered, this, &mainWindow::enterKioskModeFeedBack);

    connect(ui.actionAbout_Qt, &QAction::triggered, qApp, &QApplication::aboutQt);

    connect(ui.actionElliptic_curve_without_oval, &QAction::triggered, this, &mainWindow::setRealTypeEllipticWithoutOval);
    connect(ui.actionElliptic_curve_with_oval, &QAction::triggered, this, &mainWindow::setRealTypeEllipticWithOval);
    connect(ui.actionNeil_Parabola, &QAction::triggered, this, &mainWindow::setRealTypeNeilParabola);
    connect(ui.actionNodal_Plane_Cubic, &QAction::triggered, this, &mainWindow::setRealTypeNodalCurve);
    connect(ui.actionCurve_with_Isolated_Point, &QAction::triggered, this, &mainWindow::setRealTypeIsolatedPt);
    connect(ui.actionAbout, &QAction::triggered, this, &mainWindow::showAboutDialog);
    connect(ui.actionWhat_s_This, &QAction::triggered, this, &mainWindow::enterWhatsThis);

    //
    // Wire actions
    //
    connect(ui.actionExportSVG, &QAction::triggered, this, &mainWindow::exportSVG);
    connect(ui.actionExportPNG, &QAction::triggered, this, &mainWindow::exportPNG);
    connect(ui.actionProblem, &QAction::triggered, this, &mainWindow::report);
    connect(ui.actionHelp, &QAction::triggered, this, &mainWindow::help);
    connect(ui.actionExperiment, &QAction::triggered, this, &mainWindow::experiment);

    //
    // Restore point computations
    updateActions();
    if (ui.actionCompute_minusP->isEnabled()) {
        ui.actionCompute_minusP->setChecked(settings.value("actions/minusP", false).toBool());
    }
    if (ui.actionCompute_2P->isEnabled()) {
        ui.actionCompute_2P->setChecked(settings.value("actions/2P", false).toBool());
    }
    if (ui.actionCompute_PPlusQ->isEnabled()) {
        ui.actionCompute_PPlusQ->setChecked(settings.value("actions/PPlusQ", false).toBool());
    }
    if (ui.actionCompute_QPlusR->isEnabled()) {
        ui.actionCompute_QPlusR->setChecked(settings.value("actions/QPlusR", false).toBool());
    }
    if (ui.actionCompute_PPlusQPlusR_1->isEnabled()) {
        ui.actionCompute_PPlusQPlusR_1->setChecked(settings.value("actions/PPlusQPlusR_1", false).toBool());
    }
    if (ui.actionCompute_PPlusQPlusR_2->isEnabled()) {
        ui.actionCompute_PPlusQPlusR_2->setChecked(settings.value("actions/PPlusQPlusR_2", false).toBool());
    }

    //
    // Enter Kiosk mode
    //
#ifndef __EMSCRIPTEN__
    if (kiosk) {
        enterKioskMode();
    }
#endif //  __EMSCRIPTEN__
}

mainWindow::~mainWindow() = default;


void mainWindow::makeLanguageMenu()
{
    ui.actionSystem_Language->setData("system");

    languageActionGroup.addAction(ui.actionSystem_Language);
    connect(&languageActionGroup, &QActionGroup::triggered, this, &mainWindow::onSwitchLanguageAction);

    // Go through the list of all translation files
    QDir const qmDir(u":"_s);
    QStringList fileNames = qmDir.entryList(QStringList(u"ellipticcurve_*.qm"_s));
    // Add fake entry for 'english
    fileNames << u"ellipticcurve_en.qm"_s;
    fileNames.sort();

    for (const auto &fileName : fileNames) {
        // Find short name of the locale (e.g. 'de', 'en')
        QString localeName = fileName;
        localeName.remove(0, localeName.indexOf('_') + 1);
        localeName.chop(3);

        // Find natural language name for the locale
        QString languageName;
        if (localeName == u"en"_s) {
            languageName = u"English"_s;
        } else {
            QTranslator translator;
            (void) translator.load(fileName, qmDir.absolutePath());
            languageName = translator.translate("mainWindow", "English", "Translate this to the name of YOUR language, in your language (e.g. 'Deutsch', 'Italiano', 'Español')");
        }

        if (!languageName.isEmpty()) {
            auto *action = new QAction( QString(u"%1 - %2"_s).arg(localeName.toUpper(), languageName), this);
            action->setCheckable(true);
            action->setData(localeName);
            languageActionGroup.addAction(action);
            ui.menuLanguage->addAction(action);
        }
    }
}

void mainWindow::showAboutDialog()
{
    aboutDialog dlg(this);
    dlg.exec();
}

void mainWindow::enterWhatsThis()
{
    QWhatsThis::enterWhatsThisMode();
}

void mainWindow::setRealTypeNeilParabola()
{
    intCurve->setRealTypeNeilParabola();
    ui.aSlider->setValue( intCurve->a() );
    ui.bSlider->setValue( intCurve->b() );
}

void mainWindow::setRealTypeNodalCurve()
{
    intCurve->setRealTypeNodalCurve();
    ui.aSlider->setValue( intCurve->a() );
    ui.bSlider->setValue( intCurve->b() );
}

void mainWindow::setRealTypeIsolatedPt()
{
    intCurve->setRealTypeIsolatedPt();
    ui.aSlider->setValue( intCurve->a() );
    ui.bSlider->setValue( intCurve->b() );
}

void mainWindow::setRealTypeEllipticWithoutOval()
{
    intCurve->setRealTypeEllipticWithoutOval();
    ui.aSlider->setValue( intCurve->a() );
    ui.bSlider->setValue( intCurve->b() );
}

void mainWindow::setRealTypeEllipticWithOval()
{
    intCurve->setRealTypeEllipticWithOval();
    ui.aSlider->setValue( intCurve->a() );
    ui.bSlider->setValue( intCurve->b() );
}

void mainWindow::updateActions()
{
    ui.actionReset->setEnabled(isTouched());

    if (!minusP.isNull()) {
        if (minusP->isComputable()) {
            ui.actionCompute_minusP->setEnabled(true);
        } else {
            ui.actionCompute_minusP->setChecked(false);
            ui.actionCompute_minusP->setEnabled(false);
        }
    }

    if (!twoP.isNull()) {
        if (twoP->isComputable()) {
            ui.actionCompute_2P->setEnabled(true);
        } else {
            ui.actionCompute_2P->setChecked(false);
            ui.actionCompute_2P->setEnabled(false);
        }
    }

    if (!PPlusQ.isNull()) {
        if (PPlusQ->isComputable()) {
            ui.actionCompute_PPlusQ->setEnabled(true);
        } else {
            ui.actionCompute_PPlusQ->setChecked(false);
            ui.actionCompute_PPlusQ->setEnabled(false);
        }
    }

    if (!QPlusR.isNull()) {
        if (QPlusR->isComputable()) {
            ui.actionCompute_QPlusR->setEnabled(true);
        } else {
            ui.actionCompute_QPlusR->setChecked(false);
            ui.actionCompute_QPlusR->setEnabled(false);
        }
    }

    if (!PPlusQPlusR2.isNull()) {
        if (PPlusQPlusR1->isComputable()) {
            ui.actionCompute_PPlusQPlusR_2->setEnabled(true);
        } else {
            ui.actionCompute_PPlusQPlusR_2->setChecked(false);
            ui.actionCompute_PPlusQPlusR_2->setEnabled(false);
        }
    }

    if (!PPlusQPlusR2.isNull()) {
        if (PPlusQPlusR2->isComputable()) {
            ui.actionCompute_PPlusQPlusR_1->setEnabled(true);
        } else {
            ui.actionCompute_PPlusQPlusR_1->setChecked(false);
            ui.actionCompute_PPlusQPlusR_1->setEnabled(false);
        }
    }

    // Check appropriate entries in the 'language' menu.
    QList<QAction *> const actions = languageActionGroup.actions();
    for(auto* action : actions) {
        action->setChecked(action->data() == language);
    }

}

void mainWindow::curveChanged()
{
    ui.equation->setText( tr("Curve Equation: %1").arg(realCurve->equation()) );
}

void mainWindow::closeEvent(QCloseEvent *event)
{
    QSettings settings;

    // Save elliptic curve
    settings.setValue("ec/a", intCurve->a());
    settings.setValue("ec/b", intCurve->b());

    // Save Free Points
    settings.setValue("points/P_affine", (P->getState() == ec_realPoint::affine));
    if (P->getState() == ec_realPoint::affine) {
        settings.setValue("points/P", P->getCoords());
    }
    settings.setValue("points/Q_affine", (Q->getState() == ec_realPoint::affine));
    if (Q->getState() == ec_realPoint::affine) {
        settings.setValue("points/Q", Q->getCoords());
    }
    settings.setValue("points/R_affine", (R->getState() == ec_realPoint::affine));
    if (R->getState() == ec_realPoint::affine) {
        settings.setValue("points/R", R->getCoords());
    }

    // Save actions
    settings.setValue("actions/minusP", ui.actionCompute_minusP->isChecked());
    settings.setValue("actions/2P", ui.actionCompute_2P->isChecked());
    settings.setValue("actions/PPlusQ", ui.actionCompute_PPlusQ->isChecked());
    settings.setValue("actions/QPlusR", ui.actionCompute_QPlusR->isChecked());
    settings.setValue("actions/PPlusQPlusR_1", ui.actionCompute_PPlusQPlusR_1->isChecked());
    settings.setValue("actions/PPlusQPlusR_2", ui.actionCompute_PPlusQPlusR_2->isChecked());

    // Save window geometry
    if (!isFullScreen()) {
        settings.setValue("mainWindow/geometry", saveGeometry());
        settings.setValue("mainWindow/windowState", saveState());
        settings.setValue("splitterSizes", ui.splitter->saveState());
    }

    // Save language settings
    QAction *ptr = languageActionGroup.checkedAction();
    if (ptr != nullptr) {
        settings.setValue("mainWindow/language", ptr->data().toString());
    }

    event->accept();
}

auto mainWindow::getSaveFileName(const QString& title, const QString& filter, const QString& ending) -> QString
{
    return QFileDialog::getSaveFileName(this, title, "elliptic curve."+ending, filter);
}

void mainWindow::changeEvent(QEvent *event)
{
    if ((event->type() == QEvent::LocaleChange) && (language == u"system"_s)) {
        switchLanguage(u"system"_s);
    }

    QMainWindow::changeEvent(event);
}

void mainWindow::exportSVG()
{
    gfxExportDialog expDLG(ui.plotter, false, this);
    expDLG.setWindowTitle(ui.actionExportSVG->text());
    if (expDLG.exec() ==  QDialog::Rejected) {
        return;
    }

    QString const fileName = getSaveFileName(ui.actionExportSVG->text(),
                                             tr("Scalable Vector Graphics File (*.svg)"),
                                             u".svg"_s);
    if (fileName.isEmpty()) {
        return;
    }

    QFile file( fileName );
    if (file.open(QIODevice::WriteOnly)) {
        QSvgGenerator generator;
        generator.setOutputDevice(&file);
        QRectF const box = ui.plotter->augmentedBox();
        generator.setSize( QSize(qRound(box.width()), qRound(box.height())) );
#if QT_VERSION >= 0x040500
        generator.setViewBox( QRectF(0, 0, box.width(), box.height()) );
        generator.setTitle( tr("Elliptic Curve") );
        generator.setDescription(u"Drawing generated by the Elliptic Curve Plotter."_s);
#endif

        QPainter painter;
        painter.begin(&generator);
        ui.plotter->paint(painter, expDLG.showAxes(), expDLG.showGrid());
        painter.end();
        file.close();
    }

    if (file.error() != QFile::NoError) {
        QMessageBox::warning( this, tr("File Error"),
                              tr("<p>The file %1 could not be written.</p><p>The system reported the following error: %2</p>").arg(fileName, file.errorString()));
    }
}

void mainWindow::exportPNG()
{
    gfxExportDialog expDLG(ui.plotter, true, this);
    expDLG.setWindowTitle(ui.actionExportPNG->text());
    if (expDLG.exec() ==  QDialog::Rejected) {
        return;
    }

    QString const fileName = getSaveFileName(ui.actionExportSVG->text(),
                                             tr("Portable Network Graphics (*.png)"),
                                             u".png"_s);
    if (fileName.isEmpty()) {
        return;
    }

    QFile file( fileName );
    if (file.open(QIODevice::WriteOnly)) {
        QImage img( expDLG.width(), expDLG.height(), QImage::Format_ARGB32 );
        if (expDLG.transparent()) {
            img.fill(Qt::transparent);
        }
        QPainter painter;
        painter.begin(&img);
        painter.setRenderHint(QPainter::Antialiasing);
        ui.plotter->paint(painter, expDLG.showAxes(), expDLG.showGrid(), expDLG.transparent());
        painter.end();
        img.setText(QString(), u"Drawing generated by the Elliptic Curve Plotter."_s);
        img.save( &file, "PNG" );
        file.close();
    }

    if (file.error() != QFile::NoError) {
        QMessageBox::warning( this, tr("File Error"),
                              tr("<p>The file %1 could not be written.</p><p>The system reported the following error: %2</p>").arg(fileName, file.errorString()));
    }
}

void mainWindow::report()
{
    Ui::infoDialog ui;
    QDialog dlg;
    ui.setupUi(&dlg);
    ui.logo->load(QString(u":/icons/ecLogo.svg"_s));
    dlg.setWindowTitle(tr("Report Problem or Suggestion"));
    ui.stackedWidget->setCurrentWidget(ui.pageProblems);
    dlg.exec();
}

void mainWindow::help()
{
    Ui::infoDialog ui;
    QDialog dlg;
    ui.setupUi(&dlg);
    ui.logo->load(QString(u":/icons/ecLogo.svg"_s));
    dlg.setWindowTitle(tr("How to Help"));
    ui.stackedWidget->setCurrentWidget(ui.pageHelp);
    dlg.exec();
}

void mainWindow::experiment()
{
    Ui::infoDialog ui;
    QDialog dlg;
    ui.setupUi(&dlg);
    ui.logo->load(QString(u":/icons/ecLogo.svg"_s));
    dlg.setWindowTitle(tr("Questions and Experiments"));
    ui.stackedWidget->setCurrentWidget(ui.pageExperiments);
    dlg.exec();
}

void mainWindow::onSwitchLanguageAction(QAction *action)
{
    if (action == nullptr) {
        return;
    }

    switchLanguage(action->data().toString());
}

void mainWindow::switchLanguage(const QString& lang)
{
    if (lang.isEmpty()) {
        return;
    }

    setupTranslators(lang);

    // Trigger retranslation of the user interface
    ui.retranslateUi(this);
    realCurve->touch(); // Triggers retranslation of the user-visible objects in the 'elliptic curve sketch' and 'description' window
}

void mainWindow::setupTranslators(const QString& lang)
{
    language = lang;

    if (lang == u"system"_s) {
        // Update translators
        (void) appTranslator.load(QLocale::system(), u"ellipticcurve"_s, u"_"_s, u":"_s);
        (void) QtTranslator.load(QLocale::system(), u"qt"_s, u"_"_s, QLibraryInfo::path(QLibraryInfo::TranslationsPath));
    } else {
        // Update translators
        (void) appTranslator.load(QString(u"ellipticcurve_"_s) + lang, u":"_s);
        (void) QtTranslator.load("qt_" + lang, QLibraryInfo::path(QLibraryInfo::TranslationsPath));
    }
}

void mainWindow::resetGUIFeedback()
{
    if (!isTouched()) {
        updateActions();
        return;
    }

    auto *msg = new QMessageBox(QMessageBox::Information, u"Elliptic Curve Plotter"_s,
                                tr("Resetting User Interface"), QMessageBox::Ok, this, Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint );
    msg->setAttribute(Qt::WA_DeleteOnClose);
    msg->show();
    QTimer::singleShot(750ms, msg, &QWidget::close);
    QTimer::singleShot(500ms, this, &mainWindow::updateActions);
    resetGUI();
}

void mainWindow::resetGUIAfterInactivity()
{
    if (!isTouched()) {
        return;
    }

    auto *msg = new QMessageBox(QMessageBox::Information, u"Elliptic Curve Plotter"_s,
                                tr("Resetting User Interface"), QMessageBox::Ok, this, Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint );
    msg->setInformativeText(tr("Several Minutes without user interaction."));
    msg->setAttribute(Qt::WA_DeleteOnClose);
    msg->show();
    QTimer::singleShot(1s, msg, &QWidget::close);
    resetGUI();
}

void mainWindow::resetGUI()
{
    if (!isTouched()) {
        return;
    }

    intCurve->setA(-6);
    intCurve->setB(6);
    ui.aSlider->setValue( intCurve->a() );
    ui.aLabel_2->setNum( intCurve->a() );
    ui.bSlider->setValue( intCurve->b() );
    ui.bLabel_2->setNum( intCurve->b() );
    intCurve->touch();

    QList<int> sizes;
    sizes.push_back(1000);
    sizes.push_back(1000);
    ui.splitter->setSizes(sizes);

    if (P != nullptr) {
        P->setInvalid();
    }
    if (Q != nullptr) {
        Q->setInvalid();
    }
    if (R != nullptr) {
        R->setInvalid();
    }

    switchLanguage(u"system"_s);
}

auto mainWindow::isTouched() -> bool
{
    if (ui.aSlider->value() != -6) {
        return true;
    }
    if (ui.bSlider->value() != 6) {
        return true;
    }

    if (P->getState() != ec_realPoint::invalid) {
        return true;
    }
    if (Q->getState() != ec_realPoint::invalid) {
        return true;
    }
    if (R->getState() != ec_realPoint::invalid) {
        return true;
    }

    if (language != u"system"_s) {
        return true;
    }

    QList<int> sizes = ui.splitter->sizes();
    if (sizes.size() == 2) {
        if ( qAbs(sizes[0]-sizes[1]) > 10 ) {
            return true;
        }
    }

    return false;
}

void mainWindow::enterKioskMode()
{
    auto *detector = new inactivityDetector(this);
    connect(detector, &inactivityDetector::inactivityDetected, this, &mainWindow::resetGUIAfterInactivity);
    qApp->installEventFilter(detector);

    ui.menuExport->setEnabled(false);
    ui.actionExportSVG->setEnabled(false);
    ui.actionExportPNG->setEnabled(false);

    ui.menuView->setEnabled(false);
    ui.actionKioskMode->setEnabled(false);
    showFullScreen();

    resetGUI();
}

void mainWindow::enterKioskModeFeedBack()
{
    auto *msg = new QMessageBox(QMessageBox::Warning, tr("Elliptic Curve Plotter - Kiosk Mode"), tr("Do you really wish to enter the Kiosk Mode?"),
                                QMessageBox::Yes|QMessageBox::No, this);
    msg->setDefaultButton(QMessageBox::No);
    msg->setInformativeText(tr("<p>The Kiosk Mode is a restricted mode useful when "
			     "running the Elliptic Curve Plotter on interactive kiosks that are "
			     "open to the public.</p>"
			     "<ul>"
			     "<li>Several menu items are disabled, so users cannot exit the program or access the file system.</li>"
			     "<li>The program switches to full-screen mode.</li>"
			     "<li>The user interface will be reset after three minutes of inactivity.</li>"
			     "</ul>"
			     "<p><b>There is no way to leave the Kiosk Mode other than exiting the program.</b></p>"
			     "<p><b>Hint:</b> To start the Elliptic Curve Plotter in Kiosk Mode "
			     "without showing this warning, start the Elliptic Curve Plotter "
			     "from the command line using the command line option "
			     "<b>-kiosk</b>.</p>"));
    if (msg->exec() == QMessageBox::Yes) {
        enterKioskMode();
    }
}
