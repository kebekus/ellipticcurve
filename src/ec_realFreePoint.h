/***************************************************************************
 *   Copyright (C) 2018-2024 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef EC_REALFREEPOINT
#define EC_REALFREEPOINT 1

#include "ec_realPoint.h"


/**
 * Graphic presentation of a point on a real elliptic curve, whose coordinates
 * can be set freely and which can be manipulated with the mouse.
 *
 * The free point has a 'home' position, where an image of the point is
 * displayed whenever the point is invalid. The used can drag the point with
 * the mouse along the curve, or to and from the home position.
 * 
 * Whenever the curve changes, the point tries to change along with the curve,
 * if possible. If changing along with the curve is not possible (e.g. when
 * the curve becomes singular, or then the point is on the oval, which
 * vanishes during the change of the curve), the state is set to invalid, and
 * the point jumps to the home position.
 *
 * @author Stefan Kebekus
 */


class ec_realFreePoint : public ec_realPoint
{
    Q_OBJECT
    
public:
    /**
   * Default constructor
   *
   * Constructs a point on the curve "parentcurve". The parent curve is
   * mandatory. Once the parent curve is destructed, this object becomes
   * useless.
   *
   * @param boundingBox The point is only shown if it is inside the
   * boundingBox, which is defined here. This argument is passed on to the
   * constructor of the ec_realPoint class.
   *
   * @param parentCurve This object represents a point on the curve
   * "parentCurve". Changes of the parentCurve are monitored, and coordinates,
   * state and visibility of the point may change when the parentCurve
   * changes. The point is not deleted automatically when the parentCurve is
   * deleted, but becomes useless once the parentCurve is gone.
   *
   * @param homePosition A position where an image of the point is displayed
   * whenever the point is invalid. The use can drag the point to and from the
   * home position, in order to make the point valid or invalid.
   *
   * @param parentItem The parentItem is passed on to the default constructor
   * of the QGraphicsItem class, which this class inherits.
   *
   * @param name The name of the point, used for the label.
   */
    ec_realFreePoint(const QRectF& boundingBox,
                     QObject* parentCurve,
                     QPointF homePosition,
                     QGraphicsItem* parentItem,
                     const QString& name);

    /**
     * Reimplemented from QGraphicsItem
   */
    QRectF boundingRect() const override;

    /**
   * Returns the home positions set in the constructor
   */
    QPointF getHomePos() const { return homePosition; };

    /**
   * Re-implemented from QGraphicsItem
   */
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;

    /**
   * Re-implemented from QGraphicsItem
   *
   * Object of type "ec_realFreePoint" have type # = (QGraphicsItem::UserType+1)
   */
    int type() const override { return UserType + 1; };

public slots:
    /**
   * Set coordinates of the point
   *
   * Attempts to set the coordinate of the point. If the coordinate is close
   * the elliptic curve, the coordinate is accepted and corrected. Otherwise,
   * the attempt fails and the point remains unchanged.
   *
   * @returns true, if the change was successful, false otherwise
   */
    bool setCoords(QPointF pt) { return ec_realPoint::setCoords(pt);}

    /**
   * Sets this point to state 'infinity'
   *
   * Attempts to set this point to 'infinity'. The attempt will fail if the
   * curve is not an elliptic curve.
   *
   * @returns true, if the change was successful, false otherwise
   */
    bool setInfinity() { return ec_realPoint::setInfinity(); }


protected:
    /**
   * Re-implemented from ec_realPoint
   */
    void setToolTip() override;


protected slots:
    /**
   * Modify the point whenever the curve changes
   *
   * This slot is used to monitor the curve, in order to adjust the poin't
   * position and state whenever the curve changes.
   */
    void updateGraphics() override;


protected:
    /**
   * Re-implemented from QGraphicsItem
   */
    void mousePressEvent( QGraphicsSceneMouseEvent * event ) override;

    /**
   * Re-implemented from QGraphicsItem
   */
    void mouseReleaseEvent( QGraphicsSceneMouseEvent * event ) override;


private slots:
    /**
   * Shows or hides the tmpCircle, depending on the state of the point
   */
    void setTmpCircleVisibility();


private:
    /**
   * Pointer to an auxiliary circle
   * 
   * The auxiliary circle is constructed in the constructor of this
   * ec_realFreePoint, so that this pointer is always valid. The tmpCircle is
   * used to display the point whenever it is in its home position; this way
   * the class ec_realFreePoint does not interfer with the circle, which is
   * entirely controlled by the ec_realPoint, which this class inherits.
   */
    QGraphicsEllipseItem * tmpCircle;

    /**
   * Pointer to an auxiliary label
   *
   * This is a label that described the tmpCircle. Like tmpCircle, this
   * element is constructed in the constructor of this ec_realFreePoint, so
   * that the pointer is always valid.
   */
    QGraphicsSimpleTextItem * tmpLabel;

    /**
   * home position of the point
   */
    QPointF homePosition;
};

#endif
