/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QBrush>
#include <QDebug>
#include <QGraphicsLineItem>
#include <QPen>

#include "ec_real.h"
#include "ec_realMinusPoint.h"

ec_realMinusPoint::ec_realMinusPoint(const QRectF &boundingBox,
                                     ec_real *_parentCurve,
                                     ec_realPoint *_parentPoint,
                                     QGraphicsItem *parentItem)
    : ec_realPoint(boundingBox, _parentCurve, parentItem, "-" + _parentPoint->getName(), true)
{
  // Paranoid safety checks
  if (_parentCurve == nullptr) {
      qCritical() << "ec_realMinusPoint::ec_realMinusPoint called with parentCurve == 0";
  }
  if (_parentPoint == nullptr) {
      qCritical() << "ec_realMinusPoint::ec_realMinusPoint called with parentPoint == 0";
  }

  compute     = false;
  parentPoint = _parentPoint;

  connect(parentPoint, SIGNAL(changed()), this, SLOT(updateGraphics()));

  circle->setBrush(QBrush(Qt::yellow));
}


QString ec_realMinusPoint::description() const
{
  // Paranoid safety checks
  if (parentCurve.isNull()) {
    qWarning() << "ec_realMinusPoint::description() called with parentCurve == 0";
    return {};
  }

  QString descr;

  descr = tr("The point <b>%1</b> is the inverse of the point <b>%2</b> according to the group law of the elliptic curve. "
	     "Geometrically, the point <b>%1</b> is constructed as the image of the point <b>%2</b> under reflection against "
         "the x-axis.").arg(getName(), parentPoint->getName());
  descr += " " + ec_realPoint::description();
  return descr;
}


bool ec_realMinusPoint::isComputable() const
{
    if (parentPoint.isNull()) {
        return false;
    }

  return (parentPoint->getState() != invalid);
}


void ec_realMinusPoint::setCompute(bool doCompute)
{
    if (doCompute == compute) {
        return;
    }

  compute = doCompute;
  updateGraphics();
}


void ec_realMinusPoint::setToolTip()
{
    QString const descr = tr("<h4>Auxiliary Line</h4>"
                             "<p>The point <b>%1</b> is constructed as the image of the point "
                             "<b>%2</b> under reflection against the x-axis. "
                             "This auxiliary line connects the points <b>%2</b> and <b>%1</b>.</p>")
                              .arg(getName(), parentPoint->getName());
    vertLine->setToolTip(descr);

    QString tt;
    if (!parentPoint.isNull()) {
        tt += "<h4>" + tr("Inverse of the Point <b>%1</b>").arg(parentPoint->getName()) + "</h4>";
    }
  tt += "<p>" + description() + "</p>";
  tt += "<p>" + tr("Move the point <b>%1</b> to see how the inverse changes along with <b>%1</b>. "
		   "You can also change the shape of the curve to see how the whole construction changes.").arg(parentPoint->getName()) + "</p>";
  circle->setToolTip( tt ); 
}


void ec_realMinusPoint::updateGraphics()
{
  // Paranoid safety checks
  if (parentCurve.isNull()) {
      qCritical() << "ec_realMinusPoint::pointChanged called with parentCurve == 0";
  }
  if (parentPoint.isNull()) {
      qCritical() << "ec_realMinusPoint::pointChanged called with parentPoint == 0";
  }

  emit computabilityChanged();
  if ((!compute) || !isComputable()) {
    setInvalid();
    return;
  }

  setCoords(QPointF(parentPoint->getCoords().x(), -parentPoint->getCoords().y()));
}
