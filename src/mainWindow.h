/***************************************************************************
 *   Copyright (C) 2018-2024 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QActionGroup>
#include <QLibraryInfo>
#include <QGraphicsItem>
#include <QTranslator>

#include "ec_real.h"
#include "ec_realDoublePoint.h"
#include "ec_realMinusPoint.h"
#include "ec_realSumPoint.h"
#include "ui_mainWindow.h"

class ec_realSumPoint;


/**
 * Main window of the elliptic curve plotter
 *
 * This widget contains two sliders to set the coefficients of a Weierstrass
 * equation, a widget that displays the associated curve and a few points, and
 * a widget with descriptions of the curve and the points.
 *
 * Obviously, this class also handels a bunch of actions, etc. pp.
 */

class mainWindow : public QMainWindow
{
    Q_OBJECT

public:
  /**
   * Standard constructor
   *
   * Constructs the GUI and all necessary objects, and reads in user settings
   * from the last save operation.
   *
   * @param parent This parameter is passed on to the constructor of the
   * QMainWindow
   */
  mainWindow(bool kiosk, QWidget *parent = nullptr);

  // No copy constructor
  mainWindow(mainWindow const&) = delete;

  // No assign operator
  mainWindow& operator =(mainWindow const&) = delete;

  // No move constructor
  mainWindow(mainWindow&&) = delete;

  // No move assignment operator
  mainWindow& operator=(mainWindow&&) = delete;

  /**
   * Standard destructor
   *
   * The destructor hase been re-implemented to save the user settings
   */
  ~mainWindow() override;
  

protected:
  /**
   * Re-implemented from QWidget
   */
  void closeEvent(QCloseEvent *event) override;


private slots:
  /**
   * Re-implemented to support LocaleChange events
   */
  void changeEvent(QEvent *event) override;

  /** 
   * Export drawing to an SVG file
   */
  void exportSVG();

  /** 
   * Export drawing to a PNG file
   */
  void exportPNG();

  /**
   * Update the curve equation text
   */
  void curveChanged();

  /**
   * Shows a trivial about dialog
   */
  void showAboutDialog();

  /**
   * Enters the what's this mode prgramatically
   */
  static void enterWhatsThis();

  /**
   * Set a preset value for the parameters of the curve, to obtain a Neil parabola
   */
  void setRealTypeNeilParabola();

  /**
   * Set a preset value for the parameters of the curve, to obtain a nodal curve
   */
  void setRealTypeNodalCurve();

  /**
   * Set a preset value for the parameters of the curve, to obtain a curve with an isolated point
   */
  void setRealTypeIsolatedPt();

  /**
   * Set a preset value for the parameters of the curve, to obtain a curve without an oval
   */
  void setRealTypeEllipticWithoutOval();

  /**
   * Set a preset value for the parameters of the curve, to obtain a curve with an oval
   */
  void setRealTypeEllipticWithOval();

  /**
   * Enter Kiosk Mode
   *
   * Calls enterKioskMode() after presenting a warning to the user
   */
  void enterKioskModeFeedBack();

  /**
   * Shows a dialog 'report problem'
   */
  static void report();

  /**
   * Shows a dialog 'how to help'
   */
  static void help();

  /**
   * Shows a dialog 'experiment'
   */
  static void experiment();

  /**
   * Switches to a different language and updates the GUI
   */
  void onSwitchLanguageAction(QAction *);

  /**
   * Switches to a different language and updates the GUI
   */
  void switchLanguage(const QString&);

  /**
   * Enables/Diables actions whenever points become computable or uncomputable
   */
  void updateActions();

  /* Brings the GUI to a well-defined initial state that can serve as
     a starting point for users. */
  void resetGUI();

  /* Calls resetGUI() and shows a little message window for two
     seconds to provide user feedback. Slot is called in response
     to menu item 'reset gui' */
  void resetGUIFeedback();

  /* Calls resetGUI() and also shows a little message window for two
     seconds to provide user feedback */
  void resetGUIAfterInactivity();

private:
  /**
   * Switches to a different language without updating the GUI
   *
   * lang is a string such as 'system' or 'de'
   */
  void setupTranslators(const QString& lang);

  /* Create a 'languages' menu and populates it with the list of existing
     translations. */
  void makeLanguageMenu();

  /* Checks if the GUI is in the state set by resetGUI(), or if it has
     been modified from that state */
  bool isTouched();

 /* Enters the Kiosk Mode, a restricted full-screen mode useful when
    running the Elliptic Curve Plotter on interactive kiosks that are
    open to the public. The File Menu and other menu items will be
    switched off. */
  void enterKioskMode();

  /* Pretty self-explanatory. The method issues a warning if the file
     name chosen by the user does not end in 'ending'. This is a
     workaround because on some systems, the file dialog does not end
     the 'ending' automatically.  */
  QString getSaveFileName(const QString& title, const QString& filter, const QString& ending);
  
  /**
   * Integral elliptic curve, connected with the sliders
   * 
   * The elliptic curve is constructed in the constructor of this
   * mainWindow and is destructed automatically along with the
   * mainWindow.
   */
  ec *intCurve;

  /**
   * Pointers to points on the elliptic curve.
   *
   * These pointers are kept for the use of the updateActions() method. The
   * points are construced in the constructor of this mainWindow, but
   * they are owned by the ecPlotter object, and will be destructed once the
   * ecPlotter gets destruced. To avoid segfaults, it is therefore advisable
   * to always check if the pointers did become zero before using them.
   */
  QPointer<ec_realFreePoint> P;
  QPointer<ec_realFreePoint> Q;
  QPointer<ec_realFreePoint> R;
  QPointer<ec_realDoublePoint> twoP;
  QPointer<ec_realMinusPoint> minusP;
  QPointer<ec_realSumPoint> PPlusQ;
  QPointer<ec_realSumPoint> QPlusR;
  QPointer<ec_realSumPoint> PPlusQPlusR1;
  QPointer<ec_realSumPoint> PPlusQPlusR2;

  /**
   * GUI, as construced with the Qt designer
   */
  Ui::mainWindow ui{};

  /**
   * Action Group that contains all items of the 'language' menu.
   */
  QActionGroup languageActionGroup;

  /**
   * Real elliptic curve, connected with the animator
   * 
   * The elliptic curve is constructed in the constructor of this
   * mainWindow and is destructed automatically along with the
   * mainWindow.
   */
  ec_real *realCurve;

  /**
   * Application-wide tranlators
   */
  QTranslator appTranslator;
  QTranslator QtTranslator;
  // Either 'system' or the name of the language last set. This variable is
  // written only by the method setupTranslators()
  QString language;

};
