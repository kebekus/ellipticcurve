/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef INTERVALF
#define INTERVALF

#include <QMetaType>
#include <QtGlobal>


/**
 * Closed, bounded interval in the real numbers
 *
 * This class represents a closed, bounded interval in the real numbers which
 * can also be empty.
 *
 * @note Unbounded intervals can be simulated by setting the boundaries of the
 * interval to -DBL_MAX and DBL_MAX, respectively.
 * 
 * @author Stefan Kebekus
 */

class intervalF
{
 public:
  /**
   * Default constructor
   *
   * Constructs an empty interval
   */
  intervalF() { setEmpty(); }

  /**
   * Constructs bounded interval
   *
   * Constructs a bounded interval. If left > right, a warning is issued and
   * the empty interval is constructed.
   */
  intervalF(double left, double right);

  /**
   * Default copy constructor
   */
  intervalF(const intervalF & other);

  // Default assign operator
  intervalF& operator =(intervalF const&) = default;

  // Default move constructor
  intervalF(intervalF&&) = default;

  // Default move assignment operator
  intervalF& operator=(intervalF&&) = default;

  // Default destructor
  ~intervalF() = default;

  /**
   * Check if the interval is empty
   */
  bool isEmpty() const { return empty; }

  /**
   * Makes the interval the empty interval
   */
  void setEmpty() { empty = true; }

  /**
   * Sets interval boundaries
   *
   * This method sets the left boundaries of the interval. If the right
   * boundary is smaller than the new left boundary, this method will set the
   * interval to empty and a warning is issues via qWarning().
   *
   * @param leftBd new left boundary of the interval
   *
   * @param rightBd new right boundary of the interval
   */
  void set( double leftBd, double rightBd );

  /**
   * Sets the left boundary of the interval
   *
   * This method sets the left boundary of the interval. There are a few
   * special cases that deserve extra mention
   *
   * - if the interval is empty, this method will set both the left and the
       right boundary to be equal to the new left boundary. The interval will
       thus become the closed interval [left, left] which is of length zero.
      
   * - if the interval is not empty, and the right boundary is smaller than
       the new left boundary, this method will set the interval to empty.

   * - in all other cases, the existing right boundary will not be touched.
   *
   * @param leftBd new left boundary of the interval
   */
  void setLeft( double leftBd );

  /**
   * Returns left boundary of the interval if the interval is not empty
   *
   * If the interval is not empty, this method returns the left
   * bound. Otherwise, a random value is returned, and a warning is issued via
   * qWarning().
   */
  double left() const;

  /**
   * Sets the right boundary of the interval
   *
   * This method sets the right boundary of the interval. There are a few
   * special cases that deserve extra mention
   *
   * - if the interval is empty, this method will set both the left and the
       right boundary to be equal to the new right boundary. The interval will
       thus become the closed interval [right, right] which is of length zero.
      
   * - if the interval is not empty, and the existing left boundary is larger
       than the new right boundary, this method will set the interval to
       empty.

   * - in all other cases, the existing right boundary will not be touched.
   *
   * @param rightBd new right boundary of the interval
   */
  void setRight( double rightBd );

  /**
   * Returns right boundary of the interval if the interval is not empty
   *
   * If the interval is not empty, this method returns the right
   * bound. Otherwise, a random value is returned, and a warning is issued via
   * qWarning().
   */
  double right() const;

  /**
   * Returns the intersection of two intervals
   *
   * @param other another interval which is intersected with this interval
   */
  intervalF intersection( const intervalF &other ) const;

  /**
   * Compares two intervals
   *
   * @warning This method uses floating point comparison to check if two
   * intervals agree. The usual caveats apply.
   */
  bool operator==(const intervalF &other) const;
  
  
 private:
  /**
   * Indicates if the interval is empty
   */
  bool empty{};

  /**
   * If the interval is not empty and leftBounded==true, this member contains
   * the left bound. Otherwise, this member contains random values.
   */
  double leftBound{};

  /**
   * If the interval is not empty and leftBounded==true, this member contains
   * the left bound. Otherwise, this member contains random values.
   */
  double rightBound{};
};

Q_DECLARE_METATYPE(intervalF)

#endif
