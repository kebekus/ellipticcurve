/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include <QtTest/QtTest>

#include "intervalF.h"
#include "intervalF_test.h"

void intervalF_test::constructor()
{
    intervalF const shouldBeEmpty1;
    QVERIFY(shouldBeEmpty1.isEmpty());

    intervalF const shouldBeEmpty2(2, 1);
    QVERIFY(shouldBeEmpty2.isEmpty());

    intervalF const shouldNotBeEmpty(1, 2);
    QVERIFY(!shouldNotBeEmpty.isEmpty());
    QVERIFY(shouldNotBeEmpty.left() == 1);
    QVERIFY(shouldNotBeEmpty.right() == 2);
}


void intervalF_test::set()
{
  intervalF e;
  
  e.set(2,1);
  QVERIFY( e.isEmpty() );

  e.set(1,2);
  QVERIFY( !e.isEmpty() );
  QVERIFY( e.left() == 1 );
  QVERIFY( e.right() == 2 );
}


void intervalF_test::setLeft()
{
  intervalF e;

  // Empty interval should become zero-length
  e.setLeft(1); 
  QVERIFY( !e.isEmpty() );
  QVERIFY( e.left() == 1 );
  QVERIFY( e.right() == 1 );

  // Setting left to value larger than existing right => empty
  e.setLeft(2);
  QVERIFY( e.isEmpty() );

  // Setting left value smaller then existing right
  e.setRight(2);
  e.setLeft(1);
  QVERIFY( !e.isEmpty() );
  QVERIFY( e.left() == 1 );
  QVERIFY( e.right() == 2 );
}


void intervalF_test::setRight()
{
  intervalF e;

  // Empty interval should become zero-length
  e.setRight(1); 
  QVERIFY( !e.isEmpty() );
  QVERIFY( e.left() == 1 );
  QVERIFY( e.right() == 1 );

  // Setting right to value smaller than existing left => empty
  e.setRight(0);
  QVERIFY( e.isEmpty() );

  // Setting right value larger then existing right
  e.setLeft(1);
  e.setRight(2);
  QVERIFY( !e.isEmpty() );
  QVERIFY( e.left() == 1 );
  QVERIFY( e.right() == 2 );
}


void intervalF_test::equality()
{
    intervalF e;
    intervalF f;

    e.set(1, 2);
    f.set(1, 2);
    QVERIFY(e == f);

    e.set(1, 2);
    f.set(1, 3);
    QVERIFY(!(e == f));

    e.setEmpty();
    f.set(1, 3);
    QVERIFY(!(e == f));

    e.set(1, 2);
    f.setEmpty();
    QVERIFY(!(e == f));

    e.setEmpty();
    f.setEmpty();
    QVERIFY(e == f);
}


void intervalF_test::intersection_data()
{
  QTest::addColumn<intervalF>("i1");
  QTest::addColumn<intervalF>("i2");
  QTest::addColumn<intervalF>("intersection");
  
  QTest::newRow("t1") << intervalF( 0, 2) << intervalF( -2, -1) << intervalF();
  QTest::newRow("t2") << intervalF( 0, 2) << intervalF( -2,  1) << intervalF( 0, 1 );
  QTest::newRow("t3") << intervalF( 0, 2) << intervalF( -2,  3) << intervalF( 0, 2 );
  QTest::newRow("t4") << intervalF( 0, 3) << intervalF(  1,  2) << intervalF( 1, 2 );
  QTest::newRow("t5") << intervalF( 0, 3) << intervalF(  1,  4) << intervalF( 1, 3 );
  QTest::newRow("t6") << intervalF( 0, 3) << intervalF(  4,  5) << intervalF();

  QTest::newRow("e1") << intervalF( 0, 3) << intervalF() << intervalF();
  QTest::newRow("e2") << intervalF() << intervalF( 1, 2 ) << intervalF();
  QTest::newRow("e3") << intervalF() << intervalF() << intervalF();
}


void intervalF_test::intersection()
{
  QFETCH(const intervalF, i1);
  QFETCH(const intervalF, i2);
  QFETCH(const intervalF, intersection);

  QCOMPARE( i1.intersection(i2), intersection );
}


QTEST_MAIN(intervalF_test)
