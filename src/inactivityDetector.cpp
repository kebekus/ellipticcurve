/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "inactivityDetector.h"
#include <chrono>

using namespace std::chrono_literals;


inactivityDetector::inactivityDetector(QObject *parent)
  : QObject(parent)
{
  connect(&timer, SIGNAL(timeout()), this, SIGNAL(inactivityDetected()));
  timer.setSingleShot(true);
  timer.setInterval(3min); // 3 Minutes
}


bool inactivityDetector::eventFilter(QObject *obj, QEvent *ev)
{
    if (ev->type() == QEvent::KeyPress || ev->type() == QEvent::MouseMove) {
        // now reset your timer, for example
        timer.start();
    }

  return QObject::eventFilter(obj, ev);
}
