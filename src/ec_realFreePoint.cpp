/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QBrush>
#include <QCursor>
#include <QDebug>
#include <QGraphicsSceneMouseEvent>
#include <QPen>

#include "ec_real.h"
#include "ec_realFreePoint.h"

ec_realFreePoint::ec_realFreePoint(const QRectF &boundingBox,
                                   QObject* parentCurve,
                                   QPointF _homePosition,
                                   QGraphicsItem *parentItem,
                                   const QString &name)
    : ec_realPoint(boundingBox, parentCurve, parentItem, name)
    , homePosition(_homePosition)
    , tmpCircle(new QGraphicsEllipseItem(this))
    , tmpLabel(new QGraphicsSimpleTextItem(tmpCircle))
{
    setCursor(Qt::OpenHandCursor);
    setAcceptedMouseButtons(Qt::LeftButton);

    tmpCircle->setRect(homePosition.x() - 0.15, homePosition.y() - 0.15, 0.3, 0.3);
    tmpCircle->setPen(QPen(Qt::black, 0.03));
    tmpCircle->setBrush(QBrush(Qt::red));
    tmpCircle->setZValue(2.0);

    tmpLabel->setZValue(2.0);
    tmpLabel->setScale(0.04);
    tmpLabel->setText(getName());
    QRectF const br = tmpLabel->mapToScene(tmpLabel->boundingRect()).boundingRect();
    QPointF const offset = QPointF(-0.3, br.height() / 2);
    tmpLabel->setPos(homePosition - offset);

    setTmpCircleVisibility();

    connect(this, &ec_realPoint::changed, this, &ec_realFreePoint::setTmpCircleVisibility);
}


QRectF ec_realFreePoint::boundingRect() const
{
    if (getState() != affine) {
        return tmpCircle->boundingRect();
    }

    return ec_realPoint::boundingRect();
}


void ec_realFreePoint::setToolTip()
{
    QString tt;

    tt += "<h4>" + tr("Point") + " <b>%1</b></h4>";
    tt += "<p>" + description() + "</p>";
    tt += "<p>" + tr("Move the point along the curve, or move it to its home position.") + "</p>";
    circle->setToolTip(tt.arg(getName()));
}


void ec_realFreePoint::updateGraphics()
{
    // Paranoid safety checks.
    if (parentCurve.isNull()) {
        qCritical() << "ec_realFreePoint::mouseMoveEvent, but no parentcurve";
    }

    // If the curve is not smooth, this point cannot be valid. Make this point
    // invalid, and return.
    if ( !parentCurve->isSmooth() ) {
        setInvalid();
        tmpCircle->setCursor(Qt::ArrowCursor);
        tmpCircle->setToolTip("<p>"+tr("This point cannot be moved to the curve right now, because the curve is not an elliptic curve. "
                                         "Before moving the point, use the sliders to change the shape of the curve.")+"</p>");
        tmpLabel->setToolTip(tmpCircle->toolTip());
        return;
    }

    // Now we can assume that the curve is actually smooth. It remains to check
    // is this point is still on the cuve, and to make the point invalid if it
    // is not.
    tmpCircle->setCursor(Qt::OpenHandCursor);
    tmpCircle->setToolTip("<p>"+tr("Drag the point to the curve!")+"</p>");
    tmpLabel->setToolTip(tmpCircle->toolTip());

    // If this point is invalid, there is nothing to do, really.
    if (getState() == invalid) {
        return;
    }

    // Since smooth curves in Weistrass normal form always contain the point at
    // infinity, nothing needs to be done if the state if infinity.
    if (getState() == infinity) {
        return;
    }

    // Otherwise, this point is affine, and the coordinates need perhaps be
    // adjusted.
    setCoords(parentCurve->pointOnCurve(getCoords()));
}


void ec_realFreePoint::mousePressEvent( QGraphicsSceneMouseEvent * event )
{
    // Paranoid safety checks.
    if (parentCurve.isNull()) {
        qCritical() << "ec_realFreePoint::mouseMoveEvent, but no parentcurve";
    }

    if (event->button() != Qt::LeftButton) {
        return;
    }

    // If the curve is smooth, and if the point can therefore be moved, set the
    // curve to a closed hand, so the user knows that he will be able to move.
    if (parentCurve->isSmooth()) {
        setCursor(Qt::ClosedHandCursor);
    }
}


void ec_realFreePoint::mouseMoveEvent( QGraphicsSceneMouseEvent * event )
{
    // Paranoid safety checks.
    if (parentCurve == nullptr) {
        qCritical() << "ec_realFreePoint::mouseMoveEvent, but no parentcurve";
    }

    qreal const dx = event->scenePos().x() - homePosition.x();
    qreal const dy = event->scenePos().y() - homePosition.y();
    qreal const distanceToHomeSq = dx * dx + dy * dy;

    if (distanceToHomeSq < 4) {
        setInvalid();
    } else {
        // The next three lines implement the magnetic snap-on for 2-torsion
        // points: if the y-coordinate of the mouse is close enough to zero, we
        // set it to zero precisely. Calling the method
        // parentCurve->pointOnCurve() on a point whose y-coordinate is precisely
        // zero will again return a point whose coordinate is precisely zero.
        QPointF mousePt = event->scenePos();
        if (qAbs(mousePt.y()) < 0.2) {
            mousePt.setY(0);
        }

        bool intError = false;
        QPointF const pointPosition = parentCurve->pointOnCurve(mousePt, &intError);

        if (!intError && !isOffscreen(pointPosition)) {
            setCoords(pointPosition);
        }
    }
}

void ec_realFreePoint::mouseReleaseEvent(QGraphicsSceneMouseEvent * /*event*/)
{
    setCursor(Qt::OpenHandCursor);
}


void ec_realFreePoint::setTmpCircleVisibility()
{
    prepareGeometryChange();
    if (getState() == affine) {
        tmpCircle->hide();
    } else {
        tmpCircle->show();
    }
}
