/***************************************************************************
 *   Copyright (C) 2018 by Stefan Kebekus                             *
 *   stefan.kebekus@math.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/*! \mainpage Elliptic Curve Plotter
 *
 * \section intro_sec Introduction
 *
 * The Elliptic Curve Plotter is a small Qt application that plots elliptic
 * curves which are given in Weierstrass normal form, allows the user to
 * change the equation parameters, add a few points and play with the group
 * law.
 *
 * \section design_sec Design
 *
 * The program is centered around the mainWindow, which is a standard
 * Qt QMainWindow that contains
 *
 * - an ecPlotter widget, where the curve and its points are drawn
 *
 * - a few sliders to change the curve shape
 *
 * - an ecDescription widget where the curve and the points are described.
 *
 * The design is rather straight-forward. All of the setup is done in
 * the constructor of the mainWindow. There, the curve and its points
 * are constructed, and connected to the ecDescription. The points are
 * then passed to the ecPlotter widget, which uses and owns them. All
 * the actions are handled in the ellipticcurveDialog.
 *
 * \section xx Coypright and License
 *
 * This program is (C) 2018 by Stefan Kebekus.
 *
 * This program is available under the GNU General Public License, Version 3.
 *
 * \section Dependencies
 *
 * The program uses the Qt library, version 4. To build, a Qt4-version of the
 * program "qmake" must be available.
 *
 *
 * @version Version
 * @date 2009
 * @author Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 */
